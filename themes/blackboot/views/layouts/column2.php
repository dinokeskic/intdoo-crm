<?php $this->beginContent('//layouts/main'); ?>
      <div class="row-fluid">
      <div id="sidebar-wrapper" class="span2">
        <div id="sidebar">
         <?php
       Yii::setPathOfAlias("bootstrap",Yii::getPathOfAlias("application.extensions.bootstrap"));
			$this->beginWidget('zii.widgets.CPortlet', array(
	
			));
			$this->widget('bootstrap.widgets.TbMenu', array(
				'items'=>Yii::app()->user->getMenu(),
				'htmlOptions'=>array('class'=>'sidebar'),
			));
			$this->endWidget();
		?>
		</div><!-- sidebar span3 -->
		</div>
          <div class="span10" >
		
                   <?php if(Yii::app()->user->getID()!=0){ ?> <div class="main" style="min-height: 700px;"> <?php } ?>
			<?php echo $content; ?>
                <?php if(Yii::app()->user->getID()!=0){ ?> </div><!-- content --><?php } ?>

	</div>
</div>

<?php $this->endContent(); ?>
