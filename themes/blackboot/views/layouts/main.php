<?php
Yii::setPathOfAlias("bootstrap",
Yii::getPathOfAlias('application.extensions.bootstrap'));

	$cs=Yii::app()->clientscript;
		// use it when you need it!
	$cs->registerCoreScript( 'jquery' );
		$cs->registerCssFile( Yii::app()->theme->baseUrl . '/css/bootstrap.css' );
		$cs->registerCssFile( Yii::app()->theme->baseUrl . '/css/bootstrap-responsive.css' );
		 $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/js/bootstrap-affix.js');      
        $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/js/sidebar.js');       

                
		/*$cs->registerScriptFile( Yii::app()->theme->baseUrl . '/js/bootstrap-transition.js', CClientScript::POS_END );
		$cs->registerScriptFile( Yii::app()->theme->baseUrl . '/js/bootstrap-alert.js', CClientScript::POS_END );
		$cs->registerScriptFile( Yii::app()->theme->baseUrl . '/js/bootstrap-modal.js', CClientScript::POS_END );
		$cs->registerScriptFile( Yii::app()->theme->baseUrl . '/js/bootstrap-dropdown.js', CClientScript::POS_END );
		$cs->registerScriptFile( Yii::app()->theme->baseUrl . '/js/bootstrap-scrollspy.js', CClientScript::POS_END );
		$cs->registerScriptFile( Yii::app()->theme->baseUrl . '/js/bootstrap-tab.js', CClientScript::POS_END );
		$cs->registerScriptFile( Yii::app()->theme->baseUrl . '/js/bootstrap-tooltip.js', CClientScript::POS_END );
		$cs->registerScriptFile( Yii::app()->theme->baseUrl . '/js/bootstrap-popover.js', CClientScript::POS_END );
		$cs->registerScriptFile( Yii::app()->theme->baseUrl . '/js/bootstrap-button.js', CClientScript::POS_END );
		$cs->registerScriptFile( Yii::app()->theme->baseUrl . '/js/bootstrap-collapse.js', CClientScript::POS_END );
		$cs->registerScriptFile( Yii::app()->theme->baseUrl . '/js/bootstrap-carousel.js', CClientScript::POS_END );
		$cs->registerScriptFile( Yii::app()->theme->baseUrl . '/js/bootstrap-typeahead.js', CClientScript::POS_END );*/
		
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo CHtml::encode($this->pageTitle); ?></title>
<meta name="language" content="en" />
<!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<!-- Le styles -->
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/bootstrap-responsive.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/bootstrap2.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/jquery-ui-timepicker-addon.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/style.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/dino.css" />

<!-- Le fav and touch icons -->
</head>

<body style="height:1200px; padding-top: 0px; " >
  
    <?php if(Yii::app()->user->getID()!=0){ ?>
	<div class="navbar navbar-inverse navbar-fixed-top" id="header">
		<div class="navbar-inner">
			<div class="container-fluid">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>
				<a class="brand" href=""><?php echo Yii::app()->name ?></a>
				<div class="nav-collapse">
					<?php $this->widget('zii.widgets.CMenu',array(
						'htmlOptions' => array( 'class' => 'nav' ),
						'activeCssClass'	=> 'active',
						'items'=>array(
							array('label'=>'Home ', 'url'=>array('/'), 'visible'=>!Yii::app()->user->isGuest),
                                                        array('label'=>Yii::t("main","NOTIFIKACIJE")."(".Yii::app()->user->notificationsCount().")",
                                                            'url'=>array('korisnik/obavijesti'),'visible'=>!Yii::app()->user->isGuest),
							array('label'=>'Login', 'url'=>array('/korisnik/login'), 'visible'=>Yii::app()->user->isGuest),
							array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/korisnik/logout'), 'visible'=>!Yii::app()->user->isGuest)
						),
					)); ?>
					
				</div><!--/.nav-collapse -->
			</div>
		</div>
	</div>
 
    <br>
        <br>
       
            
	<div class="cont">
	<div class="container-fluid">
	
	<?php echo $content ?>
	
	
	</div><!--/.fluid-container-->
	</div>
    <?php }else{  ?>
    <div class="cont">
    <div class="container-fluid" style="padding-top: 0px">
     
        <?php 
        echo $content;
        ?>
    </div>
    </div>
    <?php } ?>
	<!--<div class="extra">
	  <div class="container">
		<div class="row">
			<div class="col-md-3">
				<h4>Heading 1</h4>
				<ul>
					<li><a href="#">Subheading 1.1</a></li>
					<li><a href="#">Subheading 1.2</a></li>
					<li><a href="#">Subheading 1.3</a></li>
					<li><a href="#">Subheading 1.4</a></li>
				</ul>
			</div> 
			
			<div class="col-md-3">
				<h4>Heading 2</h4>
				<ul>
					<li><a href="#">Subheading 2.1</a></li>
					<li><a href="#">Subheading 2.2</a></li>
					<li><a href="#">Subheading 2.3</a></li>
					<li><a href="#">Subheading 2.4</a></li>
				</ul>
			</div> 
			
			<div class="col-md-3">
				<h4>Heading 3</h4>	
				<ul>
					<li><a href="#">Subheading 3.1</a></li>
					<li><a href="#">Subheading 3.2</a></li>
					<li><a href="#">Subheading 3.3</a></li>
					<li><a href="#">Subheading 3.4</a></li>
				</ul>
			</div> 
			
			<div class="col-md-3">
				<h4>Heading 4</h4>
				<ul>
					<li><a href="#">Subheading 4.1</a></li>
					<li><a href="#">Subheading 4.2</a></li>
					<li><a href="#">Subheading 4.3</a></li>
					<li><a href="#">Subheading 4.4</a></li>
				</ul>
				</div> 
			</div> 
		</div> 
	</div> -->
	<div class="footer">
	  <div class="container">
		<div class="row">
                    <!--
			<div id="footer-copyright" class="col-md-6">
				About us | Contact us | Terms & Conditions
			</div> <!-- /span6 -->
			<div id="footer-terms" class="col-md-6">
                            Copyright &copy; <?php echo date('Y'); ?> <a href="http://int-doo.com/int-doo/">INT-d.o.o</a><br/>
                            Developed by <a href="https://www.facebook.com/dino.keskic">Dino Keškić</a> & <a href="https://www.facebook.com/damir.dizdarevic.75">Damir Dizdarević</a><br/>
			</div> <!-- /.span6 -->
		 </div> <!-- /row -->
	  </div> <!-- /container -->
	</div>
</body>
</html>
