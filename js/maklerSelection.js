$(function()
		{
	$("#idmaklerSelect").select2({
		placeholder:"odaberite maklera..",
		allowClear:true,
		data:allMaklers,
		results:function(data,page)
		{
			return {results:data};
		},
		formatResult:formatMaklers,
		formatSelection:formatMaklersSelection,
		escapeMarkup:function(m){return m;},
	});
		});
function displayErrorsOrNotifications(data)
{
	console.log("displayErrorsOrNotifications pozvana.");
	if(data.success==true)
		{
                    location.reload();
	$.notify(Yii.t("main","TERMIN_DODIJELJEN"),"success");
	$("#assignMaklerDialog").dialog("close");
	var link=$("<a>",{
		href:data.linkUrl,
	});
	link.html(data.linkText);
	$("#assignedMaklerLink").append(link);
		}
	else
		{
		console.log("success je false a bom za ostalo ne znam.");
	   for(var error in data.error)
		   {
		     $("#"+error+"_em_").html(data.error[error]);
		     $("#"+error+"_em_").show();
		     console.log(error+":"+data.error[error]);
		   }
		}
	}
function formatMaklers(makler)
{
      var markup="<div>";
      markup+="<div>"+makler.ime+" "+makler.prezime+"</div>";
      markup+="<div>"+makler.adresa+"</div>";
      markup+="<div>"+makler.telefon+"</div>";
      markup+="</div>";
      return markup;
	}
function formatMaklersSelection(makler)
{
   return makler.text;	
}
function displayStupidAlert(data)
{
     
	}