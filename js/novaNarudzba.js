/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(function(){
   $("#novaNarudzbaButton").click(function(e){
       e.preventDefault();
       $.ajax({
           url:novaNarudzbaUrl,
           type:"POST",
           data:$("#novaNarudzba-form").serialize(),
           dataType:"json",
           success:function(response)
           {
               if(response.success==true)
               {
                   $.notify(Yii.t("main","NARUDZBA_SPREMLJENA"),"success");
                   $("#novaNarudzbaDialogOpenerButton").hide();
                  $("#novaNarudzbaDialog").dialog("close"); 
               }
                   else
           {
             for(var error in response.errors)
		   {
		     $("#"+error+"_em_").html(response.errors[error]);
		     $("#"+error+"_em_").show();
		     console.log(error+":"+response.errors[error]);
		   }
           }
           }
       })
   }); 
   $("#novaNarudzbaDialogOpenerButton").click(function(e){
       e.preventDefault();
       console.log("nova narudzba je pozvana");
 
      $("#novaNarudzbaDialog").dialog("open"); 
   });
});

