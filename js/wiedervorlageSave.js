function showWiedervorlageDialog(event)
{
    if($(event.target).is(".deleteEvent"))
    {
     return false;
    }
    else
    {
    var datum=event.date.format("YYYY-MM-DD");
    var datumDiv=event.date.format("DD.MM.YYYY");
    $("#datum").html(datumDiv);
  $("#datumVrijemeWiedervorlage").val(datum);
  console.log("inside showWiedervorlageDialog method");
    $("#addWiedervorlageDialog").dialog("open");
    $("input").val("");
    $(".errorMessage").hide();
}
}
$(function(){
   $("#saveWiedervorlageButton").click(saveWiedervorlage); 
});
function saveWiedervorlage()
{
    var vrijeme=$("#vrijemeWiedervorlage").val();
    if(vrijeme!="")
    {
    var datum=moment($("#datum").html(),"DD.MM.YYYY");
    var month=datum.month()+1;
    var year=datum.year();
    var datum=datum.format("DD.MM.YYYY");
    var datumVrijeme=datum+" "+vrijeme+":00";
    $("#datumVrijemeWiedervorlage").val(datumVrijeme);
    var data=$("#wiedervorlage-form").serialize();
    $.ajax({
       url:saveWiedervorlageUrl,
       data:data,
       type:"POST",
       dataType:"json",
       success:function(response)
       {
           $(".errorMessage").hide();
           if(response.success==true)
           {
               $.notify(Yii.t("main","WIEDERVORLAGE_DODAN"),"success");
           $("#addWiedervorlageDialog").dialog("close");
           $("input").val("");
           	$.ajax({
		url:ajaxUrl,
		data:{
			month:month,
			year:year
		},
		dataType:'json',
                context:this,
		type:'GET',
                success:function(response){
          calendar.setEvents(response);
                }
            });

       }
           
           else
           {
             for(var error in response.errors)
		   {
		     $("#"+error+"_em_").html(response.errors[error]);
		     $("#"+error+"_em_").show();
		     console.log(error+":"+response.errors[error]);
		   }
           }
       }
    });
}
else
{
  $("#Wiedervorlage_vrijeme_em_").val("Morate odabrati vrijeme");
  $("#Wiedervorlage_vrijeme_em_").show();
}
}

