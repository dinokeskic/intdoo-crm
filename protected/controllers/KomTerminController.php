<?php

class KomTerminController extends Controller
{
	public function actionIndex()
	{
		$this->render('index');
	}
	public function actionCreate()
	{
		if(isset($_POST['KomTermin']))
		{
			$komTermin=new KomTermin();
			$komTermin->attributes=$_POST['KomTermin'];
			if($komTermin->save())
			Yii::log("Uspješno sačuvan komentar");
			else
			{
				Yii::log("Komentar Bgm nije sačuvan");
			}
		}
	}
	public function actionDelete()
	{
		if(isset($_POST['id']))
		{
			$komTermin=KomTermin::model()->findByPk($_POST['id']);
			$bool=$komTermin->delete();
			if($bool)
				Yii::log("Brisanje je uspjelo.");
			else
				Yii::log("Brisanje nije uspjelo");
		}
	}
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}