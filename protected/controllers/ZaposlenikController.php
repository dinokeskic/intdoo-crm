<?php

class ZaposlenikController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */

	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */

	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','create','update','delete','admin'),
				'expression'=>array('ZaposlenikController','allowOnlyAdmin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	public static function allowOnlyAdmin()
	{
		Yii::log("Yap,called");
		if(!Yii::app()->user->isGuest)
		{
				return Yii::app()->user->tip=="admin";
			 
		}
		
		else
		{
			return false;
		}
	
	}
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));

	}


	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$zaposlenik=new Zaposlenik();
        $korisnik=new Korisnik();
        $zaposlenik->scenario="create";
        $korisnik->scenario="create";
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Zaposlenik']) && isset($_POST['Korisnik']))
		{
			$zaposlenik->attributes=$_POST['Zaposlenik'];
			$korisnik->attributes=$_POST['Korisnik'];
			if($korisnik->validate() && $zaposlenik->validate())
			{
				$korisnik->save();
				$zaposlenik->idkorisnik=$korisnik->id;
				$zaposlenik->save();
				$this->redirect(Yii::app()->user->returnUrl);
			}
		}

		$this->render('create',array(
			'korisnik'=>$korisnik,
				'zaposlenik'=>$zaposlenik,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$zaposlenik=$this->loadModel($id);
	    $korisnik=$zaposlenik->idkorisnik0;
	    $zaposlenik->scenario='update';
	    $korisnik->scenario='update';
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

	if(isset($_POST['Zaposlenik']) && isset($_POST['Korisnik']))
		{
			$zaposlenik->attributes=$_POST['Zaposlenik'];
			$korisnik->attributes=$_POST['Korisnik'];
			if($korisnik->validate() && $zaposlenik->validate())
			{
				$korisnik->save();
				$zaposlenik->idkorisnik=$korisnik->id;
				$zaposlenik->save();
				$this->redirect(Yii::app()->user->returnUrl);
			}
		}
	    

		$this->render('update',array(
			'zaposlenik'=>$zaposlenik,
			'korisnik'=>$korisnik,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$zaposlenik=$this->loadModel($id);
		$korisnik=$zaposlenik->idkorisnik0;
		$zaposlenik->delete();
		$korisnik->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Zaposlenik');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Zaposlenik('search');
		$model->idkorisnik0=new Korisnik('search');
		$model->unsetAttributes(); 
		$model->idkorisnik0->unsetAttributes(); // clear any default values
		if(isset($_GET['Zaposlenik']) && isset($_GET['Korisnik']))
		{
			$model->attributes=$_GET['Zaposlenik'];
		$model->idkorisnik0->attributes=$_GET['Korisnik'];
		}
		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Zaposlenik the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Zaposlenik::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Zaposlenik $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='zaposlenik-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
