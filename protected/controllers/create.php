<?php
/* @var $this WiedervorlageController */
/* @var $model Wiedervorlage */
Yii::setPathOfAlias("bootstrap",
Yii::getPathOfAlias('application.extensions.bootstrap'));
$this->breadcrumbs=array(
	'Wiedervorlages'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Wiedervorlage', 'url'=>array('index')),
	array('label'=>'Manage Wiedervorlage', 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t("main","NOVI_WIEDERVORLAGE");?></h1>

<?php $this->renderPartial('_form', array('model'=>$model,'stranka'=>$stranka)); ?>