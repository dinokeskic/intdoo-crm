<?php
require_once 'TerminController.php';
class MaklerController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
            
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete,interestedInTermin', // we only allow deletion via POST request
		);
 
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */

	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','admin'),
				'users'=>array('@'),
			),
                    array('allow',
                        'actions'=>array('novaNarudzba'),
                        'users'=>array('@'),
                        ),
				array('allow',
					'expression'=>array(Yii::app()->user,'isMakler'),
		),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','delete'),
				'expression'=>array(Yii::app()->user,'isAdminOrPosrednik'),
			),
			array('allow',
					'actions'=>array('mojiTermini'),
					'users'=>array('@')
		),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$termin=new Termin();
		Yii::log("Controller is called.");
		if(isset($_GET['Wiedervorlage']) && isset($_GET['Stranka']))
		{
			$wiedervorlage->attributes=$_GET['Wiedervorlage'];			
			$wiedervorlage->idstranke0->attributes=$_GET['Stranka'];
		}
		$this->render('view',array(
			'model'=>$this->loadModel($id),
				'termin'=>$termin,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$makler=new Makler();
                $korisnik=new Korisnik();
                
                $makler->scenario="create";
                $korisnik->scenario="makler_create";
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Makler'])&&isset($_POST['Korisnik']))
		{
			$makler->attributes=$_POST['Makler'];
                        $korisnik->attributes=$_POST['Korisnik'];
		if($makler->validate()&&$korisnik->validate()){
                   
                    $korisnik->tip="makler";
                    $korisnik->save();
                    $makler->idkorisnik=$korisnik->id;
                    $makler->save();
                    if(isset($_POST['Makler']['lokacije']))
                    {
                    foreach($_POST['Makler']['lokacije'] as $lokacijaValue)
                    {
                    	$lokacija=new Lokacija('create');
                    	$lokacija->idmakler=$makler->idmakler;
                    	$lokacija->lokacija=$lokacijaValue;
                    	$lokacija->save();
                    }
                    }
                    	
				$this->redirect(array('view','id'=>$makler->idmakler));
                }}

		$this->render('create',array(
			'makler'=>$makler,
                         'korisnik'=>$korisnik
		));
        }
                 

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */


	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */


	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
              

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$makler=$this->loadModel($id);
                $korisnik=$makler->idkorisnik0;
                $makler->scenario='update';
                $korisnik->scenario='update';
                if(isset($_POST['Makler'])&&isset($_POST['Korisnik']))
		{
			$makler->attributes=$_POST['Makler'];
                        $korisnik->attributes=$_POST['Korisnik'];
		if($makler->validate()&&$korisnik->validate()){
                   
                    $korisnik->tip="makler";
                    $korisnik->save();
                    $makler->idkorisnik=$korisnik->id;
                    $makler->save();
                      if(isset($_POST['Makler']['lokacije']))
                    {
                    foreach($_POST['Makler']['lokacije'] as $lokacijaValue)
                    {
                    	$lokacija=new Lokacija('create');
                    	$lokacija->idmakler=$makler->idmakler;
                    	$lokacija->lokacija=$lokacijaValue;
                    	$lokacija->save();
                    }
                    }
				$this->redirect(array('view','id'=>$makler->idmakler));
                }}

		$this->render('create',array(
			'makler'=>$makler,
                         'korisnik'=>$korisnik
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	
     public function actionMojiTermini()
     {
     	$termin=new Termin('mojiTermini');
     	$termin->idwiedervorlage0=new Wiedervorlage('search');
     	$termin->idwiedervorlage0->idstranke0=new Stranka('search');
     	$termin->idwiedervorlage0->idkorisnik0=new Korisnik('search');
     	$termin->assignedMakler=new MaklerOwnsTermin('search');
     	$termin->assignedMakler->makler=new Makler('search');
     	if(isset($_GET['Termin']) && isset($_GET['Stranka']))
     	{
     		$termin->attributes=$_GET['Termin'];
     		$termin->idwiedervorlage0->idstranke0->attributes=$_GET['Stranka'];
     	}
     	$this->render('mojiTermini',array(
     			'termin'=>$termin,
     	));
     }
     public function actionNovaNarudzba()
     {
         if(isset($_POST['NarudzbaTermina']))
         {
             $response=array();
             $narudzba=new NarudzbaTermina();
             $narudzba->br_termina=$_POST['NarudzbaTermina']['br_termina'];
             $narudzba->idmakler=$_POST['NarudzbaTermina']['idmakler'];
             Yii::log("idmakler je:".$narudzba->idmakler,"info","idmaklera");
             if($narudzba->save())
             {
                $response['success']=true;
             }
             else
             {
                $errors=  CJSON::decode(CActiveForm::validate($narudzba));
                $response['errors']=$errors;
                $response['success']=false;
             }
             echo CJSON::encode($response);
         }
     }

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$makler=$this->loadModel($id);
                $korisnik=$makler->idkorisnik0;
                
                $makler->delete();
                $korisnik->delete();
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Makler');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Makler('search');
		$model->idkorisnik0=new Korisnik('search');
		$model->unsetAttributes();  // clear any default values
		$model->idkorisnik0->unsetAttributes();
		if(isset($_GET['Makler'])  && isset($_GET['Korisnik']))
		{
			$model->attributes=$_GET['Makler'];
			$model->idkorisnik0->attributes=$_GET['Korisnik'];
			Yii::log("Korisnicko ime korisnika:".$model->idkorisnik0->korisnickoIme);
		}
		$this->render('admin',array(
			'model'=>$model,
		));
	}
	public function actionInterestedInTermin()
	{
          
		if(isset($_POST['idtermin']) && isset($_POST['komentar']))
		{
			Yii::log("post i je to sve","info","interestedInTermin");
                   $criteria=new CDbCriteria();
			$criteria->with=array('idkorisnik0');
			$criteria->together=true;
			$criteria->compare("idkorisnik0.korisnickoIme",Yii::app()->user->name);
			$makler=Makler::model()->find($criteria);
			$mwt=new MaklerWantsTermin();
			$mwt->idmakler=$makler->idmakler;
                        $mwt->komentar=$_POST['komentar'];
			$mwt->idtermin=$_POST['idtermin'];
			$json=array();
			$json['success']=$mwt->save();
                             $admins=  Korisnik::model()->findAll('tip=:tip',array(":tip"=>"admin"));
                        foreach ($admins as $admin)
                        {
                            $notifikacija=new Notifikacija();
                            $notifikacija->idkorisnik=$admin->id;
                            $notifikacija->tip=  Notifikacija::MAKLER_INTESTED_TERMIN;
                            $notifikacija->code=$mwt->idtermin;
                          $maklerLink=  CHtml::link($makler->idkorisnik0->korisnickoIme, array(
                              'makler/view',
                              'id'=>$makler->idmakler,
                          ));
                          $terminLink=  CHtml::link("termin",array(
                              'termin/view',
                              'id'=>$mwt->idtermin,
                          ));
                          $notifikacija->text=Yii::t("main","MAKLER_ZAINTERESOVAN_NOTIFIKACIJA",array(
                              '{terminLink}'=>$terminLink,
                              '{maklerLink}'=>$maklerLink,
                          ));
                         $notifikacija->pregledano=0;
                         $notifikacija->save();
                        }
			echo CJSON::encode($json);
		}
	}
public function allMaklers()
{
	$maklers=Makler::model()->findAll();
	$jsonArray=array();
	foreach($maklers as $makler)
	{
		$json['korisnickoIme']=$makler->idkorisnik0->korisnickoIme;
		$json['ime']=$makler->ime;
		$json['prezime']=$makler->prezime;
		$json['adresa']=$makler->adresa;
		$json['telefon']=$makler->telefon;
		array_push($jsonArray,$json);
	}
	echo json_encode($jsonArray);
}
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Makler the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Makler::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Makler $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='makler-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
