<?php

class TerminController extends Controller
{
	public $layout='//layouts/column2';
	
	public function filters()
	{
	
			return array(
					'accessControl', // perform access control for CRUD operations
					'postOnly + delete,create,cancel,confirm', // we only allow deletion via POST request
			);
	}
	public function accessRules()
	{
		return array(
			array(
					'allow',
					'actions'=>array('create','index','view','getTerminsForMakler'),
					'users'=>array('@')
		),
                    array(
					'allow',
					'actions'=>array('create','index','view','getTerminsForZaposlenik'),
					'users'=>array('@')
                       
		),
				array(
						'allow',
						'actions'=>array('assignMakler','ponistiMaklera'),
						'expression'=>array(Yii::app()->user,"isAdminOrPosrednik"),
                                
		),
                    array(
					'allow',
					'actions'=>array('create','index','view','getTermins','delete'),
					'users'=>array('@')
		),
				array(
						'allow',
						'actions'=>array('cancel','confirm','pomjeriTermin','delete'),
						'expression'=>  array($this,'hasModifficationActionsAccess'),
		),
				array(
						'deny',
						'users'=>array('*'),
		)
		);
	}
        public function actionPonistiMaklera($id)
                
        {
            $criteria=new CDbCriteria();
            $criteria->compare("idtermin", $id);
            $mot=  MaklerOwnsTermin::model()->find($criteria);
           if($mot->delete())
           {
               $response['success']=true;
           }
           else
           {
               $response['success']=false;
           }
           echo CJSON::encode($response);
        }
	public function hasModifficationActionsAccess($user,$rule)
	{
		if(isset($_POST['id']))
		{
			return TerminController::userIsAuthorOrAdmin($_POST['id']);
		}
                elseif (Yii::app()->request->getQuery("id")!=null) {
                return TerminController::userIsAuthorOrAdmin(Yii::app()->request->getQuery("id"));
            }
		else
		{
			return false;
		}
	}
	/*
	 * Method checks if current user is author termin or application 
	 * administrator.
	 */
	public static function userIsAuthorOrAdmin($idtermin)
	{
		$termin=Termin::model()->findByPk($idtermin);
		return $termin->currentUserIsAuthor() || Yii::app()->user->isAdmin();
		
	}
        public function actionDelete($id)
        {
            $termin=  Termin::model()->findByPk($id);
            $termin->idwiedervorlage0->delete();
        }
public function actionCreate()
{
	if(isset($_POST['Termin']))
	{
		$termin=new Termin('create');
		$termin->attributes=$_POST['Termin'];
		
		$termin->status="offen";
		if($termin->validate())
		{
		$termin->idwiedervorlage0->status="termin";
	
			$termin->save(false);
			$termin->idwiedervorlage0->save(false);
			echo CJSON::encode(array('status'=>'success'));
		}
		else
		{
			$error = CActiveForm::validate($termin);
			if($error!='[]')
				echo $error;
			Yii::app()->end();
				
		}
		
	}
}
public function actionIndex()
{
	$termin=new Termin('search');
	$termin->idwiedervorlage0=new Wiedervorlage('search');
        if(Yii::app()->user->isMakler())
        {
            $termin->scenario="UserIsMakler";
        }
	$termin->idwiedervorlage0->idstranke0=new Stranka('search');
	$termin->idwiedervorlage0->idkorisnik0=new Korisnik('search');
	if(isset($_GET['Termin']) && isset($_GET['Stranka']))
	{
            if(isset($_GET['Wiedervorlage']))
            {
                $termin->idwiedervorlage0->attributes=$_GET['Wiedervorlage'];
            }
		$termin->attributes=$_GET['Termin'];
                Yii::log("Vrijeme je ".$termin->vrijeme,"info","TerminController::actionIndex");
		$termin->idwiedervorlage0->idstranke0->attributes=$_GET['Stranka'];
	
	}
	$this->render('index',array(
		'termin'=>$termin,	
	));
}
public function actionView($id)
{
	$model=Termin::model()->findByPk($id);
	$komentar=new KomWv();
	$mot=new MaklerOwnsTermin();
	$komentar->idwiedervorlage=$model->idwiedervorlage;
	$criteria=new CDbCriteria();
	$criteria->compare('idwiedervorlage',$model->idwiedervorlage,true);
	$criteria->limit=5;
	$dataProvider=new CActiveDataProvider('KomWv',array(
			'criteria'=>$criteria,
			'sort'=>array(
					'defaultOrder'=>'datum DESC',
			),
			'pagination'=>array(
					'pageSize'=>5,
			)
	
	));
   if($model!=null)
   {
	$this->render('view',array(
			'model'=>$model,
			'komentar'=>$komentar,
			'dataProvider'=>$dataProvider,
			'mot'=>$mot,
	));
   }
   else
   {
   	throw new CHttpException(400,"Stranica ne postoji");
   }
}
public function actionGetTerminsForMakler($idmakler)
{
	if(isset($_GET['month']) && isset($_GET['year']))
	{
		$monthBegin=DateHelper::getBeginningOfMonth($_GET['month'], $_GET['year']);
		$monthEnd=DateHelper::getEndOfMonth($_GET['month'],$_GET['year']);
		$criteria=new CDbCriteria();
		$criteria->with=array('assignedMakler');
		$criteria->compare('assignedMakler.idmakler',$idmakler);
		$criteria->together=true;
		$termins=Termin::model()->findAll($criteria);
		$jsonArray=array();
		foreach($termins as $termin)
		{
			$json['title']=$termin->idwiedervorlage0->idstranke0->imeStranke." ".
					$termin->idwiedervorlage0->idstranke0->prezimeStranke;
			$json['url']=Yii::app()->createUrl("termin/view",array("id"=>$termin->idtermin));
			$dateTime=DateTime::createFromFormat("Y-m-d H:i:s", $termin->vrijeme);
			$json['date']=$dateTime->format("Y-m-d");
			$json['time']=$dateTime->format("H:i");
			array_push($jsonArray, $json);
		}
		echo json_encode($jsonArray);
	}
}
public function actionGetTerminsForZaposlenik($idkorisnik)
{
	if(isset($_GET['month']) && isset($_GET['year']))
	{
		$monthBegin=DateHelper::getBeginningOfMonth($_GET['month'], $_GET['year']);
		$monthEnd=DateHelper::getEndOfMonth($_GET['month'],$_GET['year']);
		$criteria=new CDbCriteria();
		$criteria->with=array('idwiedervorlage0');
		$criteria->together=true;
		$criteria->compare('idwiedervorlage0.idkorisnik',$idkorisnik);
		$termins=Termin::model()->findAll($criteria);
		$jsonArray=array();
		foreach($termins as $termin)
		{
			$json['title']=$termin->status;
			$json['url']=Yii::app()->createUrl("termin/view",array("id"=>$termin->idtermin));
			$dateTime=DateTime::createFromFormat("Y-m-d H:i:s", $termin->vrijeme);
			$json['date']=$dateTime->format("Y-m-d");
			$json['time']=$dateTime->format("H:i");
			array_push($jsonArray, $json);
		}
		echo json_encode($jsonArray);
	}
}
public function actionPomjeriTermin($id)
{
    $termin=Termin::model()->findByPk($id);
    if(isset($_POST['Termin']))
    {
        $termin->attributes=$_POST['Termin'];
       if($termin->save())
       {
          $this->redirect(array('termin/view','id'=>$termin->idtermin));          
       }
}
      $this->render("pomjeriVrijeme",array(
          'termin'=>$termin,
      ));
}
public function actionGetTermins()
{
	if(isset($_GET['month']) && isset($_GET['year']))
	{
		$monthBegin=DateHelper::getBeginningOfMonth($_GET['month'], $_GET['year']);
		$monthEnd=DateHelper::getEndOfMonth($_GET['month'],$_GET['year']);
		$criteria=new CDbCriteria();
		$criteria->with=array('idwiedervorlage0');
		$criteria->order='t.vrijeme';
		$criteria->together=true;
		//$criteria->compare('idwiedervorlage0.idkorisnik',$idkorisnik);
		$termins=Termin::model()->findAll($criteria);
		$jsonArray=array();
		foreach($termins as $termin)
		{
			$json['text']=$termin->idwiedervorlage0->idstranke0->imeStranke." ".
					$termin->idwiedervorlage0->idstranke0->prezimeStranke;
			$json['status']=$termin->status;
			$json['url']=Yii::app()->createUrl("termin/view",array("id"=>$termin->idtermin));
			$dateTime=DateTime::createFromFormat("Y-m-d H:i:s", $termin->vrijeme);
			$json['date']=$dateTime->format("Y-m-d");
			$json['time']=$dateTime->format("H:i");
			array_push($jsonArray, $json);
		}
		echo json_encode($jsonArray);
	}
}
public function actionCancel()
{
	if(isset($_POST['id']))
	{
		$id=$_POST['id'];
		
	$termin=Termin::model()->findByPk($id);
	$termin->status="absage";
	$termin->save();
        if($termin->isMaklerAssigned())
        {
            
        $mot=  MaklerOwnsTermin::model()->find("idtermin=?",array($termin->idtermin));
         $notifikacija=new Notifikacija();
                       $terminLink=  CHtml::link(Yii::t("main", "TERMIN"),array(
                           "termin/view",
                           'id'=>$mot->idtermin
                       ));
                       $notifikacija->text=Yii::t("main","TERMIN_OTKAZAN_NOTIFIKACIJA",array(
                           '{terminLink}'=>$terminLink,
                       ));
                       $notifikacija->idkorisnik=$mot->makler->idkorisnik;
                       $notifikacija->pregledano=0;
                       $notifikacija->save();
        }
	}
}
public function actionConfirm()
{
	
	if(isset($_POST['id']))
	{
		$id=$_POST['id'];
		$terminBestetig=new Terminbestitig();
		$termin=Termin::model()->findByPk($id);
		$terminBestetig->idtermin=$id;
		$transaction=Yii::app()->db->beginTransaction();
		try
		{
			if(!$terminBestetig->save(false))
				throw new Exception("yeah..");
			$termin->status="bestätigt";
                        $termin->idwiedervorlage0->status="bestätigt";
			if(!$termin->save(false))
				throw new Exception("ok...");
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();
			Yii::log($e->getTraceAsString());
			
		}
	}
}
public function actionAssignMakler()
{
	$json=array();
	if(isset($_POST ['MaklerOwnsTermin']))
	{
		$mot=new MaklerOwnsTermin('create');
        $mot->attributes=$_POST['MaklerOwnsTermin'];
		if($mot->save())
		{
			$json['success']=true;
			$json['linkUrl']=Yii::app()->createUrl('makler/view',array('id'=>$mot->idmakler));
			$json['linkText']=$mot->makler->idkorisnik0->korisnickoIme;
                        $notifikacija=new Notifikacija();
                       $terminLink=  CHtml::link(Yii::t("main", "TERMIN"),array(
                           "termin/view",
                           'id'=>$mot->idtermin
                       ));
                       $notifikacija->text=Yii::t("main","TERMIN_DODJELJEN_NOTIFIKACIJA",array(
                           '{terminLink}'=>$terminLink,
                       ));
                       $notifikacija->idkorisnik=$mot->makler->idkorisnik;
                       $notifikacija->pregledano=0;
                       $notifikacija->save();
		}
		else
		{
			$json['success']=false;
			$json['error']=CJSON::decode(CActiveForm::validate($mot));
		}
	}
	else
	{
		$json['success']=false;
		$json['message']="nisu proslijeđeni neophodni parametri!";
	}
	echo CJSON::encode($json);
}
}