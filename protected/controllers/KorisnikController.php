<?php

class KorisnikController extends Controller
{
	public $layout='//layouts/column2';
	public function actionIndex()
	{
		$this->render('index');
	}
public function actionLogin()
{
	if(!Yii::app()->user->isGuest)
		$this->redirect(Yii::app()->createUrl(Yii::app()->user->homeUrl));
	$korisnik=new Korisnik;
	$korisnik->scenario='login';
	if(isset($_POST['Korisnik']))
	{
		$korisnik->attributes=$_POST['Korisnik'];
		if($korisnik->validate() && $korisnik->login())
		{
			$this->redirect(array(Yii::app()->user->homeUrl));
		}
		else
		{
			$korisnik->addError("lozinka", Yii::t("main", "LOGIN_FAIL"));
		}
	}
	$this->render('login',array('korisnik'=>$korisnik));
}
public function actionView($id)
{
   $zaposlenik=  Zaposlenik::model()->find('idkorisnik=?',array($id));
   $this->redirect(array('zaposlenik/view','id'=>$zaposlenik->idzaposlenik));
}
public function actionObavijesti()
{
    $korisnik=  Korisnik::model()->findByPk(Yii::app()->user->id);

    $this->render("obavijesti",array("korisnik"=>$korisnik));
}
public function actionPromjenaLozinke($korisnik)
{
   $korisnik=Korisnik::model()->find('korisnickoIme=?',array($korisnik));
   if(Yii::app()->user->getState("tip")=="admin" && Yii::app()->user->name!=Yii::app()->request->getQuery('korisnik'))
   {
   	$korisnik->scenario="adminMijenjaLozinku";
   }
   else
   {
   $korisnik->scenario="promjenaLozinke";
   }
	if(isset($_POST['Korisnik']))
	{
		Yii::log("unutar posta smo.");
		$hash=$korisnik->lozinka;
		$korisnik->attributes=$_POST['Korisnik'];
	if($korisnik->validate())
	{
		Yii::log("Validacija je prošla");
		if($korisnik->scenario!="adminMijenjaLozinku")
		{
			Yii::log("Scenario nije admin mijenja lozinku");
		if($hash==md5($korisnik->staraLozinka))
		{
			Yii::log("Provjera stare lozinke je prosla.");
			$t=$korisnik->save(false);
			if($t==true)
			{
				Yii::log("Sve je ok");
			}
			else
			{
			}
			Yii::app()->clientScript->registerScript("notifikacija",
			"$.notify('Lozinka uspješno promjenjena!','info');");
		}
		else
		{
			$korisnik->addError("staraLozinka", "Niste unijeli ispravnu staru lozinku!");
		}
		}
		else
		{
			Yii::log("Scenario je admin mijenja lozinku");
			if($korisnik->save(false))
				Yii::log("Save je uspio");
			else
				Yii::log("Save nije uspio");
		Yii::app()->clientScript->registerScript("notifikacija",
			"$.notify('Lozinka uspješno promjenjena!','info');");
		}
	}

	}
	$this->render('promjenaLozinke',array('korisnik'=>$korisnik));
}
public function actionPocetna()
{
	$korisnik=Korisnik::model()->find('korisnickoIme=?',array(Yii::app()->user->name))->with("zaposlenik")->together(true);
	
	$this->render('pocetna',array(
			'korisnik'=>$korisnik,
	));
}
public function actionAdminPanel()
{
	$this->render("index");
}
public function actionkalendar()
{
	$this->render("kalendar");
}
public function actionpocetnaMaklera()
{
	$this->render("pocetnaMaklera");
}
public function actionLogout()
{
			Yii::app()->user->logout(true);
		$this->redirect(Yii::app()->homeUrl);
}
public function allowOnlyOwner($user,$rule)
{
	if($user->name==Yii::app()->request->getQuery("korisnik") ||
	   Yii::app()->user->getState("tip")=="admin")
		return true;
	else
		return false;
	
}
public function filters()
{
	return array(
			'accessControl', // perform access control for CRUD operations
	);
}

/**
 * Specifies the access control rules.
 * This method is used by the 'accessControl' filter.
 * @return array access control rules
 */

public function accessRules()
{
	return array(
			array('allow',
					'actions'=>array('adminPanel'),
					'expression'=>array('KorisnikController','allowOnlyAdmin'),
	),
            array('allow',
					'actions'=>array('kalendar'),
					'expression'=>array('KorisnikController','allowOnlyAdmin'),
	),
			array('allow',  // allow all users to perform 'index' and 'view' actions
					'actions'=>array('logout','pocetna','obavijesti'),
                            
                    'users'=>array('@'),
			),
            array('allow',  // allow all users to perform 'index' and 'view' actions
					'actions'=>array('logout','pocetnaMaklera'),
                            
                    'users'=>array('@'),
			),
			array('allow',
					'actions'=>array('promjenaLozinke'),
					'expression'=>array($this,'allowOnlyOwner'),
	),
			array('allow',
					'actions'=>array('login'),
					'users'=>array('*'),				
	              ),
			array('deny',
					'users'=>array('*'),
	),
	);
}
public static function allowOnlyAdmin()
{
	Yii::log("Yap,called");
	if(!Yii::app()->user->isGuest)
	{
		return Yii::app()->user->tip=="admin";

	}

	else
	{
		return false;
	}

}
	// Uncomment the following methods and override them if needed
	/*
	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}