<?php

class WiedervorlageController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
				array('allow',
						'actions'=>array('index'),
						'expression'=>array($this,'allowOnlyOwnerIndex'),
		),
				array('allow',
						'actions'=>array('view'),
						'expression'=>array($this,'allowOnlyOwnerView'),
				),
                    array('allow',
                        'actions'=>array('getWiedervorlages'),
                        'expression'=>array($this,'allowOnlyOwnerCalendar'),
                        ),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','cancel'),
				'users'=>array('@'),
			),
                    array(
                        'allow',
                        'actions'=>array('dodaj'),
                        'expression'=>array(Yii::app()->user,"isAdmin"),
                    ),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('@'),
			),
                    array('allow',
                        'actions'=>array('all'),
                        'users'=>array('@'),
                        ),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	

	/**
	 * This method checks whether the user is the owner of the index page, or
	 * an application administrator, returning true in both cases. It returns false othervise
	 * Called by accessRules
	 */
	public function allowOnlyOwnerIndex($user,$rule)
	{
		$korisnickoIme=Yii::app()->request->getQuery('korisnik');
		if(Yii::app()->user->name==$korisnickoIme || Yii::app()->user->getState('tip')=="admin")
			return true;
		else
			return false;
	}
     public function allowOnlyOwnerCalendar($user,$rule)
	{
		$idkorisnik=Yii::app()->request->getQuery('idkorisnik');
		if(Yii::app()->user->id==$idkorisnik || Yii::app()->user->getState('tip')=="admin")
			return true;
		else
			return false;
	}
	/**
	 * This method checks whether the user is the creator of the wiedervorlage this page displays, or
	 * an application administrator, returning true in both cases. It returns false othervise
	 * Called by accessRules
	 */
	public function allowOnlyOwnerView()
	{
		$idwiedervorlage=Yii::app()->request->getQuery("id");
		$wiedervorlage=Wiedervorlage::model()->findByPk($idwiedervorlage);
		Yii::log("korisnicko ime:".$wiedervorlage->idkorisnik0->korisnickoIme,"info","wiedervorlageAllowView");
		if(Yii::app()->user->name==$wiedervorlage->idkorisnik0->korisnickoIme || Yii::app()->user->tip=="admin")
			return true;
		else
			return false;
	}
	public function actionCreate()
	{
		$model=new Wiedervorlage;
                $stranka=new Stranka;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
                $model->scenario='create';
                $stranka->scenario='create';
                if(Yii::app()->user->tip=="admin")
                {
                    $model->scenario='adminCreate';
                }
		if(isset($_POST['Wiedervorlage'])&&isset($_POST['Stranka']))
		{
                   
                        
			$model->attributes=$_POST['Wiedervorlage'];
			$stranka->attributes=$_POST['Stranka'];
                        Yii::log("Postavljeno vrijeme:".$model->vrijeme,"info","postavljenoVrijeme");
                        if($model->validate()&&$stranka->validate()){
                          $transaction=Yii::app()->db->beginTransaction();
                          Yii::app()->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                          try 
                          { 
                          	
                          if($success= $stranka->save(false))
                          	Yii::log("Uspjelo je spremanje stranke","info","myCategory");
                       
                          else
                          {
                          	Yii::log("Nije uspjelo spremanje stranke","info","myCategory");
                    
                          	throw  new CDbException("Error saving stranka");
                          }
                            $model->idstranke=$stranka->idstranka;
                            $model->idkorisnik=Yii::app()->user->getId();
                            $model->status="offen";
              
                            if($success=$model->save(false))
                            {
                            	Yii::log("uspjelo je spremanje wiedervorlage","info","myCategory");
                                $json['success']=true;
                                   if(Yii::app()->user->tip=="zaposlenik")
                          {
                                     $admins=  Korisnik::model()->findAll('tip=:tip',array(":tip"=>"admin"));
                        foreach ($admins as $admin)
                        {
                            $notifikacija=new Notifikacija();
                            $notifikacija->idkorisnik=$admin->id;
                            $notifikacija->tip=  Notifikacija::WIEDERVORLAGE_CREATED;
                            $notifikacija->code=$model->idwiedervorlage;
                          $korisnikLink=  CHtml::link(Yii::app()->user->name, array(
                              'korisnik/view',
                              'id'=>Yii::app()->user->id,
                          ));
                          $wiedervorlageLink=  CHtml::link("wiedervorlage",array(
                              'wiedervorlage/view',
                              'id'=>$model->idwiedervorlage,
                          ));
                          $notifikacija->text=Yii::t("main","NOVI_WIEDERVORLAGE_NOTIFIKACIJA",array(
                              '{wiedervorlageLink}'=>$wiedervorlageLink,
                              '{korisnikLink}'=>$korisnikLink,
                          ));
                         $notifikacija->pregledano=0;
                         $notifikacija->save();
                        }
                          }
                                if(Yii::app()->request->isAjaxRequest)
                                echo CJSON::encode($json);
                            }
                            
                            else
                            {
                            	Yii::log("nije uspjelo spremanje wiedervorlage","info","myCategory");
                        
                            	throw new CDbException("Error saving model");
                            }
                            $transaction->commit();
                            if(!Yii::app()->request->isAjaxRequest)
                            {
                            $this->redirect(array("wiedervorlage/view","id"=>$model->idwiedervorlage));
                            }
                          }
                          catch(CDbException $e)
                          {
                          	Yii::log($e->errorInfo[2],"error","db exception");
                          	$transaction->rollback();
                          	Yii::app()->clientScript->registerScript("notifikacija",
                          	"$.notify('Greška u povezivanju s bazom podataka!','error');");
                          }
                        }
                        else
                        {
                            if(Yii::app()->request->isAjaxRequest)
                            {
                                $validacija=  CJSON::decode( CActiveForm::validate(array($model,$stranka)));
                                $response['success']=false;
                                $response['errors']=$validacija;
                                echo CJSON::encode($response);
                            }
                        }
                      
		}
                
    if(Yii::app()->request->isAjaxRequest==false)
    {
    Yii::log("nije bio ajax","info","JeliAjax");
		$this->render('create',array(
			'model'=>$model,
                        'stranka'=>$stranka
		));
    }
	}
        public function actionDodaj($korisnik)
	{
            $korisnik=  Korisnik::model()->find("korisnickoIme=?",array($korisnik));
		$model=new Wiedervorlage;
                $stranka=new Stranka;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
                $model->scenario='create';
                $stranka->scenario='create';
                  if(Yii::app()->user->tip=="admin")
                {
                    $model->scenario='adminCreate';
                }
		if(isset($_POST['Wiedervorlage'])&&isset($_POST['Stranka']))
		{
                   
                        
			$model->attributes=$_POST['Wiedervorlage'];
			$stranka->attributes=$_POST['Stranka'];
			$model->vrijeme=$_POST['Wiedervorlage']['vrijeme'];
                        if($model->validate()&&$stranka->validate()){
                          $transaction=Yii::app()->db->beginTransaction();
                          Yii::app()->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                          try 
                          { 
                          	
                          if($success= $stranka->save(false))
                          	Yii::log("Uspjelo je spremanje stranke","info","myCategory");
                          else
                          {
                          	Yii::log("Nije uspjelo spremanje stranke","info","myCategory");
                    
                          	throw  new CDbException("Error saving stranka");
                          }
                            $model->idstranke=$stranka->idstranka;
                            $model->idkorisnik=$korisnik->id;
                            $model->status="offen";
              
                            if($success=$model->save(false))
                            {
                            	Yii::log("uspjelo je spremanje wiedervorlage","info","myCategory");
                                $json['success']=true;
                                if(Yii::app()->request->isAjaxRequest)
                                {
                                echo CJSON::encode($json);
                                }
                            }
                            
                            else
                            {
                            	Yii::log("nije uspjelo spremanje wiedervorlage","info","myCategory");
                        
                            	throw new CDbException("Error saving model");
                            }
                            $transaction->commit();
                            if(!Yii::app()->request->isAjaxRequest)
                            {
                            $this->redirect(array("wiedervorlage/view","id"=>$model->idwiedervorlage));
                            }
                          }
                          catch(CDbException $e)
                          {
                          	Yii::log($e->errorInfo[2],"error","db exception");
                          	$transaction->rollback();
                          	Yii::app()->clientScript->registerScript("notifikacija",
                          	"$.notify('Greška u povezivanju s bazom podataka!','error');");
                          }
                        }
                        else
                        {
                            if(Yii::app()->request->isAjaxRequest)
                            {
                                $validacija=  CJSON::decode( CActiveForm::validate(array($model,$stranka)));
                                $response['success']=false;
                                $response['errors']=$validacija;
                                echo CJSON::encode($response);
                            }
                        }
                      
		}
                
    if(Yii::app()->request->isAjaxRequest==false)
    {
    Yii::log("nije bio ajax","info","JeliAjax");
		$this->render('create',array(
			'model'=>$model,
                        'stranka'=>$stranka
		));
    }
	}
	public function actionView($id)
	{
		$komWv=new KomWv();
		$komWv->idwiedervorlage=$id;
		$model=$this->loadModel($id);
		$criteria=new CDbCriteria();
		$criteria->compare('idwiedervorlage',$id,true);
		$criteria->limit=5;
		$dataProvider=new CActiveDataProvider('KomWv',array(
				'criteria'=>$criteria,
				'sort'=>array(
						'defaultOrder'=>'datum DESC',
				),
				'pagination'=>array(
						'pageSize'=>5,
				)
	
		));
		$this->render('view',array(
				'model'=>$model,
				'dataProvider'=>$dataProvider,
				'komWv'=>$komWv,
		));
	}
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$wiedervorlage=$this->loadModel($id);
		$stranka=$wiedervorlage->idstranke0;
		$wiedervorlage->scenario='update';
		$stranka->scenario='update';
                if(Yii::app()->user->tip=="admin")
                {
                    $wiedervorlage->scenario="adminUpdate";
                }
		if(isset($_POST['Stranka']) && isset($_POST['Wiedervorlage']))
		{
                   Yii::log("unutar posta smo!","info","postiSe");
                        
			$wiedervorlage->attributes=$_POST['Wiedervorlage'];
			$stranka->attributes=$_POST['Stranka'];
		$transaction=Yii::app()->db->beginTransaction();
			try
			{
           if(!$stranka->save())
           	throw new CDbException("Stranka nije sacuvana");
           if(!$wiedervorlage->save())
           	throw new CDbException("Wiedervorlage nije sacuvan");
           $transaction->commit();
           if($wiedervorlage->status=="offen" || $wiedervorlage->status=="absage")
           	$this->redirect(array("wiedervorlage/view","id"=>$wiedervorlage->idwiedervorlage));
               else {
               $termin=Termin::model()->find('idwiedervorlage=?',array($wiedervorlage->idwiedervorlage));
               $this->redirect(array('termin/view','id'=>$termin->idtermin));
               }
			}
              catch(CDbException $e)
              {
              	Yii::log($e->getMessage(),"info","WiedervorlageController_actionUpdate");
              	$transaction->rollback();
              }      
		}
	 	$this->render('update',array(
			'model'=>$wiedervorlage,
			'stranka'=>$stranka
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!Yii::app()->request->isAjaxRequest)
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex($korisnik)
	{
		$korisnikModel=Korisnik::model()->find("korisnickoIme=?",array($korisnik));
		$wiedervorlage=new Wiedervorlage('searchByKorisnik');
		$wiedervorlage->idstranke0=new Stranka('search');
		$termin=new Termin();
		$wiedervorlage->unsetAttributes();
		$wiedervorlage->idstranke0->unsetAttributes();
		
			
		if(isset($_GET['Wiedervorlage']) && isset($_GET['Stranka']))
		{
			$wiedervorlage->attributes=$_GET['Wiedervorlage'];

			$wiedervorlage->idstranke0->attributes=$_GET['Stranka'];
		}
		$wiedervorlage->idkorisnik=$korisnikModel->id;
		Yii::log("iz actionIdex idkorisnik:".$wiedervorlage->idkorisnik,"info");
		$dataProvider=new CActiveDataProvider('Wiedervorlage');
		$this->render('index',array(
			'wiedervorlage'=>$wiedervorlage,
				'termin'=>$termin,
		));
	}
        public function actionAll()
	{
	
		$wiedervorlage=new Wiedervorlage('searchByKorisnik');
		$wiedervorlage->idstranke0=new Stranka('search');
		$termin=new Termin();
		$wiedervorlage->unsetAttributes();
		$wiedervorlage->idstranke0->unsetAttributes();
		
			
		if(isset($_GET['Wiedervorlage']) && isset($_GET['Stranka']))
		{
			$wiedervorlage->attributes=$_GET['Wiedervorlage'];

			$wiedervorlage->idstranke0->attributes=$_GET['Stranka'];
		}
		$dataProvider=new CActiveDataProvider('Wiedervorlage');
		$this->render('index',array(
			'wiedervorlage'=>$wiedervorlage,
				'termin'=>$termin,
		));
	}
	public function actionCancel()
	{
		if(isset($_POST['id']))
		{
			$wiedervorlage=Wiedervorlage::model()->findByPk($_POST['id']);
			$wiedervorlage->status="absage";
			$wiedervorlage->save();
		}
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Wiedervorlage('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Wiedervorlage']))
			$model->attributes=$_GET['Wiedervorlage'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
public function actionUpdateVrijeme($id)
{
	
}
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Wiedervorlage the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Wiedervorlage::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Wiedervorlage $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='wiedervorlage-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
public function actionGetWiedervorlages($idkorisnik)
{
		$criteria=new CDbCriteria();
		$criteria->with=array('idstranke0');
		$criteria->together=true;
		$criteria->compare('t.idkorisnik',$idkorisnik);
		$wiedervorlages=  Wiedervorlage::model()->findAll($criteria);
		$jsonArray=array();
		foreach($wiedervorlages as $wiedervorlage)
		{
			$json['title']=$wiedervorlage->idstranke0->imeStranke.
                                " ".$wiedervorlage->idstranke0->prezimeStranke.
                                "<br/>".$wiedervorlage->idstranke0->telefonStranke;
			$json['url']=Yii::app()->createUrl("wiedervorlage/view",array("id"=>$wiedervorlage->idwiedervorlage));
			$dateTime=DateTime::createFromFormat("Y-m-d H:i:s", $wiedervorlage->vrijeme);
			$json['date']=$dateTime->format("Y-m-d");
			$json['time']=$dateTime->format("H:i");
                        $json['deleteLink']=Yii::app()->createUrl("wiedervorlage/delete",array('id'=>$wiedervorlage->idwiedervorlage));
			array_push($jsonArray, $json);
		}
		echo json_encode($jsonArray);
}
}
