<?php

class KomWvController extends Controller
{
	public function actionIndex()
	{
		$this->render('index');
	}
public function actionCreate()
{
	if(isset($_POST['KomWv']))
	{
		$komWv=new KomWv();
		$komWv->attributes=$_POST['KomWv'];
		$komWv->save();
	}
}
public function actionDelete()
{
	if(isset($_POST['id']))
	{
	$komWv=KomWv::model()->findByPk($_POST['id']);
	$bool=$komWv->delete();
	if($bool)
		Yii::log("Brisanje je uspjelo.");
	else
		Yii::log("Brisanje nije uspjelo");
	}
}
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}