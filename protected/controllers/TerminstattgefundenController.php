<?php

class TerminstattgefundenController extends Controller
{
	public $layout='//layouts/column2';
	public function actionIndex()
	{
			$terminstattgefunden=new Terminstatgefunden('search');
	$terminstattgefunden->idterminbestig0=new Terminbestitig('search');
	$terminstattgefunden->idterminbestig0->idtermin0=new Termin('search');
	$terminstattgefunden->idterminbestig0->idtermin0->idwiedervorlage0=new Wiedervorlage('search');
	$terminstattgefunden->idterminbestig0->idtermin0->idwiedervorlage0->idstranke0=new Stranka('search');
        $terminstattgefunden->idterminbestig0->idtermin0->assignedMakler=new MaklerOwnsTermin("search");
        $terminstattgefunden->idterminbestig0->idtermin0->assignedMakler->makler=new Makler("search");
        $terminstattgefunden->idterminbestig0->idtermin0->assignedMakler->makler->idkorisnik0=new Korisnik("search");
	$terminstattgefunden->idterminbestig0->idtermin0->idwiedervorlage0->idkorisnik0=new Korisnik('search');
        $terminstattgefunden->idterminbestig0->idtermin0->assignedMakler->makler->firma=new Firma("search");
	if( isset($_GET['Termin']) && isset($_GET['Stranka']))
	{
              if(isset($_GET['Wiedervorlage']))
            {
                $terminstattgefunden->idterminbestig0->idtermin0->idwiedervorlage0->attributes=$_GET['Wiedervorlage'];
            }
            if(isset($_GET['Firma']))
                $terminstattgefunden->idterminbestig0->idtermin0->assignedMakler->makler->firma->attributes=$_GET['Firma'];
                if(isset($_GET['Korisnik']))
                    $terminstattgefunden->idterminbestig0->idtermin0->assignedMakler->makler->idkorisnik0->attributes=$_GET['Korisnik'];
		$terminstattgefunden->idterminbestig0->idtermin0->attributes=$_GET['Termin'];
		$terminstattgefunden->idterminbestig0->idtermin0->idwiedervorlage0->idstranke0->attributes=$_GET['Stranka'];
		//$terminstattgefunden->idterminbestig0->idtermin0->idwiedervorlage0->idkorisnik0->korisnickoIme=$_GET['autor'];
	
	}
	$this->render('index',array(
		'terminstattgefunden'=>$terminstattgefunden,	
	));
	}
	
	
	public function actionCreate()
	{
		if(isset($_POST['id']))
		{
			$terminstattgefunden=new Terminstatgefunden('create');
			$terminstattgefunden->idterminbestig=$_POST['id'];
			if($terminstattgefunden->validate())
			{
				$terminstattgefunden->idterminbestig0->idtermin0->status="terminstattgefunden";
                                $terminstattgefunden->idterminbestig0->idtermin0->idwiedervorlage0->status="stattgefunden";
	
				$terminstattgefunden->save(false);
				$terminstattgefunden->idterminbestig0->idtermin0->save(false);
                                $terminstattgefunden->idterminbestig0->idtermin0->idwiedervorlage0->save(false);
				echo CJSON::encode(array('status'=>'success'));
			}
			else
			{
				echo CJSON::encode(array('success'=>false));
	
			}
	
		}
	}
	// Uncomment the following methods and override them if needed

	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
	return array(
					'accessControl', // perform access control for CRUD operations
					'postOnly + create', // we only allow deletion via POST request
			);
	}
	public function accessRules()
	{
		return array(
				array('allow',
						'actions'=>array('index','create'),
						'expression'=>array(Yii::app()->user,'isAdmin'),
		)
		);
	}
	public function actionView($id)
	{
		$terminstattgefunden=Terminstatgefunden::model()->findByPk($id);
		$this->redirect(array("termin/view",'id'=>$terminstattgefunden->idterminbestig0->idtermin));
	}
	
/*
	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
*/
}