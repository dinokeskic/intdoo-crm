<?php

class TerminbestetigungController extends Controller
{
	public $layout='//layouts/column2';
	public function actionIndex()
	{
		$terminstattgefunden=new Terminstatgefunden();
	$terminbestetigung=new Terminbestitig('search');
	$terminbestetigung->idtermin0=new Termin('search');
	$terminbestetigung->idtermin0->idwiedervorlage0=new Wiedervorlage('search');
	$terminbestetigung->idtermin0->idwiedervorlage0->idstranke0=new Stranka('search');
	$terminbestetigung->idtermin0->idwiedervorlage0->idkorisnik0=new Korisnik('search');
	if(isset($_GET['Terminbestitig']) && isset($_GET['Termin']) && isset($_GET['Stranka']) && isset($_GET['Korisnik']) && 
	isset($_GET['autor']))
	{ 
              if(isset($_GET['Wiedervorlage']))
            {
                $terminbestetigung->idtermin0->idwiedervorlage0->attributes=$_GET['Wiedervorlage'];
            }
		$terminbestetigung->attributes=$_GET['Terminbestitig'];
		$terminbestetigung->idtermin0->attributes=$_GET['Termin'];
		$terminbestetigung->idtermin0->idwiedervorlage0->idstranke0->attributes=$_GET['Stranka'];
		$terminbestetigung->idtermin0->idwiedervorlage0->idmakler0->idkorisnik0->attributes=$_GET['Korisnik'];
		$terminbestetigung->idtermin0->idwiedervorlage0->idkorisnik0->korisnickoIme=$_GET['autor'];
	
	}
	$this->render('index',array(
		'terminbestetigung'=>$terminbestetigung,
			'terminstattgefunden'=>$terminstattgefunden,	
	));
	}
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
				'accessControl', // perform access control for CRUD operations
				'postOnly + create', // we only allow deletion via POST request
		);
	}
	public function accessRules()
	{
		return array(
				array('allow',
						'actions'=>array('index','create'),
						'expression'=>array(Yii::app()->user,'isAdmin'),
				)
		);
	}
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}