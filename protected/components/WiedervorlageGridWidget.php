<?php
/*
 * Widget prikazuje listu wiedervorlage-a
 * Widget zahtjeva kao parametre dataProvider, wiedervorlage model i termin model
 * Koristi CGridView za prikaz podataka s mogućnošću filtriranja , sortiranja,
 * otkizivanja wiedervorlage-a i kreiranja termina.
 */
class WiedervorlageGridWidget extends CWidget {

	public $wiedervorlage;
	public  $termin;
	public $dataProvider;
	public $showMaklerColumn=false;
	public $gridColumns;
	public function run()
	{
		Yii::setPathOfAlias("bootstrap",
		Yii::getPathOfAlias('application.extensions.bootstrap'));
		new JsTrans("main", "de");
		$this->initializeGridColumns();
		$this->render('wiedervorlage');
	}
private function initializeGridColumns()
{
	$this->gridColumns=array(
	
			array(
					'name'=>'vrijeme',
					'htmlOptions'=>array(
					'header'=>Yii::t("main","VRIJEME"),
					),
					'value'=>'DateTime::createFromFormat("Y-m-d H:i:s",$data->vrijeme)->format("d.m.Y H:i")',
					'filter'=>$this->widget('bootstrap.widgets.TbDateRangePicker',array(
							'model'=>$this->wiedervorlage,
							'attribute'=>'vrijeme',
	
							'callback'=>'js:function(start,end){
										gridID="wiedervorlage-grid";
											$.fn.yiiGridView.update(gridID, {data: $.param(
        			$("#"+gridID+" .filters input, #"+gridID+" .filters select")
   								)});
												}',
							'options'=>array(
	
									'format'=>'DD.MM.YYYY hh:mm',
									'timePicker'=>true,
									'timePicker12Hour'=>false,
									'locale'=>array(
											'applyLabel'=>Yii::t("main","PRIMJENI"),
											'cancelLabel'=>Yii::t("main","OTKAŽI"),
											'fromLabel'=>Yii::t("main","OD"),
											'toLabel'=>Yii::t("main","DO"),
	
									),
	
							),
	
							'htmlOptions'=>array(
									'id'=>'date_range_picker',
	
							),
					),true),
			),
			/*array(
					'name'=>'status',
					'header'=>Yii::t("main","STATUS"),
					'value'=>'$data->status',
					'filter'=>CHtml::activeDropDownList($this->wiedervorlage, 'status', array(
							''=>'',
							'offen'=>'Offen',
							'absage'=>'Absage',
					))
	
			),*/
			array(
					'name'=>'ime',
					'header'=>Yii::t("main","IME_PREZIME"),
					'sortable'=>true,
                                        'visible'=>Yii::app()->user->tip!="makler",
					'value'=>'$data->idstranke0->imeStranke." ".$data->idstranke0->prezimeStranke',
					'filter'=>CHtml::activeTextField($this->wiedervorlage->idstranke0,'imeStranke')
			),
			array(
					'name'=>'telefon',
					'header'=>Yii::t("main","TELEFON"),
					'visible'=>Yii::app()->user->tip!="makler",
					'value'=>'$data->idstranke0->telefonStranke',
					'filter'=>CHtml::activeTextField($this->wiedervorlage->idstranke0,'telefonStranke'),
			),
			array(
					'name'=>'adresa',
					"header"=>Yii::t("main","ADRESA"),
					'visible'=>Yii::app()->user->tip!="makler",
					'value'=>'$data->idstranke0->adresaStranke',
					'filter'=>CHtml::activeTextField($this->wiedervorlage->idstranke0,'adresaStranke'),
			),
            array(
                    'name'=>'lokacija',
                    'header'=>Yii::t("main","LOKACIJA"),
                    'visible'=>true,
                    'value'=>'$data->idstranke0->lokacija',
                    'filter'=>CHtml::activeTextField($this->wiedervorlage->idstranke0, "lokacija"),
            ),
            array(
                'name'=>'godište',
                'header'=>Yii::t("main","GODISTE"),
                'visible'=>true,
                'value'=>'$data->idstranke0->godisteStranke',
                'filter'=>CHtml::activeTextField($this->wiedervorlage->idstranke0, "godisteStranke"),
            ),
             array(
                'name'=>'zanimanje',
                'header'=>Yii::t("main","ZANIMANJE"),
                'visible'=>true,
                'value'=>'$data->idstranke0->zanimanjeStranke',
                'filter'=>CHtml::activeTextField($this->wiedervorlage->idstranke0, "zanimanjeStranke"),
            ),
			array(
					'name'=>'idkorisnik0.korisnickoIme',
					'header'=>Yii::t("main","AUTOR"),
					'value'=>'$data->idkorisnik0->korisnickoIme',
                                        'visible'=>false,
	),
			array(
					'name'=>'PKV_ili_VK',
					"header"=>Yii::t("main","VRSTA_TERMINA"),
					'value'=>'$data->idstranke0->pkv_vk',
					'filter'=>CHtml::activeDropDownList($this->wiedervorlage->idstranke0, 'pkv_vk', array(
							''=>'',
							'pkv'=>'PKV',
							'vk'=>'VK',
                                                        'bav'=>'BAV',
                                                        'sv'=>'SV',
					))
			),
                        array(
                            'name'=>'cijena',
                            "header"=>Yii::t("main","CIJENA"),
                            'value'=>'$data->cijena',
                            'filter'=>  CHtml::activeTextField($this->wiedervorlage, "cijena"),
                            'visible'=>Yii::app()->user->tip!="zaposlenik",
                                    ),
			array(
					'class'=>'bootstrap.widgets.TbButtonColumn',
                                        'visible'=>Yii::app()->user->tip!="makler",
					'template'=>'{view}{termin}{otkazi}{delete}',
					'viewButtonLabel'=>Yii::t("main","PREGLED"),
                                        'deleteConfirmation'=>Yii::t("main","WIEDERVORLAGE_DELETE_CONFIRMATION"),
                                    'deleteButtonLabel'=>Yii::t("main","OBRISI_WIEDERVORLAGE"),
					'viewButtonUrl'=>'Yii::app()->createUrl("wiedervorlage/view",array("id"=>"$data->idwiedervorlage"))',
					'buttons'=>array(
							'termin'=>array(
									'label'=>Yii::t("main","NAPRAVI_TERMIN"),
									'visible'=>'$data->status=="offen"',
									'imageUrl'=>Yii::app()->baseUrl.'/images/date_add.png',
									'url'=>'$data->idwiedervorlage',
									'click'=>'function(){
												var id=$(this).attr("href");
												$("#id_wv").val(id);
												$("#terminDialog").dialog("open");
												return false;
												}',
									'options'=>array(
											'style'=>'margin-left:5px;',
	
									)
							),
							'otkazi'=>array(
									'label'=>'Absage',
									'imageUrl'=>Yii::app()->baseUrl."/images/cancel2.png",
									'url'=>'$data->idwiedervorlage',
									'visible'=>'$data->status=="offen"',
									'click'=>'cancelWiedervorlage',
							)
					)
			),
	
	
	);
}
}