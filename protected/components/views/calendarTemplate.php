
      <script type="text/template" id="template-calendar">
        <div class='clndr-controls'>
       <div class='clndr-control-button'><span class='clndr-previous-button'>Zurück</span></div><div class='month'><%= month %> <%= year %></div><div class='clndr-control-button rightalign'><span class='clndr-next-button'>Nächste</span></div>
       </div>
       <table class='clndr-table' border='0' cellspacing='0' cellpadding='0'>
          <thead>
         <tr class='header-days'>
       <% for(var i = 0; i < daysOfTheWeek.length; i++) { %>
       <td class='header-day'><%= daysOfTheWeek[i] %></td>
           <% } %>
          </tr>
  </thead>
  <tbody>
  <% for(var i = 0; i < numberOfRows; i++){ %>
              <tr>
    <% for(var j = 0; j < 7; j++){ %>
     <% var d = j + i * 7; %>
    <td class='<%= days[d].classes %>'><div class='day-contents'>
    <%= days[d].day %>
     <br/>
<% _.each(days[d].events,function(event) {%>
<div class="event-display"><a href="<%=event.url%>"><span class="label label-error"><%= event.time%></span><%=" "+event.title%></a>
<%if(showDeleteButton==true) { %>
<a href="<%=event.deleteLink%>" class="deleteEvent" data-month="<%=days[d].date.month()%>" data-year="<%=days[d].date.year()%>  style="margin:2px;"><img src="<%=deleteImageLink%>" style="width:10px;height:10px;"/></a>
            <%}%>
   
</div>
<%});%>
                                 <%
   
                                    %>
    </div></td>
    <% } %>
      </tr>
      <% } %>
      </tbody>
      </table>
      </script>
      <script>
    $(function(){
    $(".deleteEvent").live('click',function(e){
            e.preventDefault();
                   e.stopPropagation();
                   $(".ui-dialog-content").dialog("close");
                                     if(showDeleteButton==true)
                                     {
                   e.preventDefault();
                   e.stopPropagation();
                if(confirm(deleteConfirmMessage))
                {
                    $.ajax({
                        url:$(this).attr("href"),
                        type:"POST",
                        success:function(r){
                            	$.ajax({
		url:ajaxUrl,
		dataType:'json',
                context:this,
		type:'GET',
                success:function(response){
          calendar.setEvents(response);
                }
            });
                        }
                    });
                }

                                             }
                                             return false;
                                 });
                             });
          </script>
      <div class="call-wrapper">
          <div id="clndr" class="cal1"></div>
      </div>
      