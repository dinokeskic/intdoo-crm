<?php

$this->widget('bootstrap.widgets.TbGridView',
		array(
				'dataProvider'=>$this->dataProvider,
				//'summaryText'=>'Prikaz {start}-{end} od ukupno {count} rezultata ',
				'type'=>'bordered',
				'id'=>'wiedervorlage-grid',
				//'emptyText'=>Yii::t("main","NEMA_REZULTATA"),
				'filter'=>$this->wiedervorlage,
				'rowCssClassExpression'=>'$data->status=="offen"?"success":"error"',
				'afterAjaxUpdate'=>'function(id,data){
				reinstallDateRangePicker("date_range_picker","wiedervorlage-grid");
				}',
				'columns'=>$this->gridColumns,
)
		);
?>
<?php $this->beginWidget('zii.widgets.jui.CJuiDialog',array(
		'id'=>'terminDialog',
		'options'=>array(
			'title'=>Yii::t("main","NOVI_TERMIN"),
				'autoOpen'=>false,
				'modal'=>true,
				'width'=>'400',
				'height'=>'300',
		)
)); ?>
	<div class="form">
<?php $form= $this->beginWidget('CActiveForm',array(
		'id'=>'terminForm',
		'enableClientValidation'=>true,
		'enableAjaxValidation'=>true,
));?>
<div class="row">
<?php echo $form->labelEx($this->termin,'vrijeme');?>
 <?php
 $now=new DateTime();
 $startDate=$now->format("Y-m-d H:i:s");
  $this->widget('bootstrap.widgets.TbDateTimePicker',array(
               'model' => $this->termin,
        'attribute' => 'vrijeme',
 		'options'=>array(
 				'startDate'=>$startDate,
 				'minuteStep'=>30,
 				'format'=>'dd.mm.yyyy hh:ii',
 		)
            ));?>
            <?php echo $form->error($this->termin,'vrijeme');?>
</div>
<div class="row">
<?php echo $form->labelEx($this->termin,'rok_za_potvrdu');?>
 <?php $this->widget('bootstrap.widgets.TbDateTimePicker',array(
               'model' => $this->termin,
        'attribute' => 'rok_za_potvrdu',
 		'options'=>array(
                               //  'startDate'=>$startDate,
 				'minuteStep'=>30,
 				'format'=>'dd.mm.yyyy hh:ii',
 		)
            ));?>
   <?php echo $form->error($this->termin,'rok_za_potvrdu');?>
</div>
<div class="row">
<?php echo $form->hiddenField($this->termin, 'idwiedervorlage',array('id'=>'id_wv'));?>
</div>
<?php echo CHtml::ajaxSubmitButton(Yii::t("main","SPREMI"),Yii::app()->createUrl('termin/create'),array(
		'type'=>'POST',
		'dataType'=>'json',
		'success'=>'function(data){
		if(data.status=="success")
		{
		$("#terminDialog").dialog("close");
		$.notify(Yii.t("main","TERMIN_DODAN"),"success");
		$.fn.yiiGridView.update("wiedervorlage-grid",{});
		}
		else
		{
		    $.each(data, function(key, val) {
                        $("#"+key+"_em_").html(val);                                                    
                        $("#"+key+"_em_").show();
                        });
		}
		}'
) );?>
<?php $this->endWidget('CActiveForm'); ?>
</div>	

<?php $this->endWidget('zii.widgets.jui.CJuiDialog'); ?>
<?php Yii::app()->clientScript->registerScript('open-termin-dialog',"
				function openTerminDialog()
				{
				$('#id_wv').val('".$this->wiedervorlage->idwiedervorlage."');
				$('#terminDialog').dialog('open');
				}
				")?>
<?php
$assetUrl=Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias("application.components"));
Yii::app()->clientScript->registerScriptFile($assetUrl."/js/reinstallDateRangePicker.js");
?>
<?php Yii::app()->clientScript->registerScript("skripta","function cancelWiedervorlage(){
		if(confirm('Absage?'))
		{
		var id=$(this).attr('href');
		$.ajax({
		type:'POST',
		url:'".Yii::app()->createUrl("wiedervorlage/cancel")."',
		data:{
			id:id,
						},
			success:function(){
							$.fn.yiiGridView.update('wiedervorlage-grid',{});
						 }
		});
		}
						return false;
		}")?>