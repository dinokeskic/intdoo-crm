<?php
$this->widget('bootstrap.widgets.TbGridView',array(
		'dataProvider'=>$this->dataProvider,
		//'summaryText'=>'Prikaz {start}-{end} od ukupno {count} rezultata ',
		'emptyText'=>Yii::t("main","NEMA_REZULTATA"),
		'type'=>'bordered',
		'id'=>'termin-grid',
		'filter'=>$this->termin,
		'rowCssClassExpression'=>'$data->getCssClass()',
		'afterAjaxUpdate'=>'function(id,data){
		reinstallDateRangePicker("vrijeme_range_picker","termin-grid");
		reinstallDateRangePicker("vrijeme_potvrde_range_picker","termin-grid");
		}',
		'columns'=>array(
	
			array(
					'name'=>'vrijeme',
					'header'=>Yii::t("main","VRIJEME"),
					'htmlOptions'=>array(
	
					),
					'value'=>'DateTime::createFromFormat("Y-m-d H:i:s",$data->vrijeme)->format("d.m.Y H:i")',
					'filter'=>$this->widget('bootstrap.widgets.TbDateRangePicker',array(
							'model'=>$this->termin,
							'attribute'=>'vrijeme',
	
							'callback'=>'js:function(start,end){
										gridID="termin-grid";
											$.fn.yiiGridView.update(gridID, {data: $.param(
        			$("#"+gridID+" .filters input, #"+gridID+" .filters select")
   								)});
												}',
							'options'=>array(
	
									'format'=>'DD.MM.YYYY hh:mm',
									'timePicker'=>true,
									'timePicker12Hour'=>false,
									'locale'=>array(
											'applyLabel'=>Yii::t("main","PRIMJENI"),
											'cancelLabel'=>Yii::t("main","OTKAŽI"),
											'fromLabel'=>Yii::t("main","OD"),
											'toLabel'=>Yii::t("main","DO"),
	
									),
	
							),
	
							'htmlOptions'=>array(
									'id'=>'vrijeme_range_picker',
	
							),
					),true),
			),
				array(
						'name'=>'rok_za_potvrdu',
						'header'=>Yii::t("main","ROK_ZA_POTVRDU"),
						'visible'=>Yii::app()->user->tip!="makler",
						'htmlOptions'=>array(
				
						),
						'value'=>'DateTime::createFromFormat("Y-m-d H:i:s",$data->rok_za_potvrdu)->format("d.m.Y H:i")',
						'filter'=>$this->widget('bootstrap.widgets.TbDateRangePicker',array(
								'model'=>$this->termin,
								'attribute'=>'rok_za_potvrdu',
				
								'callback'=>'js:function(start,end){
										gridID="termin-grid";
											$.fn.yiiGridView.update(gridID, {data: $.param(
        			$("#"+gridID+" .filters input, #"+gridID+" .filters select")
   								)});
												}',
								'options'=>array(
				
										'format'=>'DD.MM.YYYY hh:mm',
										'timePicker'=>true,
										'timePicker12Hour'=>false,
										'locale'=>array(
												'applyLabel'=>Yii::t("main","PRIMJENI"),
												'cancelLabel'=>Yii::t("main","OTKAŽI"),
												'fromLabel'=>Yii::t("main","OD"),
												'toLabel'=>Yii::t("main","DO"),
				
										),
				
								),
				
								'htmlOptions'=>array(
										'id'=>'vrijeme_potvrde_range_picker',
				
								),
						),true),
				),
				/*array(
						'name'=>'autor',
						'header'=>Yii::t("main","AUTOR"),
						'value'=>'CHtml::link($data->idwiedervorlage0->idkorisnik0->korisnickoIme,
						Yii::app()->createUrl("zaposlenik/view",array("id"=>$data->idwiedervorlage0->idkorisnik0->zaposlenik->idzaposlenik)))',
						'filter'=>CHtml::textField("autor"),
						'type'=>'raw',
						),*/
			array(
					'name'=>'status',
					'header'=>Yii::t("main","STATUS"),
					'visible'=>Yii::app()->user->isAdmin(),
					'value'=>'$data->status',
					'filter'=>CHtml::activeDropDownList($this->termin, 'status', array(
							''=>Yii::t("main","ODABERITE"),
							'offen'=>'Offen',
							'absage'=>'Absage',
					))
	
			),
			array(
					'name'=>'ime',
					'header'=>Yii::t("main","IME_PREZIME"),
					'sortable'=>true,
					'value'=>'$data->idwiedervorlage0->idstranke0->imeStranke." ".$data->idwiedervorlage0->idstranke0->prezimeStranke',
					'filter'=>CHtml::activeTextField($this->termin->idwiedervorlage0->idstranke0,'imeStranke'),
                                        'visible'=>Yii::app()->user->tip!="makler" || $this->isMaklerTermins==true,
			),
                        array(
                            'name'=>'godište',
                            'sortable'=>true,
                            'header'=>Yii::t("main","GODISTE"),
                           'value'=>'$data->idwiedervorlage0->idstranke0->godisteStranke',  
                            'filter'=>CHtml::activeTextField($this->termin->idwiedervorlage0->idstranke0,'godisteStranke'),
                        ),
                    array(
                        'name'=>'zanimanje',
                        'value'=>'$data->idwiedervorlage0->idstranke0->zanimanjeStranke',
                        'header'=>Yii::t("main","ZANIMANJE"),
                        'filter'=>CHtml::activeTextField($this->termin->idwiedervorlage0->idstranke0,'zanimanjeStranke'),
                        
                    ),
			array(
					'name'=>'prezime',
					'header'=>Yii::t("main","PREZIME"),
					'sortable'=>true,
					'visible'=>false,
					'value'=>'$data->idwiedervorlage0->idstranke0->prezimeStranke',
					'filter'=>CHtml::activeTextField($this->termin->idwiedervorlage0->idstranke0,'prezimeStranke'),
                                   
			),
			array(
					'name'=>'telefon',
					'header'=>Yii::t("main","TELEFON"),
					'value'=>'$data->idwiedervorlage0->idstranke0->telefonStranke',
					'filter'=>CHtml::activeTextField($this->termin->idwiedervorlage0->idstranke0,'telefonStranke'),
                                        'visible'=>Yii::app()->user->tip!="makler" || $this->isMaklerTermins==true,
			),
			array(
					'name'=>'adresa',
					'header'=>Yii::t("main","ADRESA"),
					'value'=>'$data->idwiedervorlage0->idstranke0->adresaStranke',
					'filter'=>CHtml::activeTextField($this->termin->idwiedervorlage0->idstranke0,'adresaStranke'),
                                        'visible'=>Yii::app()->user->tip!="makler" || $this->isMaklerTermins==true,
			),
			     array(
                    'name'=>'lokacija',
                    'header'=>Yii::t("main","LOKACIJA"),
                    'visible'=>true,
                    'value'=>'$data->idwiedervorlage0->idstranke0->lokacija',
                    'filter'=>CHtml::activeTextField($this->termin->idwiedervorlage0->idstranke0, "lokacija"),
            ),
			array(
					'name'=>'PKV_ili_VK',
					"header"=>Yii::t("main","VRSTA_TERMINA"),
					'value'=>'$data->idwiedervorlage0->idstranke0->pkv_vk',
					'filter'=>CHtml::activeDropDownList($this->termin->idwiedervorlage0->idstranke0, 'pkv_vk', array(
							''=>Yii::t("main","ODABERITE"),
							'pkv'=>'PKV',
							'vk'=>'VK',
                                                        'bav'=>'BAV',
                                                        'sv'=>'SV'
					))
			),
                       array(
                            'name'=>'cijena',
                            "header"=>Yii::t("main","CIJENA"),
                            'value'=>'$data->idwiedervorlage0->cijena',
                            'filter'=>  CHtml::activeTextField($this->termin->idwiedervorlage0, "cijena"),
                            'visible'=>Yii::app()->user->tip!="zaposlenik",
                                    ),
				array(
						'class'=>'bootstrap.widgets.TbButtonColumn',
                                    'viewButtonLabel'=>Yii::t("main","PREGLED"),
                                    'deleteConfirmation'=>Yii::t("main","TERMIN_DELETE_CONFIRMATION"),
                                    'deleteButtonLabel'=>Yii::t("main","OBRISI_TERMIN"),
						'template'=>'{view} {update} {delete} {otkazi} {potvrdi} {interested}',
                                    'updateButtonUrl'=>'array("termin/pomjeriTermin","id"=>$data->idtermin)',
                                    'updateButtonLabel'=>Yii::t("main","POMJERI_TERMIN"),
						'buttons'=>array(
								'otkazi'=>array(
										'label'=>'Absage',
										'imageUrl'=>Yii::app()->baseUrl."/images/cancel2.png",
										'url'=>'$data->idtermin',
										'visible'=>'TerminController::userIsAuthorOrAdmin($data->idtermin) &&
										$data->status=="offen"',
										'click'=>'cancelTermin',
						),
								'potvrdi'=>array(
										'label'=>'Bestätigen',
										'imageUrl'=>Yii::app()->baseUrl."/images/termin_confirmed.png",
										'url'=>'$data->idtermin',
										'visible'=>'TerminController::userIsAuthorOrAdmin($data->idtermin) &&
										$data->status=="offen"',
										'click'=>'confirmTermin',
								),
								'interested'=>array(
										'label'=>Yii::t("main","ZAINTERESOVAN"),
										'imageUrl'=>Yii::app()->baseUrl."/images/Heart-icon.png",
										'url'=>'$data->idtermin',
										'visible'=>'Yii::app()->user->tip=="makler" &&
										 !$data->assignedToCurrentUser() &&
										!$data->currentUserInterested() ',
										'click'=>'showInterestedDialog',
                                                                    ),
                                                                    'delete'=>array(
                                                                        'visible'=>'TerminController::userIsAuthorOrAdmin($data->idtermin)',
                                                                    ),
                                                                    'update'=>array(
                                                                        'visible'=>'TerminController::userIsAuthorOrAdmin($data->idtermin)',
                                                                    ),
                                                                    'view'=>array(
                                                                        'visible'=>'Yii::app()->user->tip!="makler"',
                                                                    )
                                    
					)
					)
	
	
	)
));
$assetUrl=Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias("application.components.js"));
Yii::app()->clientScript->registerScriptFile($assetUrl."/reinstallDateRangePicker.js");
 Yii::app()->clientScript->registerScript("skripta","function cancelTermin(){
		if(confirm('Absage?'))
		{
		var id=$(this).attr('href');
		$.ajax({
		type:'POST',
		url:'".Yii::app()->createUrl("termin/cancel")."',
		data:{
			id:id,
						},
			success:function(){
							$.fn.yiiGridView.update('termin-grid',{});
						 }
		});
		}
						return false;
		}");?>
  <?php Yii::app()->clientScript->registerScript("skripta2","function confirmTermin(){
		if(confirm('Bestätigen?'))
		{
		var id=$(this).attr('href');
		$.ajax({
		type:'POST',
		url:'".Yii::app()->createUrl("termin/confirm")."',
		data:{
			id:id,
						},
			success:function(){
							$.fn.yiiGridView.update('termin-grid',{});
						$.notify(Yii.t('main','TERMIN_POTVRĐEN'),'success');
						 }
		});
		}
						return false;
		}");?>
 <?php Yii::app()->clientScript->registerScript("InterestedUrl","
		var interestedUrl='".Yii::app()->createUrl("makler/interestedInTermin")."';",CClientScript::POS_HEAD);
 ?>
 <script>
     function showInterestedDialog(e)
     {
         e.preventDefault();
         $("#id_termin").val($(this).attr("href"));
         $("#maklerZainteresovanDialog").dialog("open");
     }
 </script>
 <?php
$this->beginWidget('zii.widgets.jui.CJuiDialog',array(
		'id'=>'maklerZainteresovanDialog',
		'options'=>array(
				'autoOpen'=>false,
				'modal'=>true,
				'width'=>'500',
				'height'=>'300',
		)
)); ?>
<?php $this->beginWidget("CActiveForm",array(
    'id'=>'maklerZaintersovanForma',
   
)); ?>
<?php echo CHtml::hiddenField("idtermin","",array('id'=>'id_termin')); ?>
    <?php echo CHtml::textArea("komentar","",array('placeholder'=>Yii::t("main", "KOMENTAR"),
        'id'=>'komentarArea',
        'style'=>'margin-right:10px;'
        )); ?>
<?php $this->widget("bootstrap.widgets.TbButton",array(
    'type'=>'primary',
    'buttonType'=>'button',
    'label'=>Yii::t("main","SPREMI"),
    'htmlOptions'=>array(
        'id'=>'interestedButton'
    )
))  ?>
<?php $this->endWidget(); ?>
<?php $this->endWidget() ;?>

<script>
    $(function(){
       $("#interestedButton").click(function(e){
          e.preventDefault();
          	$.ajax({
			url:interestedUrl,
			type:"POST",
			data:{idtermin:$("#id_termin").val(),
                        komentar:$("#komentarArea").val(),
            },
			success:function(response) {
                            $("#maklerZainteresovanDialog").dialog("close");
				$.fn.yiiGridView.update('termin-grid',{});
                               
			}
		});
	

	return false;
       }); 
    });
    </script>