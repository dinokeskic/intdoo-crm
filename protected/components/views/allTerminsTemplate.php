
<script type="text/template" id="template-calendar">
<div class='clndr-controls'>
<div class='clndr-control-button'><span class='clndr-previous-button'><%=Yii.t("main","PRETHODNI")%></span></div><div class='month'><%= month %> <%= year %></div><div class='clndr-control-button rightalign'><span class='clndr-next-button'><%=Yii.t("main","SLIJEDECI")%></span></div>
</div>
<table class='clndr-table' border='0' cellspacing='0' cellpadding='0'>
<thead>
<tr class='header-days'>
<% for(var i = 0; i < daysOfTheWeek.length; i++) { %>
<td class='header-day'><%= daysOfTheWeek[i] %></td>
<% } %>
</tr>
</thead>
<tbody>
<% for(var i = 0; i < numberOfRows; i++){ %>
<tr>
<% for(var j = 0; j < 7; j++){ %>
<% var d = j + i * 7; %>
<td class='<%= days[d].classes %>'><div class='day-contents'>
<%= days[d].day %>
<br/>
<% _.each(days[d].events,function(event) {%>
<div class="event-display text-success" rel="popover">
<a href="<%=event.url%>">
<span class="label label-error"><%= event.time%></span><span style="margin-left:2px;"><%= event.text %></span>
</a>
</div>
<%});%>
</div></td>
<% } %>
</tr>
<% } %>
</tbody>
</table>
</script>
 <div class="call-wrapper">
          <div id="clndr" class="cal1"></div>
      </div>