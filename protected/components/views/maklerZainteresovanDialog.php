<?php
$this->beginWidget("zii.widgets.jui.CJuiDialog",array(
    'id'=>'maklerZainteresovanDialog',
    array(
        'modal'=>true,
        'autoOpen'=>false,
        'width'=>400,
        'height'=>400,
    )
));
?>
<?php $this->beginWidget("CActiveForm",array(
    'id'=>'maklerZaintersovanForma',
   
)); ?>
<?php echo CHtml::hiddenField("idtermin","",array('id'=>'id_termin')); ?>
<div class='row'>
    <span style='margin-right: 10px;'><?php echo Yii::t("main", "KOMENTAR"); ?></span>
    <?php echo CHtml::textArea("komentar"); ?>
</div>
<?php $this->widget("bootstrap.widgets.TbButton",array(
    'type'=>'primary',
    'buttonType'=>'button',
    'htmlOptions'=>array(
        'id'=>'interestedButton'
    )
))  ?>
<?php $this->endWidget(); ?>
<?php $this->endWidget() ;?>

<script>
    $(function(){
       $("#interestedButton").click(function(e){
          e.preventDefault();
          	$.ajax({
			url:interestedUrl,
			type:"POST",
			data:$("#maklerZainteresovanForma").serialize(),
			success:function() {
				$.fn.yiiGridView.update('termin-grid',{});
			}
		});
	

	return false;
       }); 
    });
    </script>