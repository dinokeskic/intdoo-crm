<?php
$this->widget('bootstrap.widgets.TbGridView',array(
		'dataProvider'=>$this->dataProvider,
		'type'=>'bordered',
		'id'=>'terminbestetigung-grid',
		'filter'=>$this->terminbestetigung,
		'afterAjaxUpdate'=>'function(id,data){
		reinstallDateRangePicker("vrijeme_range_picker","terminbestetigung-grid");
		reinstallDateRangePicker("vrijeme_potvrde_range_picker","terminbestetigung-grid");
		}',
		'columns'=>array(

				array(
						'name'=>'vrijeme',
						'header'=>Yii::t("main","VRIJEME"),
						'htmlOptions'=>array(

						),
						'value'=>'DateTime::createFromFormat("Y-m-d H:i:s",$data->idtermin0->vrijeme)->format("d.m.Y H:i")',
						'filter'=>$this->widget('bootstrap.widgets.TbDateRangePicker',array(
								'model'=>$this->terminbestetigung->idtermin0,
								'attribute'=>'vrijeme',

								'callback'=>'js:function(start,end){
										gridID="terminbestetigung-grid";
											$.fn.yiiGridView.update(gridID, {data: $.param(
        			$("#"+gridID+" .filters input, #"+gridID+" .filters select")
   								)});
												}',
								'options'=>array(

										'format'=>'DD.MM.YYYY hh:mm',
										'timePicker'=>true,
										'timePicker12Hour'=>false,
										'locale'=>array(
												'applyLabel'=>Yii::t("main","PRIMJENI"),
												'cancelLabel'=>Yii::t("main","OTKAŽI"),
												'fromLabel'=>Yii::t("main","OD"),
												'toLabel'=>Yii::t("main","DO"),

										),

								),

								'htmlOptions'=>array(
										'id'=>'vrijeme_range_picker',

								),
						),true),
				),
				array(
						'name'=>'vrijeme_potvrde',
						'header'=>Yii::t("main","POTVRĐEN"),
						'htmlOptions'=>array(

						),
						'value'=>'DateTime::createFromFormat("Y-m-d H:i:s",$data->datum)->format("d.m.Y H:i")',
						'filter'=>$this->widget('bootstrap.widgets.TbDateRangePicker',array(
								'model'=>$this->terminbestetigung,
								'attribute'=>'datum',

								'callback'=>'js:function(start,end){
										gridID="terminbestetigung-grid";
											$.fn.yiiGridView.update(gridID, {data: $.param(
        			$("#"+gridID+" .filters input, #"+gridID+" .filters select")
   								)});
												}',
								'options'=>array(

										'format'=>'DD.MM.YYYY hh:mm',
										'timePicker'=>true,
										'timePicker12Hour'=>false,
										'locale'=>array(
												'applyLabel'=>'Primjeni',
												'cancelLabel'=>'Otkaži',
												'fromLabel'=>'Od',
												'toLabel'=>'Do',

										),

								),

								'htmlOptions'=>array(
										'id'=>'vrijeme_potvrde_range_picker',

								),
						),true),
				),
				/*array(
						'name'=>'autor',
						'header'=>Yii::t("main","AUTOR"),
						'value'=>'CHtml::link($data->idtermin0->idwiedervorlage0->idkorisnik0->korisnickoIme,
						Yii::app()->createUrl("zaposlenik/view",array("id"=>$data->idtermin0->idwiedervorlage0->idkorisnik0->zaposlenik->idzaposlenik)))',
						'filter'=>CHtml::textField("autor"),
						'type'=>'raw',
				),*/
				array(
						'name'=>'ime',
						'header'=>Yii::t("main","IME_PREZIME"),
						'sortable'=>true,
						'value'=>'$data->idtermin0->idwiedervorlage0->idstranke0->imeStranke." ".$data->idtermin0->idwiedervorlage0->idstranke0->prezimeStranke',
						'filter'=>CHtml::activeTextField($this->terminbestetigung->idtermin0->idwiedervorlage0->idstranke0,'imeStranke')
				),

				array(
						'name'=>'prezime',
						'header'=>Yii::t("main","PREZIME"),
						'sortable'=>true,
						'visible'=>false,
						'value'=>'$data->idtermin0->idwiedervorlage0->idstranke0->prezimeStranke',
						'filter'=>CHtml::activeTextField($this->terminbestetigung->idtermin0->idwiedervorlage0->idstranke0,'prezimeStranke'),
				),
				array(
						'name'=>'telefon',
						'header'=>Yii::t("main","TELEFON"),
						'value'=>'$data->idtermin0->idwiedervorlage0->idstranke0->telefonStranke',
						'filter'=>CHtml::activeTextField($this->terminbestetigung->idtermin0->idwiedervorlage0->idstranke0,'telefonStranke'),
				),
				array(
						'name'=>'adresa',
						'header'=>Yii::t("main","ADRESA"),
						'value'=>'$data->idtermin0->idwiedervorlage0->idstranke0->adresaStranke',
						'filter'=>CHtml::activeTextField($this->terminbestetigung->idtermin0->idwiedervorlage0->idstranke0,'adresaStranke'),
				),
				     array(
                    'name'=>'lokacija',
                    'header'=>Yii::t("main","LOKACIJA"),
                    'visible'=>true,
                    'value'=>'$data->idtermin0->idwiedervorlage0->idstranke0->lokacija',
                    'filter'=>CHtml::activeTextField($this->terminbestetigung->idtermin0->idwiedervorlage0->idstranke0, "lokacija"),
            ),	
				array(
						'name'=>'PKV_ili_VK',
						"header"=>Yii::t("main","VRSTA_TERMINA"),
						'value'=>'$data->idtermin0->idwiedervorlage0->idstranke0->pkv_vk',
						'filter'=>CHtml::activeDropDownList($this->terminbestetigung->idtermin0->idwiedervorlage0->idstranke0, 'pkv_vk', array(
								''=>Yii::t("main","ODABERITE"),
								'pkv'=>'PKV',
								'vk'=>'VK',
                                                                'bav'=>'BAV',
                                                                'sv'=>'SV',
						))
				),
                       array(
                            'name'=>'cijena',
                            "header"=>Yii::t("main","CIJENA"),
                            'value'=>'$data->idtermin0->idwiedervorlage0->cijena',
                            'filter'=>  CHtml::activeTextField($this->terminbestetigung->idtermin0->idwiedervorlage0, "cijena"),
                            'visible'=>Yii::app()->user->tip!="zaposlenik",
                                    ),
				array(
						'class'=>'bootstrap.widgets.TbButtonColumn',
						'template'=>'{view}{stattgefunden}{delete}',
						'viewButtonLabel'=>Yii::t("main","PREGLED"),
						'viewButtonUrl'=>'Yii::app()->createUrl("termin/view",array("id"=>$data->idtermin))',
                                                'deleteButtonUrl'=>'Yii::app()->createUrl("termin/delete",array("id"=>$data->idtermin))',
						'buttons'=>array(
								'stattgefunden'=>array(
										'label'=>Yii::t("main","TERMINSTATTGEFUNDEN"),
										'visible'=>'Yii::app()->user->tip=="admin"',
										'imageUrl'=>Yii::app()->baseUrl."/images/clock.png",
										'url'=>'$data->idterminbestitig',
										'click'=>'stattgefunden',
										'options'=>array(
												'style'=>'margin-left:5px;',
				
										)
								),
						)
				),

		)
));

?>
 <?php Yii::app()->clientScript->registerScript("skripta2","function stattgefunden(){
		if(confirm('Terminstattgefunden?'))
		{
		var id=$(this).attr('href');
		$.ajax({
		type:'POST',
		url:'".Yii::app()->createUrl("terminstattgefunden/create")."',
		data:{
			id:id,
						},
			success:function(){
							$.fn.yiiGridView.update('terminbestetigung-grid',{});
						$.notify('Terminstattgefunden','success');
						 }
		});
		}
						return false;
		}");?>
<?php 
$assetUrl=Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias("application.components.js"));
Yii::app()->clientScript->registerScriptFile($assetUrl."/reinstallDateRangePicker.js");
?>