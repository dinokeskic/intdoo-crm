<?php
$this->widget('bootstrap.widgets.TbExtendedGridView',array(
		'dataProvider'=>$this->dataProvider,
		'type'=>'bordered',
                'extendedSummary'=>array(
                    //'title'=>'Ukupno',
                    'columns'=>array(
                        'cijena'=>array('label'=>Yii::t("main","UKUPNA_CIJENA"),'class'=>'TbSumOperation'),
                    )
                ),
                'extendedSummaryOptions'=>array(
                    'class'=>'well pull-right',
                    'style'=>'width:300px;',
                ),
		'id'=>'terminstattgefunden-grid',
		'filter'=>$this->terminstattgefunden,
		'afterAjaxUpdate'=>'function(id,data){
		reinstallDateRangePicker("vrijeme_range_picker","terminbestetigung-grid");
		reinstallDateRangePicker("vrijeme_potvrde_range_picker","terminbestetigung-grid");
		}',
		'columns'=>array(

				array(
						'name'=>'vrijeme',
						'header'=>Yii::t("main","VRIJEME"),
						'htmlOptions'=>array(

						),
						'value'=>'DateTime::createFromFormat("Y-m-d H:i:s",$data->idterminbestig0->idtermin0->vrijeme)->format("d.m.Y H:i")',
						'filter'=>$this->widget('bootstrap.widgets.TbDateRangePicker',array(
								'model'=>$this->terminstattgefunden->idterminbestig0->idtermin0,
								'attribute'=>'vrijeme',

								'callback'=>'js:function(start,end){
								console.log("vrijeme je promjenjeno");
										gridID="terminstattgefunden-grid";
											$.fn.yiiGridView.update(gridID, {data: $.param(
        			$("#"+gridID+" .filters input, #"+gridID+" .filters select")
   								)});
												}',
								'options'=>array(

										'format'=>'DD.MM.YYYY hh:mm',
										'timePicker'=>true,
										'timePicker12Hour'=>false,
										'locale'=>array(
												'applyLabel'=>Yii::t("main","PRIMJENI"),
												'cancelLabel'=>Yii::t("main","OTKAŽI"),
												'fromLabel'=>Yii::t("main","OD"),
												'toLabel'=>Yii::t("main","DO"),

										),

								),

								'htmlOptions'=>array(
										'id'=>'vrijeme_range_picker',

								),
						),true),
				),
			
				/*array(
						'name'=>'autor',
						'header'=>Yii::t("main","AUTOR"),
						'value'=>'CHtml::link($data->idterminbestig0->idtermin0->idwiedervorlage0->idkorisnik0->korisnickoIme,
						Yii::app()->createUrl("zaposlenik/view",array("id"=>$data->idterminbestig0->idtermin0->idwiedervorlage0->idkorisnik0->zaposlenik->idzaposlenik)))',
						'filter'=>CHtml::textField("autor"),
						'type'=>'raw',
				),*/
				array(
						'name'=>'ime',
						'header'=>Yii::t("main","IME_PREZIME"),
						'sortable'=>true,
						'value'=>'$data->idterminbestig0->idtermin0->idwiedervorlage0->idstranke0->imeStranke." ".$data->idterminbestig0->idtermin0->idwiedervorlage0->idstranke0->prezimeStranke',
						'filter'=>CHtml::activeTextField($this->terminstattgefunden->idterminbestig0->idtermin0->idwiedervorlage0->idstranke0,'imeStranke')
				),

				array(
						'name'=>'prezime',
						'header'=>Yii::t("main","PREZIME"),
						'sortable'=>true,
						'visible'=>false,
						'value'=>'$data->idterminbestig0->idtermin0->idwiedervorlage0->idstranke0->prezimeStranke',
						'filter'=>CHtml::activeTextField($this->terminstattgefunden->idterminbestig0->idtermin0->idwiedervorlage0->idstranke0,'prezimeStranke'),
				),
				array(
						'name'=>'telefon',
						'header'=>Yii::t("main","TELEFON"),
						'value'=>'$data->idterminbestig0->idtermin0->idwiedervorlage0->idstranke0->telefonStranke',
						'filter'=>CHtml::activeTextField($this->terminstattgefunden->idterminbestig0->idtermin0->idwiedervorlage0->idstranke0,'telefonStranke'),
				),
				array(
						'name'=>'adresa',
						'header'=>Yii::t("main","ADRESA"),
						'value'=>'$data->idterminbestig0->idtermin0->idwiedervorlage0->idstranke0->adresaStranke',
						'filter'=>CHtml::activeTextField($this->terminstattgefunden->idterminbestig0->idtermin0->idwiedervorlage0->idstranke0,'adresaStranke'),
				),
				     array(
                    'name'=>'lokacija',
                    'header'=>Yii::t("main","LOKACIJA"),
                    'visible'=>true,
                    'value'=>'$data->idterminbestig0->idtermin0->idwiedervorlage0->idstranke0->lokacija',
                    'filter'=>CHtml::activeTextField($this->terminstattgefunden->idterminbestig0->idtermin0->idwiedervorlage0->idstranke0, "lokacija"),
            ),	
				array(
						'name'=>'PKV_ili_VK',
						"header"=>Yii::t("main","VRSTA_TERMINA"),
						'value'=>'$data->idterminbestig0->idtermin0->idwiedervorlage0->idstranke0->pkv_vk',
						'filter'=>CHtml::activeDropDownList($this->terminstattgefunden->idterminbestig0->idtermin0->idwiedervorlage0->idstranke0, 'pkv_vk', array(
								''=>Yii::t("main","ODABERITE"),
								'pkv'=>'PKV',
								'vk'=>'VK',
                                                                'bav'=>'BAV',
                                                                'sv'=>'SV',
						))
				),
                 array(
                        'name'=>'makler',
                        'header'=>Yii::t("main","MAKLER"),
                        'value'=>'$data->maklerUsername',
                    
                        'filter'=>CHtml::activeTextField($this->terminstattgefunden->idterminbestig0->idtermin0->assignedMakler->makler->idkorisnik0, "korisnickoIme"),
                        
                    ),
                        array(
                            'name'=>'firma',
                            'header'=>Yii::t("main","FIRMA"),
                            'value'=>'$data->firmName',
                            'filter'=>  CHtml::activeTextField($this->terminstattgefunden->idterminbestig0->idtermin0->assignedMakler->makler->firma, "naziv"),
                            
                        ), 
			   array(
                            'name'=>'cijena',
                            "header"=>Yii::t("main","CIJENA"),
                            'value'=>'$data->idterminbestig0->idtermin0->idwiedervorlage0->cijena',
                            'filter'=>  CHtml::activeTextField($this->terminstattgefunden->idterminbestig0->idtermin0->idwiedervorlage0, "cijena"),
                            'visible'=>Yii::app()->user->tip!="zaposlenik",
                                    ),
                    array(
						'class'=>'bootstrap.widgets.TbButtonColumn',
						'template'=>'{view}{delete}',
						//'viewButtonLabel'=>Yii::t("main","PREGLED"),
						'viewButtonUrl'=>'Yii::app()->createUrl("termin/view",array("id"=>$data->idterminbestig0->idtermin))',
                                                'deleteButtonUrl'=>'Yii::app()->createUrl("termin/delete",array("id"=>$data->idterminbestig0->idtermin))',
                                                //'deleteButtonLabel'=>Yii::t("main","OBRISI_TERMIN"),
				),


		)
));
$assetUrl=Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias("application.components.js"));
Yii::app()->clientScript->registerScriptFile($assetUrl."/reinstallDateRangePicker.js");
?>