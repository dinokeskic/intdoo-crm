<?php
class DateHelper {
	/*
	 * simple function that splits date range string returned by daterangepicker
	 * into array containing two date strings, beginDate and endDate.
	 * The strings are in Y-m-d H:i:s format.
	 * 
	 */
	public static function splitDateRange ($dateRangeString)
	{
		list($beginTime,$endTime)=explode("-", $dateRangeString);
		$date=DateTime::createFromFormat("d.m.Y H:i ", $beginTime);
		$beginTime=$date->format("Y-m-d H:i:s");
		$date2=DateTime::createFromFormat(" d.m.Y H:i", $endTime);
		$endTime=$date2->format("Y-m-d H:i:s");
		return array('beginTime'=>$beginTime,'endTime'=>$endTime);
	}
	/*
	 * helper function that returns date string in mysql default format which
	 * represents the beginning of the given month 
	 *
	 */
	public static function getBeginningOfMonth($month,$year)
	{
		$monthBegin=new DateTime();
		$monthBegin->setDate($_GET['year'], $_GET['month'], 1);
		$monthBegin->setTime(0, 0, 0);
		return $monthBegin->format("Y-m-d H:i:s");
	}
	/*
	 * helper function that returns date string in mysql default format which
	* represents the end of the given month
	*
	*/
	public static function getEndOfMonth($month,$year)
	{
		$monthEnd=new DateTime();
		$daysInMonth=cal_days_in_month(CAL_GREGORIAN, $_GET['month'], $_GET['year']);
		$monthEnd->setDate($year,$month,$daysInMonth);
		$monthEnd->setTime(23,59,59);
		return $monthEnd->format("Y-m-d H:i:s");
	}
     public static   function time_elapsed_string($datetime, $full = false,$periodString=array(),
             $ago="ago",$justNow="just now") {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;
if(empty($string))
{
    $string = array(
        'y' => array('year','years'),
        'm' => array('month','months'),
        'w' => array('week','weeks'),
        'd' => array('day'),
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
);}
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) ." ".$ago : $justNow;
}
}