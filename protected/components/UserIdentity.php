<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	private $_id;
	public function getId()
	{
		return $this->_id;
	}
	public function authenticate()
	{
		$korisnik=Korisnik::model()->find("korisnickoIme=?",array($this->username));
	if($korisnik===null)
	{
		$this->errorCode=self::ERROR_USERNAME_INVALID;
	
	}
	elseif ($korisnik->lozinka!==md5($this->password))
	{
		$this->errorCode=self::ERROR_PASSWORD_INVALID;
	}
	else 
	{
	$this->setState('korisnickoIme', $korisnik->korisnickoIme);
	$this->setState('tip',$korisnik->tip);
		$this->_id=$korisnik->id;
		$this->errorCode=self::ERROR_NONE;
	}
	return $this->errorCode===self::ERROR_NONE;
	}
	
}