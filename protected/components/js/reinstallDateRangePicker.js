function reinstallDateRangePicker(pickerID,gridID) {
	console.log('metoda pozvana, pickerID:'+pickerID);
    $('#'+pickerID).daterangepicker({
				'format':'DD.MM.YYYY hh:mm',
		'timePicker':true,
		'timePicker12Hour':false,
			'locale':{
							'applyLabel':Yii.t("main","PRIMJENI"),
							'cancelLabel':Yii.t("main","OTKAŽI"),
							'fromLabel':Yii.t("main","OD"),
							'toLabel':Yii.t("main","DO"),
															
										}
		},function(start,end){
											$.fn.yiiGridView.update(gridID, {data: $.param(
        			$('#'+gridID+' .filters input, #'+gridID+' .filters select')
   								)});
		});
}