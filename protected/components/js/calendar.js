    var calendar=null;
$(document).ready(function(){
    moment.lang("de");
    var date=new Date();

    console.log(ajaxUrl);
    console.log("THis has been started!");
    var month=date.getMonth()+1;
    var year=date.getFullYear();
	$.ajax({
		url:ajaxUrl,
		data:{
			month:month,
			year:year
		},
		dataType:'json',
                context:this,
		type:'GET',
		success:function(response)
		{
			calendar=$('#clndr').clndr({
				template:$("#template-calendar").html(),
				  events:response,
				  dateParameter: 'date',
				  clickEvents: {
				    click: dayClickEvent,
				    onMonthChange: function(month) {
				    calendar=this;
				    }
				  },
				  doneRendering: function() {
				 
				  }
				});
		}
	
	});

		        
});