<?php
class TerminbestetigungGridWidget extends CWidget {
	public $terminbestetigung;
	public $terminstattgefunden;
	public $dataProvider;
	public $showMaklerColumn=false;
	public function run()
	{
		Yii::setPathOfAlias("bootstrap",
		Yii::getPathOfAlias('application.extensions.bootstrap'));
		new JsTrans("main", "de");
		$this->render('terminbestetigung');
	}
}