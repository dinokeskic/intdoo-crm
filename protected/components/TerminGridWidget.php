<?php
class TerminGridWidget extends CWidget {
	public $termin;
	public $dataProvider;
	public $showMaklerColumn=false;
        public $isMaklerTermins=false;
	public function run()
	{
		Yii::setPathOfAlias("bootstrap",
		Yii::getPathOfAlias('application.extensions.bootstrap'));
		new JsTrans("main", "de");
		$this->render('termin');
	}
}