<?php

class WebUser extends CWebUser {
	private $_model=null;
	public $korisnickoIme='';
	public $menu=array();
	public function loadUser($korisnickoIme)
	{

		$this->_model=Korisnik::model()->find("korisnickoIme=?",array($korisnickoIme));
	}
	public function getKorisnickoIme()
	{
		return $this->korisnickoIme;
	}
	public function getMenu()
	{
		if($this->isGuest)
		{
			return array();
		}
		else
		{
			if(Yii::app()->user->tip=="zaposlenik")
			{
				return array(
						array('label'=>Yii::t("main","POCETNA"),'url'=>array('korisnik/pocetna')),
						array('label'=>Yii::t("main","BROKERI"),'url'=>array('makler/admin')),
						array('label'=>Yii::t("main","PREDLOSCI"),'url'=>array('wiedervorlage/index',
								'korisnik'=>Yii::app()->user->name,
				)),
								
						array('label'=>Yii::t("main","TERMINI"),'url'=>array('termin/index')),
						array('label'=>Yii::t("main","PROMJENA_LOZINKE"),'url'=>array('korisnik/promjenaLozinke','korisnik'=>Yii::app()->user->name)),
						array('label'=>Yii::t("main","ODJAVA"),'url'=>array('/korisnik/logout')),
				);
			}
			elseif(Yii::app()->user->tip=="admin")
			{
				Yii::log("returned admin");
				return array(   
						array('label'=>Yii::t("main","POCETNA"),'url'=>array('korisnik/pocetna')),
                                                array('label'=>Yii::t("main","KALENDAR_TERMINA"),'url'=>array('korisnik/kalendar')),
						array('label'=>Yii::t("main","BROKERI"),'url'=>array('/makler/admin')),
                                                array('label'=>Yii::t("main","FIRMA"),'url'=>array('/firma/admin')),
						array('label'=>Yii::t("main","PREDLOSCI"),'url'=>Yii::app()->createUrl('wiedervorlage/index',array(
								'korisnik'=>Yii::app()->user->name,
				))),
						array('label'=>Yii::t("main","TERMINI"),'url'=>Yii::app()->createUrl('termin/index')),
						array('label'=>"Terminbestetigung",'url'=>array('terminbestetigung/index')),
						array('label'=>"Terminstattgefunden",'url'=>array('terminstattgefunden/index')),
						array('label'=>Yii::t("main","ZAPOSLENICI"),'url'=>array("zaposlenik/admin")),
						array('label'=>Yii::t("main","PROMJENA_LOZINKE"),'url'=>array('korisnik/promjenaLozinke',
								'korisnik'=>Yii::app()->user->name,
						)),
						array('label'=>Yii::t("main","ODJAVA"),'url'=>array('korisnik/logout'))
				);
			}elseif(Yii::app()->user->tip=="posrednik")
                        {
                            return array(  
                                array('label'=>Yii::t("main","BROKERI"),'url'=>array('/makler/admin')),
                                    array('label'=>Yii::t("main","TERMINI"),'url'=>Yii::app()->createUrl('termin/index')),
                                
                                    );
                        }
			else
			{
				Yii::log("returned else");
					
				return  array(
                                                 array('label'=>Yii::t("main","SVI_TERMINI"),'url'=>array('termin/index')),
                                                 array('label'=>Yii::t("main","SVI_WIEDERVORLAGE"),'url'=>array('wiedervorlage/all')),
                                    		array('label'=>Yii::t("main","MOJI_TERMINI"),'url'=>array('makler/mojiTermini')),
						array('label'=>Yii::t("main","MOJ_KALENDAR"),'url'=>array('korisnik/pocetnaMaklera')),
                                               

						array('label'=>Yii::t("main","ODJAVA"),'url'=>array('korisnik/logout')),
				);
			}
		}

	}
	public function getHomeUrl()
	{
		Yii::log("it's called motherfaken bic");
		if($this->isGuest)
		{
			Yii::log("Rijec je o gostu");
			return '';
		}
		else
		{
			if(Yii::app()->user->tip=="zaposlenik")
			{
				Yii::log("home je zaposlenik");
				return 'korisnik/pocetna';
			}
			elseif(Yii::app()->user->tip=="admin")
			{
				Yii::log("home je admin");
				return 'korisnik/pocetna';
			}
                        elseif(Yii::app()->user->tip=="posrednik")
			{
				Yii::log("home je admin");
				return 'korisnik/pocetna';
			}
			else
			{
				Yii::log("korisnik je makler");
					
				return  'korisnik/pocetnaMaklera';
			}
		}
	}
	/*
	 * Checks if user is application administrator.
	 */
	public function isAdmin()
	{
		return Yii::app()->user->tip=="admin";
	}
        public function isAdminOrPosrednik()
        {
            return Yii::app()->user->tip=="admin" || Yii::app()->user->tip=="posrednik";
        }
	/*
	 * Checks if user is a makler
	 */
	public function isMakler()
	{
		return Yii::app()->user->tip=="makler";
	}
        public function notificationsCount()
        {
            if($this->isGuest)
                return 0;
            else{
            $korisnik=  Korisnik::model()->findByPk(Yii::app()->user->id);
            if($korisnik!=null)
                return $korisnik->notifikacijeCount;
            else
                return 0;
            }
           
        }
	/**
	 * Performs a login
	 * @return true if login successful, false othervise
	 */

}
