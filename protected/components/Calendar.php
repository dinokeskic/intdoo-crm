<?php
class Calendar extends CWidget {
	public $ajaxUrl;
	public $template="calendarTemplate";
        public $dayClickEvent=false;
        public $showDeleteButton=false;
        public $deleteImageLink="";
        public $deleteConfirmMessage="";
	public function run()
	{
            $locale=Yii::app()->locale->id;
		Yii::app()->clientScript->registerScript("ajax-url","
				ajaxUrl='".$this->ajaxUrl."'; var locale='"
                        .$locale. "';",CClientScript::POS_HEAD);
                new JsTrans("main", $locale);
                if($this->dayClickEvent)
                {
                Yii::app()->clientScript->registerScript("clickAction",
                        "dayClickEvent=".$this->dayClickEvent,  CClientScript::POS_HEAD);
                }
                else
                {
                    Yii::app()->clientScript->registerScript("clickAction",
                            "dayClickEvent=function(target){"
                            . "console.log('metoda je pozvana');};",  CClientScript::POS_HEAD);
                }
		$jsAssetUrl=Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias("application.components.js"));
		Yii::app()->clientScript->registerScriptFile($jsAssetUrl."/moment-2.2.1.js");
		Yii::app()->clientScript->registerScriptFile($jsAssetUrl."/underscore.js");
		$clndrAssetUrl=Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias("application.components.clndr"));
		Yii::app()->clientScript->registerScriptFile($clndrAssetUrl."/clndr.js");
		Yii::app()->clientScript->registerCssFile($clndrAssetUrl."/clndr.css");
       Yii::app()->bootstrap->registerPopover();
       if($this->showDeleteButton==true)
       {
           Yii::app()->clientScript->registerScript("showDeleteButton",
                   "var showDeleteButton=true;".
                   "var deleteImageLink='".$this->deleteImageLink."';".
                   "var deleteConfirmMessage='".$this->deleteConfirmMessage."';",  CClientScript::POS_HEAD);
       }
       else {
           Yii::app()->clientScript->registerScript("showDeleteButton",
                   "var showDeleteButton=false;",  CClientScript::POS_HEAD);
       }
		Yii::app()->clientScript->registerScriptFile($jsAssetUrl."/calendar.js");
		$this->render($this->template,array(),false);
	}
}