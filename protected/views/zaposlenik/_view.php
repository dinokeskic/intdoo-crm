<?php
/* @var $this ZaposlenikController */
/* @var $data Zaposlenik */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('idzaposlenik')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idzaposlenik), array('view', 'id'=>$data->idzaposlenik)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ime')); ?>:</b>
	<?php echo CHtml::encode($data->ime); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('prezime')); ?>:</b>
	<?php echo CHtml::encode($data->prezime); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idkorisnik')); ?>:</b>
	<?php echo CHtml::encode($data->idkorisnik); ?>
	<br />


</div>