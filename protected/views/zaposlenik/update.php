<?php
/* @var $this ZaposlenikController */
/* @var $model Zaposlenik */

$this->breadcrumbs=array(
	'Zaposlenici'=>array('index'),
	$zaposlenik->ime." ".$zaposlenik->prezime=>array('view','id'=>$zaposlenik->idzaposlenik),
	Yii::t("main","AZURIRANJE"),
);

$this->menu=array(
	array('label'=>Yii::t("main","ZAPOSLENIK_DODAJ"), 'url'=>array('create')),
	array('label'=>Yii::t("main","ZAPOSLENIK_PREGLED"), 'url'=>array('view', 'id'=>$zaposlenik->idzaposlenik)),
	array('label'=>Yii::t("main","ZAPOSLENIK_UPRAVLJANJE"), 'url'=>array('admin')),
);
?>

<?php $this->renderPartial('_form', array('zaposlenik'=>$zaposlenik,
		'korisnik'=>$korisnik,
)); ?>