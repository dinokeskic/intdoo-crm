<?php
/* @var $this ZaposlenikController */
/* @var $model Zaposlenik */
Yii::setPathOfAlias("bootstrap",
Yii::getPathOfAlias('application.extensions.bootstrap'));
$this->menu=array(
	array('label'=>Yii::t("main","ZAPOSLENIK_DODAJ"), 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#zaposlenik-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<?php $this->renderPartial('rightMenu');?>
<br/>
<hr>
<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'zaposlenik-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'ime',
         'prezime',
			array(
			'name'=>'idkorisnik0.korisnickoIme',
			'filter'=>CHtml::activeTextField($model->idkorisnik0, 'korisnickoIme'),
),
        array(
			'name'=>'idkorisnik0.tip',
			'filter'=>CHtml::activeDropDownList($model->idkorisnik0, 'tip',array(
							''=>Yii::t("main","ODABERITE"),
						'zaposlenik'=>'zaposlenik',
						'admin'=>'admin',
					)),
),
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
            'deleteConfirmation'=>Yii::t("main","ZAPOSLENIK_POTVRDA_BRISANJA"),
            'updateButtonLabel'=>Yii::t("main","ZAPOSLENIK_AZURIRAJ"),
            'deleteButtonLabel'=>Yii::t("main","ZAPOSLENIK_BRISANJE"),
             'viewButtonLabel'=>Yii::t("main","ZAPOSLENIK_PREGLED"),
      
		),
	),
)); ?>
