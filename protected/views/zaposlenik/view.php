<?php
/* @var $this ZaposlenikController */
/* @var $model Zaposlenik */

Yii::setPathOfAlias("bootstrap",
Yii::getPathOfAlias('application.extensions.bootstrap'));
$cs=Yii::app()->clientscript;
   $cs->registerScriptFile( Yii::app()->theme->baseUrl . '/js/bootstrap-dropdown.js', CClientScript::POS_END );

$this->breadcrumbs=array(
	Yii::t("main","MAKLERI")=>array('admin'),
	$model->idkorisnik0->korisnickoIme,
        
);

?>

<h1><?php echo $model->ime." ".$model->prezime."          ";?> 
<?php
$this->widget(
        'bootstrap.widgets.TbButtonGroup', 
        array(
            'size'=>'medium',
            
            'buttons'=> array(
                array(
                'label'=>'',
                'items'=>array(
                    array('label'=>Yii::t("main","ZAPOSLENIK_DODAJ"), 'url'=>array('create')),
                    array('label'=>Yii::t("main","ZAPOSLENIK_AZURIRAJ"), 'url'=>array('update', 'id'=>$model->idzaposlenik)),
                    array('label'=>Yii::t("main","PROMJENA_LOZINKE"),'url'=>array('korisnik/promjenaLozinke','korisnik'=>$model->idkorisnik0->korisnickoIme)),
                    array('label'=>Yii::t("main","WIEDERVORLAGE"),'url'=>array('wiedervorlage/index','korisnik'=>$model->idkorisnik0->korisnickoIme)),

                    '---',
                    array('label'=>Yii::t("main","ZAPOSLENIK_BRISANJE"),'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->idzaposlenik),'confirm'=>Yii::t("main","ZAPOSLENIK_POTVRDA_BRISANJA"))),
                ),
            ),
           ),
        )
        );
?>

</h1>

<br>


<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ime',
		'prezime',
		'idkorisnik0.korisnickoIme',
       'idkorisnik0.tip',
	),
));
		?>
