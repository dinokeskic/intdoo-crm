<?php
/* @var $this ZaposlenikController */
/* @var $model Zaposlenik */

$this->breadcrumbs=array(
	Yii::t("main","ZAPOSLENICI")=>array('admin'),
	Yii::t("main","DODAJ"),
);

$this->menu=array(
	array('label'=>'List Zaposlenik', 'url'=>array('index')),
	array('label'=>'Manage Zaposlenik', 'url'=>array('admin')),
);
?>

<h1>Create Zaposlenik</h1>

<?php $this->renderPartial('_form', array('korisnik'=>$korisnik,'zaposlenik'=>$zaposlenik)); ?>