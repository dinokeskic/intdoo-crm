<?php
/* @var $this ZaposlenikController */
/* @var $model Zaposlenik */
/* @var $form CActiveForm */
?>


<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'zaposlenik-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
		'htmlOptions'=>array(
		
)
)); ?>
	<div class="row">
		<?php echo $form->labelEx($korisnik,'korisnickoIme'); ?>
		<?php echo $form->textField($korisnik,'korisnickoIme',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($korisnik,'korisnickoIme'); ?>
	</div>
	<?php if(!($zaposlenik->scenario=="update")):?>
		<div class="row">
		<?php echo $form->labelEx($korisnik,'lozinka'); ?>
		<?php echo $form->passwordField($korisnik,'lozinka'); ?>
		<?php echo $form->error($korisnik,'lozinka'); ?>
	</div>
		<div class="row">
		<?php echo $form->labelEx($korisnik,'lozinka2'); ?>
		<?php echo $form->passwordField($korisnik,'lozinka2'); ?>
		<?php echo $form->error($korisnik,'lozinka2'); ?>
	</div>
	<?php endif;?>
		<div class="row">
		<?php echo $form->labelEx($korisnik,'tip'); ?>
		<?php echo $form->dropDownList($korisnik, 'tip', array(
		''=>Yii::t("main","TIP_KORISNIKA"),
		'zaposlenik'=>Yii::t("main","KORISNIK_ZAPOSLENIK"),
	    "admin"=>Yii::t("main","KORISNIK_ADMINISTRATOR"),
                  "posrednik"=>Yii::t("main","KORISNIK_POSREDNIK"),
)); ?>
		<?php echo $form->error($korisnik,'tip'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($zaposlenik,'ime'); ?>
		<?php echo $form->textField($zaposlenik,'ime'); ?>
		<?php echo $form->error($zaposlenik,'ime'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($zaposlenik,'prezime'); ?>
		<?php echo $form->textField($zaposlenik,'prezime',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($zaposlenik,'prezime'); ?>
	</div>
    

	<div class="row buttons">
		<?php echo CHtml::submitButton($zaposlenik->isNewRecord ? Yii::t("main","SACUVAJ") : Yii::t("main","SACUVAJ")); ?>
	</div>

<?php $this->endWidget(); ?>