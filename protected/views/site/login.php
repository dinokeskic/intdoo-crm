<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Login';
$this->breadcrumbs=array(
	'Login',
);
?>
<h1>Login</h1>
<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>
<br>
<br>
<br>
<br>
	<div class="row">
		<?php echo $form->labelEx($korisnik,'korisnickoIme'); ?>
		<?php echo $form->textField($korisnik,'korisnickoIme'); ?>
		<?php echo $form->error($korisnik,'korisnickoIme'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($korisnik,'lozinka'); ?>
		<?php echo $form->passwordField($korisnik,'lozinka'); ?>
		<?php echo $form->error($korisnik,'lozinka'); ?>

	</div>

	<div class="row rememberMe">
            <table>
                <tr>
                    <td>
		<?php echo $form->label($korisnik,'zapamtiMe'); ?>
                    </td>
                    <td>
		<?php echo $form->checkBox($korisnik,'zapamtiMe'); ?>
                    </td>
                </tr>
            </table>
	
		<?php echo $form->error($korisnik,'zapamtiMe'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton(Yii::t("main","LOGIN")
				); ?>
	</div>

<?php $this->endWidget(); ?>
</div>