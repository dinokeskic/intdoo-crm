<?php
/* @var $this TerminController */
?>
<h1><?php echo Yii::t("main","SVI_TERMINI");?></h1>
<?php $this->widget('application.components.TerminGridWidget',array(
		'termin'=>$termin,
		'dataProvider'=>$termin->search(),
		'showMaklerColumn'=>true,
));?>