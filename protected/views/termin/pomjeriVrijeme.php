<?php
Yii::setPathOfAlias("bootstrap", Yii::getPathOfAlias("ext.bootstrap"));
$form=$this->beginWidget("bootstrap.widgets.TbActiveForm",array(
    'id'=>'pomjeriVrijeme-form',
    'htmlOptions'=>array(
        'class'=>'well',
    )
));
?>
<?php echo $form->datetimepickerRow($termin,"vrijeme",array(
    'placeholder'=>Yii::t("main","TERMIN_NOVO_VRIJEME"),
    'class'=>'input-medium',
   // 'prepend'=>'<i class="icon-calendar"> </i>',
)); ?>
<?php echo $form->datetimepickerRow($termin,'rok_za_potvrdu',array(
    'class'=>'input-medium',
        //'prepend'=>'<i class="icon-calendar"> </i>',
)); ?>
<?php $this->widget("bootstrap.widgets.TbButton",array(
   'buttonType'=>'submit',
    'type'=>'primary',
    'label'=>Yii::t("main","SPREMI"),
    'htmlOptions'=>array(
        'style'=>'margin-left:10px;',
    )
    
));
?>
<?php $this->endWidget("bootstrap.widgets.TbActiveForm"); ?>