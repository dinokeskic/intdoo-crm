<div class="comment">
<div class="author">
<?php echo $data->idwiedervorlage0->idkorisnik0->korisnickoIme;?>
<?php $img=CHtml::image(Yii::app()->baseUrl."/images/delete.png",'',array(
		'style'=>'width:8px;height:8px;'
));
   echo CHtml::ajaxLink("Löschen", array('komWv/delete'),
   		array(
		'type'=>'POST',
   		'data'=>array('id'=>$data->idkom_wv),
		 'success'=>'function(){$.fn.yiiListView.update("comments-list",{});
}',
),array(
	'class'=>'pull-right action-delete',
	'confirm'=>'Löschen?',
	'id'=>$data->idkom_wv,
)
   		);
?>
</div>

<div class="time">
<?php echo DateTime::createFromFormat("Y-m-d H:i:s",$data->datum)->format('d.M.Y H:i');
?>
</div>
<div class="comment-content">
<?php echo CHtml::encode($data->text);?>
</div>
</div>
<?php  Yii::app()->clientScript->registerScript('reinstall_links_after_ajax',
'$(document).ajaxComplete(function(){
				$(".action-delete").unbind("click");
		$(".action-delete").live("click",function(e){
		e.preventDefault();
		var self=$(this);
		if(confirm("Löschen?"))
		{
		$.ajax({
		type:"POST",
		data:{
		id:self.attr("id"),
		},
   		success:function(){
   		$.fn.yiiListView.update("comments-list",{});
   		},
   		url:"'.Yii::app()->createUrl("komWv/delete").'"
   		
		});
		}
		});
		});'
);?>