<?php
Yii::app()->bootstrap->registerPlugin("notify");
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl."/js/maklerSelection.js",
		CClientScript::POS_HEAD);
new JsTrans("main", "bs");
$select2Url=Yii::app()->assetManager->publish(Yii::getPathOfAlias("ext.select2"));
Yii::app()->clientScript->registerScriptFile($select2Url."/select2.js");
Yii::app()->clientScript->registerCssFile($select2Url."/select2.css");
$this->beginWidget("zii.widgets.jui.CJuiDialog",array(
		'id'=>'assignMaklerDialog',
		'options'=>array(
				'autoOpen'=>false,
				'modal'=>true,
				'width'=>400,
				'height'=>300,
				'show'=>array(
						'effect'=>'fadeIn',
						'duration'=>500,
					),
				'hide'=>array(
						'effect'=>'explode',
						'duration'=>500,
				)
)
)); ?>
 <?php $form= $this->beginWidget("CActiveForm",array(
 		'enableAjaxValidation'=>true,
 		'id'=>'formica_mala',
 ));?>
 <div class="offset1 row">
 <?php echo CHtml::activeLabel($mot, "idmakler");?>
 <?php echo CHtml::activeTextField($mot, "idmakler",array(
 		"id"=>"idmaklerSelect",
 		
 		
 ));?>
 <?php echo $form->error($mot,"idmakler",array('style'=>'margin-top:10px; margin-bottom:10px;'));?>
 </div>
 <br/>
 <?php echo CHtml::activeHiddenField($mot,"idtermin",array("value"=>$model->idtermin));
 ?>
 <?php $this->widget("bootstrap.widgets.TbButton",array(
 		'type'=>'primary',
 		'label'=>Yii::t("main","DODIJELI_MAKLERA"),
 		'buttonType'=>'ajaxButton',
 		'url'=>Yii::app()->createUrl("termin/assignMakler"),
 		'ajaxOptions'=>array(
 				'type'=>'POST',
				'dataType'=>'json',
				'success'=>'js:displayErrorsOrNotifications',

 ),
		'htmlOptions'=>array(
				'class'=>'offset1',
				)
 ));?>
 <?php $this->endWidget();?>
 <?php $this->endWidget();?>
 <?php
 Yii::app()->clientScript->registerScript("allMaklers","var allMaklers=".
 json_encode(Makler::allMaklersArray()),CClientScript::POS_HEAD);
 ?>
 <script>

 </script>