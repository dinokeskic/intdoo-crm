<?php
Yii::setPathOfAlias("bootstrap",
Yii::getPathOfAlias('application.extensions.bootstrap'));
new JsTrans("main","bs");
?>
<?php Yii::app()->clientScript->registerScript("assign_makler_script",
"var assignMaklerUrl='".Yii::app()->createUrl("termin/assignMakler")."';",
CClientScript::POS_HEAD);
new JsTrans("main", "de");
?>
<div class="row">
    <?php
$this->widget(
        'bootstrap.widgets.TbButtonGroup', 
        array(
            'size'=>'small',
            'type'=>'info',
            'htmlOptions'=>array(
				'class'=>'pull-left',
            		'style'=>'margin:10px;'
				),
            'buttons'=> array(
                array(
		'type'=>'info',
                    'visible'=>(Yii::app()->user->isAdmin() || Yii::app()->user->tip=="posrednik") &&
                                $model->assignedMakler==null,
		'label'=>Yii::t("main","DODIJELI_MAKLERA"),
		'buttonType'=>'button',
		'htmlOptions'=>array(
				'id'=>'displayDialogButton',
                    ),
                    ),
                 array(
                     'label'=>Yii::t("main","OBRISI_TERMIN"),
                     'visible'=>$model->currentUserIsAuthor() || Yii::app()->user->tip=="admin",
                     'url'=>'#','buttonType'=>'button',
                     'htmlOptions'=>array(
                         'id'=>'deleteButton',
                         'class'=>'deleteButton'
                         ),
                     
                     ),
                array(
                'label'=>'',
                    'visible'=>$model->currentUserIsAuthor() || Yii::app()->user->tip=="admin",
                'items'=>array(
                    
                    array('label'=>Yii::t("main","POMJERI_TERMIN"), 'url'=>array('pomjeriTermin', 'id'=>$model->idtermin)),
                    array('label'=>Yii::t("main","AZURIRAJ"),'url'=>array('wiedervorlage/update','id'=>$model->idwiedervorlage0->idwiedervorlage)),
                   
                    

                    '---',
                    
                ),
            ),
                
           ),
        )
        );
$deleteUrl=Yii::app()->createUrl("termin/delete",array('id'=>$model->idtermin));
$redirectUrl=Yii::app()->createUrl("termin/index");
Yii::app()->clientScript->registerScript("deleteURLs"," var deleteUrl='".$deleteUrl."';".
        "var redirectUrl='".$redirectUrl."';",
        CClientScript::POS_HEAD);

?>
    <script>
        $(function(){
    $(".deleteButton").click(function(e){
        e.preventDefault();
       if(confirm('Löschen?'))
       {
           $.ajax({
              url:deleteUrl,
              type:"POST",
              success:function()
              {
               location.assign(redirectUrl);
              }
           });
       }
       return false;
    });
    });
    </script>
</div>
<div class="row termin-view-wrapper">

<div class="span5 offset1">
    
<?php

$this->widget('bootstrap.widgets.TbDetailView',array(
		'type'=>'bordered stripped',
		'data'=>$model,
		'attributes'=>array(
				array(
						'label'=>Yii::t("main","VRIJEME_TERMINA"),
						'value'=>DateTime::createFromFormat("Y-m-d H:i:s", $model->vrijeme)->format("d.m.Y H:i"),
					
		),
					array(
						'label'=>Yii::t("main","ROK_ZA_POTVRDU"),
						'value'=>DateTime::createFromFormat("Y-m-d H:i:s", $model->rok_za_potvrdu)->format("d.m.Y H:i"),
						'visible'=>$model->status=="offen" && !Yii::app()->user->isMakler(),
							),
					
				array(
						'label'=>Yii::t("main","AUTOR"),
						'value'=>$model->idwiedervorlage0->idkorisnik0->korisnickoIme,
						'type'=>'raw',
						'visible'=>Yii::app()->user->isAdmin(),
		),
                    array(
                        'label'=>Yii::t("main","CIJENA"),
                        'value'=>$model->idwiedervorlage0->cijena,
                        'visible'=>Yii::app()->user->tip!="zaposlenik",
                    ),
				array(
					'label'=>Yii::t("main",Yii::t("main","STATUS")),
					'value'=>$model->status,
					'visible'=>Yii::app()->user->isAdmin(),	
				),
			array(
					'label'=>Yii::t("main","TIP_TERMINA"),
					'value'=>$model->idwiedervorlage0->idstranke0->pkv_vk,
					'visible'=>!Yii::app()->user->isMakler(),
		),
				array(
						'label'=>Yii::t("main","IME_STRANKE"),
						'value'=>$model->idwiedervorlage0->idstranke0->imeStranke,
                                     'visible'=>Yii::app()->user->tip!="makler" || $model->assignedToCurrentUser(),
						),
				array(
						'label'=>Yii::t("main","PREZIME_STRANKE"),
						'value'=>$model->idwiedervorlage0->idstranke0->prezimeStranke,
                                     'visible'=>Yii::app()->user->tip!="makler" || $model->assignedToCurrentUser(),
			),
				array(
						'label'=>Yii::t("main","ZANIMANJE_STRANKE"),
						'value'=>$model->idwiedervorlage0->idstranke0->zanimanjeStranke,
			),
				array(
						'label'=>Yii::t("main","GODISTE_STRANKE"),
						'value'=>$model->idwiedervorlage0->idstranke0->godisteStranke,
			),
			array(
					'label'=>Yii::t("main","ADRESA_STRANKE"),
					'value'=>$model->idwiedervorlage0->idstranke0->adresaStranke,
                                     'visible'=>Yii::app()->user->tip!="makler" || $model->assignedToCurrentUser(),
			),
                    array(
                        'label'=>Yii::t("main","TELEFON_STRANKE"),
                        'value'=>$model->idwiedervorlage0->idstranke0->telefonStranke,
                        'visible'=>Yii::app()->user->tip!="makler" || $model->assignedToCurrentUser(),
                    )
			
),
		'htmlOptions'=>array(
				'style'=>'margin:10px;',
				)
));

?>
</div>
<?php if(Yii::app()->user->isAdmin() || Yii::app()->user->tip=="posrednik"):?>
<div class="offset1 termin-maklers span3">
<div class="well span12">
<h4><?php echo Yii::t("main","MAKLER");?></h4>
<div id="assignedMaklerLink">
<?php if($model->assignedMakler!=null):?>
<?php echo CHtml::link($model->assignedMakler->makler->idkorisnik0->korisnickoIme,array(
		'makler/view',
		'id'=>$model->assignedMakler->idmakler,
));?>
    <?php $this->widget("bootstrap.widgets.TbButton",array(
        'size'=>'small',
        'type'=>'danger',
        'icon'=>'remove',
        'buttonType'=>'button',
        'htmlOptions'=>array(
            'id'=>'ponistiMakleraButton',
        )
    )); ?>
<?php endif;?>
</div>
</div>
<div class="well interested-maklers span12">
<h4><?php echo Yii::t("main","ZAINTERESOVANI_MAKLERI");?></h4>
<?php 
$this->widget('zii.widgets.CListView',array(
'id'=>'interestedMaklers-list',
'dataProvider'=>$model->interestedMaklersProvider(),
'itemView'=>'interestedMaklersView',
'viewData'=>array('isMaklerAssigned'=>$model->isMaklerAssignedString()),
'summaryText'=>'',
'enablePagination'=>true,
'htmlOptions'=>array(
'class'=>'pull-right',
)
))
?>
</div>
</div>
<?php  endif;?>
</div>

<?php if(Yii::app()->user->tip!="makler"):?>
<div class="form">

<?php $form = $this->beginWidget('CActiveForm',array(
		'id'=>'addComment-form',
		'enableAjaxValidation'=>false,
		'htmlOptions'=>array(),
));?>

    <table align="center">
        <tr>
            <td style="margin-left: 20px;" colspan="2">
<?php echo $form->textArea($komentar,'text',array(
		'id'=>'commentText',
		'rows'=>4,
		'cols'=>10,
		'maxlength'=>1000,
		'placeholder'=>Yii::t("main","UNESITE_KOMENTAR"),
		'style'=>'width:500px;margin-right:10px;'
));?>
            </td>
        </tr>
            <tr>
                <td style="width: 305px;"></td>
            <td>
<?php echo $form->hiddenField($komentar,'idwiedervorlage');?>
            
<?php echo CHtml::ajaxSubmitButton(Yii::t("main","SPREMI"),array('komWv/create'),array(
		'type'=>'POST',
		'beforeSend'=>'function(){}',
		'success'=>'function(data){
		$("#commentText").val("");
		$.fn.yiiListView.update("comments-list",{});
}',
));?>
            </td>
        </tr>
    </table>
    <?php $this->endWidget();?>
</div>
<?php $this->renderPartial("assignMaklerDialog",array('model'=>$model,'mot'=>$mot));?>

<?php $this->widget('zii.widgets.CListView',array(
		'id'=>'comments-list',
		'dataProvider'=>$dataProvider,
		'itemView'=>'commentView',
		'emptyText'=>Yii::t("main","NEMA_REZULTATA"),
		'enablePagination'=>true,
		'summaryText'=>'',
		'htmlOptions'=>array('style'=>'width:80%;margin-left:20%'),
));?>
<?php endif;?>
<?php Yii::app()->clientScript->registerScript("ponistiMakleraUrl","var ponistiMakleraUrl='".
        Yii::app()->createUrl("termin/ponistiMaklera",array("id"=>$model->idtermin))."';",  CClientScript::POS_HEAD); ?>
<script>
$(function(){
	$("#displayDialogButton").click(function(){
		$("#assignMaklerDialog").dialog("open");
	});
     $("#ponistiMakleraButton").click(function(e){
        e.preventDefault();
        if(confirm(Yii.t("main","PONISTI_MAKLERA_CONFIRM")))
        {
        $.ajax( {
            url:ponistiMakleraUrl,
            dataType:"GET",
           complete:function(response)
            {
                console.log("success pozvan");
                location.reload();
            }
        });
    }
     });

});
</script>