<?php
/* @var $this WiedervorlageController */
/* @var $data Wiedervorlage */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('idwiedervorlage')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idwiedervorlage), array('view', 'id'=>$data->idwiedervorlage)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idstranke')); ?>:</b>
	<?php echo CHtml::encode($data->idstranke); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idmakler')); ?>:</b>
	<?php echo CHtml::encode($data->idmakler); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idkorisnik')); ?>:</b>
	<?php echo CHtml::encode($data->idkorisnik); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vrijeme')); ?>:</b>
	<?php echo CHtml::encode($data->vrijeme); ?>
	<br />


</div>