<?php
/* @var $this WiedervorlageController */
/* @var $dataProvider CActiveDataProvider */
?>
<?php Yii::setPathOfAlias("bootstrap",
 Yii::getPathOfAlias("application.extensions.bootstrap"));
?>
<h1>Wiedervorlage</h1>
<?php
$route="#";
if(Yii::app()->request->getQuery("korisnik")==Yii::app()->user->name)
{
    $route=array("wiedervorlage/create");
}
 else {
  $route=  array("wiedervorlage/dodaj","korisnik"=>Yii::app()->request->getQuery("korisnik"));
}
?>
<?php if(Yii::app()->user->tip!="makler") { ?>
<?php $this->widget("bootstrap.widgets.TbButton",array(
		'buttonType'=>'link',
		'type'=>'primary',
		'size'=>'small',
		'label'=>Yii::t("main","NOVI_WIEDERVORLAGE"),
		'url'=>$route,
		'htmlOptions'=>array(
		
)
))?>
<?php } ?>
<?php $this->widget('application.components.WiedervorlageGridWidget', array(
	'dataProvider'=>$wiedervorlage->search(),
	'wiedervorlage'=>$wiedervorlage,
		'termin'=>$termin,
		'showMaklerColumn'=>true,
)); ?>
