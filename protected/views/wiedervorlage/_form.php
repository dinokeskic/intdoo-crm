<?php
/* @var $this WiedervorlageController */
/* @var $model Wiedervorlage */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'wiedervorlage-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>
    <?php if($model->status!="termin" && $model->status!="bestätigt" && $model->status!="stattgefunden") { ?>
    <h4><?php echo Yii::t("main","VRIJEME");?>
    </h4>
    <hr>
    
	<div class="row">
		
		<?php echo $form->labelEx($model,'vrijeme'); ?>
	  <?php
	  $now=new DateTime();
         $startDate=$now->format("Y-m-d H:i:s");
         Yii::log($startDate,"info","myCategory");
            $this->widget('bootstrap.widgets.TbDateTimePicker',array(
               'model' => $model,
        'attribute' => 'vrijeme',
		'options'=>array(
				'startDate'=>$startDate,
				'minuteStep'=>30,
				'format'=>'dd.mm.yyyy hh:ii',
				)
               
            ));
          //echo $form->textField($model,'vrijeme',array('size'=>45,'maxlength'=>45));
            ?>

		<?php echo $form->error($model,'vrijeme'); ?>
        </div>
    <?php } ?>
    <?php if(Yii::app()->user->tip=="admin") { ?>
    <div class="row">
        <?php echo $form->labelEx($model,"cijena"); ?>
        <?php echo $form->textField($model,"cijena"); ?>
        <?php echo $form->error($model,"cijena"); ?>
    </div>
    <?php } ?>
    <h4><?php echo Yii::t("main","STRANKA");?></h4>
    <hr>
	<div class="row">
		<?php echo $form->labelEx($stranka,  Yii::t("main", "IME_STRANKE")); ?>
		<?php echo $form->textField($stranka,'imeStranke',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($stranka,'imeStranke'); ?>
	</div>
        <div class="row">
		<?php echo $form->labelEx($stranka,Yii::t("main", "PREZIME_STRANKE")); ?>
		<?php echo $form->textField($stranka,'prezimeStranke',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($stranka,'prezimeStranke'); ?>
	</div>
        <div class="row">
		<?php echo $form->labelEx($stranka,Yii::t("main", "TELEFON_STRANKE")); ?>
		<?php echo $form->textField($stranka,'telefonStranke',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($stranka,'telefonStranke'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($stranka,Yii::t("main", "ADRESA_STRANKE")); ?>
		<?php echo $form->textField($stranka,'adresaStranke',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($stranka,'adresaStranke'); ?>
	</div>
    <div class="row">
      <?php echo $form->labelEx($stranka,"lokacija"); ?>
        <?php echo $form->textField($stranka,'lokacija'); ?>
        <?php echo $form->error($stranka,'lokacija'); ?>
    </div>
        <div class="row">
		<?php echo $form->labelEx($stranka,Yii::t("main", "GODISTE_STRANKE")); ?>
		<?php echo $form->textField($stranka,'godisteStranke',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($stranka,'godisteStranke'); ?>
	</div>
        <div class="row">
		<?php echo $form->labelEx($stranka,Yii::t("main", "ZANIMANJE_STRANKE")); ?>
		<?php echo $form->textField($stranka,'zanimanjeStranke',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($stranka,'zanimanjeStranke'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($stranka,'pkv_vk'); ?>
		<?php echo $form->dropDownList($stranka,'pkv_vk',array(''=>'...',
                    'PKV'=>'PKV',
                    'VK'=>'VK',
                    'BAV'=>'BAV',
                    'SV'=>'SV')); ?>
		<?php echo $form->error($stranka,'pkv_vk'); ?>
	</div>

    <div class="row buttons" style="text-align: right;">
		<?php echo CHtml::submitButton($model->isNewRecord ? Yii::t("main", "WV_DODAJ") : Yii::t("main", "WV_DODAJ")); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
