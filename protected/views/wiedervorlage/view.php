<?php
/* @var $this WiedervorlageController */
/* @var $model Wiedervorlage */

Yii::setPathOfAlias("bootstrap", Yii::getPathOfAlias("ext.bootstrap"));
$cs=Yii::app()->clientscript;
   $cs->registerScriptFile( Yii::app()->theme->baseUrl . '/js/bootstrap-dropdown.js', CClientScript::POS_END );

?>
<?php
$this->widget(
        'bootstrap.widgets.TbButtonGroup', 
        array(
            'size'=>'medium',
            'type'=>'info',
            'htmlOptions'=>array(
				'class'=>'pull-right',
            		'style'=>'margin-bottom:10px;'
				),
            'buttons'=> array(
                array(
                'label'=>'',
                'items'=>array(
                    
                    array('label'=>Yii::t("main","AZURIRAJ_WIEDERVORLAGE"), 'url'=>array('update', 'id'=>$model->idwiedervorlage)),
                    

                    '---',
                    
                ),
            ),
           ),
        )
        );
?>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
		'id'=>'wiedervorlage-details',
		'type'=>'bordered',
		
	'data'=>$model,
		'htmlOptions'=>array('style'=>'margin-top:20px;'),
	'attributes'=>array(
	array(
		'name'=>'vrijeme',
		'value'=>DateTime::createFromFormat("Y-m-d H:i:s",$model->vrijeme)->format("d.m.Y H:i"),
		),
            array(
                'name'=>'cijena',
                'value'=>$model->cijena,
                'visible'=>Yii::app()->user->tip!="zaposlenik",
            ),
     'status',
	'idstranke0.imeStranke',
	'idstranke0.prezimeStranke',
	'idstranke0.adresaStranke',
        'idstranke0.lokacija',
	'idstranke0.telefonStranke',
	'idstranke0.zanimanjeStranke',
	'idstranke0.godisteStranke',
	'idstranke0.pkv_vk'
	),
)); ?>
<div class="form">
<?php $form = $this->beginWidget('CActiveForm',array(
		'id'=>'addComment-form',
		'enableAjaxValidation'=>false,
		'htmlOptions'=>array(),
));?>

    <table align="center">
        <tr>
            <td style="margin-left: 20px;" colspan="2">
<?php echo $form->textArea($komWv,'text',array(
		'id'=>'commentText',
		'rows'=>4,
		'cols'=>10,
		'maxlength'=>1000,
		'placeholder'=>Yii::t("main","UNESITE_KOMENTAR"),
		'style'=>'width:500px;margin-right:10px;'
));?>
            </td>
        </tr>
            <tr>
                <td style="width: 305px;"></td>
            <td>
<?php echo $form->hiddenField($komWv,'idwiedervorlage');?>
            
<?php echo CHtml::ajaxSubmitButton(Yii::t("main","SPREMI"),array('komWv/create'),array(
		'type'=>'POST',
		'beforeSend'=>'function(){}',
		'success'=>'function(data){
		$("#commentText").val("");
		$.fn.yiiListView.update("comments-list",{});
}',
));?>
            </td>
        </tr>
    </table>
</div>
<?php $this->endWidget();?>

<?php $this->widget('zii.widgets.CListView',array(
		'id'=>'comments-list',
		'dataProvider'=>$dataProvider,
		'itemView'=>'commentView',
		'emptyText'=>Yii::t("main","NEMA_REZULTATA"),
		'enablePagination'=>true,
		'summaryText'=>'',
		'htmlOptions'=>array('style'=>'width:80%;margin-left:10%'),
));?>
