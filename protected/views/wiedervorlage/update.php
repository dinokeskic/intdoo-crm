<?php
/* @var $this WiedervorlageController */
/* @var $model Wiedervorlage */
Yii::setPathOfAlias("bootstrap",
Yii::getPathOfAlias('application.extensions.bootstrap'));
$this->breadcrumbs=array(
	'Wiedervorlages'=>array('index'),
	$model->idwiedervorlage=>array('view','id'=>$model->idwiedervorlage),
	'Update',
);

$this->menu=array(
	array('label'=>'List Wiedervorlage', 'url'=>array('index')),
	array('label'=>'Create Wiedervorlage', 'url'=>array('create')),
	array('label'=>'View Wiedervorlage', 'url'=>array('view', 'id'=>$model->idwiedervorlage)),
	array('label'=>'Manage Wiedervorlage', 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t("main","AZURIRAJ_WIEDERVORLAGE");?></h1>

<?php $this->renderPartial('_form', array('model'=>$model, 'stranka'=>$stranka)); ?>