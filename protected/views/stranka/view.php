<?php
/* @var $this StrankaController */
/* @var $model Stranka */

$this->breadcrumbs=array(
	'Strankas'=>array('index'),
	$model->idstranka,
);

$this->menu=array(
	array('label'=>'List Stranka', 'url'=>array('index')),
	array('label'=>'Create Stranka', 'url'=>array('create')),
	array('label'=>'Update Stranka', 'url'=>array('update', 'id'=>$model->idstranka)),
	array('label'=>'Delete Stranka', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->idstranka),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Stranka', 'url'=>array('admin')),
);
?>

<h1>View Stranka #<?php echo $model->idstranka; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'idstranka',
		'imeStranke',
		'prezimeStranke',
		'telefonStranke',
		'adresaStranke',
		'godisteStranke',
		'zanimanjeStranke',
		'pkv_vk',
	),
)); ?>
