<?php
/* @var $this StrankaController */
/* @var $model Stranka */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'idstranka'); ?>
		<?php echo $form->textField($model,'idstranka',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'imeStranke'); ?>
		<?php echo $form->textField($model,'imeStranke',array('size'=>45,'maxlength'=>45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'prezimeStranke'); ?>
		<?php echo $form->textField($model,'prezimeStranke',array('size'=>45,'maxlength'=>45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'telefonStranke'); ?>
		<?php echo $form->textField($model,'telefonStranke',array('size'=>45,'maxlength'=>45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'adresaStranke'); ?>
		<?php echo $form->textField($model,'adresaStranke',array('size'=>45,'maxlength'=>45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'godisteStranke'); ?>
		<?php echo $form->textField($model,'godisteStranke',array('size'=>45,'maxlength'=>45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'zanimanjeStranke'); ?>
		<?php echo $form->textField($model,'zanimanjeStranke',array('size'=>45,'maxlength'=>45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'pkv_vk'); ?>
		<?php echo $form->textField($model,'pkv_vk',array('size'=>45,'maxlength'=>45)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->