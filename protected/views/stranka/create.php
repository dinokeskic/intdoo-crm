<?php
/* @var $this StrankaController */
/* @var $model Stranka */

$this->breadcrumbs=array(
	'Strankas'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Stranka', 'url'=>array('index')),
	array('label'=>'Manage Stranka', 'url'=>array('admin')),
);
?>

<h1>Create Stranka</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>