<?php
/* @var $this StrankaController */
/* @var $data Stranka */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('idstranka')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idstranka), array('view', 'id'=>$data->idstranka)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('imeStranke')); ?>:</b>
	<?php echo CHtml::encode($data->imeStranke); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('prezimeStranke')); ?>:</b>
	<?php echo CHtml::encode($data->prezimeStranke); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('telefonStranke')); ?>:</b>
	<?php echo CHtml::encode($data->telefonStranke); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('adresaStranke')); ?>:</b>
	<?php echo CHtml::encode($data->adresaStranke); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('godisteStranke')); ?>:</b>
	<?php echo CHtml::encode($data->godisteStranke); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('zanimanjeStranke')); ?>:</b>
	<?php echo CHtml::encode($data->zanimanjeStranke); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('pkv_vk')); ?>:</b>
	<?php echo CHtml::encode($data->pkv_vk); ?>
	<br />

	*/ ?>

</div>