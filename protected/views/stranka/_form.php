<?php
/* @var $this StrankaController */
/* @var $model Stranka */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'stranka-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'imeStranke'); ?>
		<?php echo $form->textField($model,'imeStranke',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'imeStranke'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'prezimeStranke'); ?>
		<?php echo $form->textField($model,'prezimeStranke',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'prezimeStranke'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'telefonStranke'); ?>
		<?php echo $form->textField($model,'telefonStranke',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'telefonStranke'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'adresaStranke'); ?>
		<?php echo $form->textField($model,'adresaStranke',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'adresaStranke'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'godisteStranke'); ?>
		<?php echo $form->textField($model,'godisteStranke',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'godisteStranke'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'zanimanjeStranke'); ?>
		<?php echo $form->textField($model,'zanimanjeStranke',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'zanimanjeStranke'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'pkv_vk'); ?>
		<?php echo $form->textField($model,'pkv_vk',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'pkv_vk'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->