<?php
/* @var $this StrankaController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Strankas',
);

$this->menu=array(
	array('label'=>'Create Stranka', 'url'=>array('create')),
	array('label'=>'Manage Stranka', 'url'=>array('admin')),
);
?>

<h1>Strankas</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
