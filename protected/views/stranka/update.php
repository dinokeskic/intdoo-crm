<?php
/* @var $this StrankaController */
/* @var $model Stranka */

$this->breadcrumbs=array(
	'Strankas'=>array('index'),
	$model->idstranka=>array('view','id'=>$model->idstranka),
	'Update',
);

$this->menu=array(
	array('label'=>'List Stranka', 'url'=>array('index')),
	array('label'=>'Create Stranka', 'url'=>array('create')),
	array('label'=>'View Stranka', 'url'=>array('view', 'id'=>$model->idstranka)),
	array('label'=>'Manage Stranka', 'url'=>array('admin')),
);
?>

<h1>Update Stranka <?php echo $model->idstranka; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>