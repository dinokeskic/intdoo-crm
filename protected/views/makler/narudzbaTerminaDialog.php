<?php Yii::setPathOfAlias("bootstrap",Yii::getPathOfAlias("ext.bootstrap")); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl."/js/novaNarudzba.js",  CClientScript::POS_HEAD); ?>
<?php Yii::app()->clientScript->registerScript("novaNarudzbaUrl","var novaNarudzbaUrl='"
        .Yii::app()->createUrl("makler/novaNarudzba")."';",  CClientScript::POS_HEAD); ?>
<?php new JsTrans("main", "de"); ?>
<?php ?>
<?php $narudzba=new NarudzbaTermina();
$narudzba->idmakler=$makler->idmakler;
?>
<?php
$this->beginWidget("zii.widgets.jui.CJuiDialog",array(
    'id'=>'novaNarudzbaDialog',
    'options'=>array(
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>400,
        'height'=>300,
    )
));

?>
<?php $form= $this->beginWidget("CActiveForm",array(
    'id'=>'novaNarudzba-form',
    'enableAjaxValidation'=>true,
));
?>
<div class='row'>
<?php echo $form->labelEx($narudzba,"br_termina"); ?>
<?php echo $form->textField($narudzba,"br_termina"); ?>
<?php echo $form->error($narudzba,"br_termina"); ?>
</div>
<?php echo $form->hiddenField($narudzba,"idmakler"); ?>
<?php $this->widget("bootstrap.widgets.TbButton",array(
    'type'=>'primary',
    'buttonType'=>'button',
    'label'=>Yii::t("main","SPREMI"),
    'htmlOptions'=>array(
        'id'=>'novaNarudzbaButton',
        'class'=>'offset2',
    )
)); ?>
<?php $this->endWidget("CActiveForm"); ?>
<?php $this->endWidget("zii.widgets.cjuiCJuiDialog"); ?>