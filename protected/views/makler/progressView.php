<?php
/* @var $data Makler */
?>
<?php Yii::setPathOfAlias("bootstrap", Yii::getPathOfAlias("ext.bootstrap"));?>
<div class="maklerProgessItem">
    <div><?php echo $data->ime." ".$data->prezime; ?></div>
    <div><?php echo $data->adresa;?></div>
    <?php if($data->getPosljednjaNarudzba()!=null){?>
    <?php $total=$data->getPosljednjaNarudzba()->br_termina;
          $odradjeno=$data->getBrDodijeljenihTerminaOdNarudzbe();
          $procenat=  NumUtil::percentage($total, $odradjeno);
    ?>
    <div class='offset10'><?php echo $odradjeno."/".$total."  ".$procenat."%"."<br/>"; ?></div>
<?php $this->widget("bootstrap.widgets.TbProgress",array(
   "type"=>$data->getProgressColor(),
    "striped"=>true,
    "percent"=>$procenat,
));
?>
    <?php } else { ?>
    <?php echo Yii::t("main","NEMA_NARUDZBI");  }?>
    <hr/>
</div>
    