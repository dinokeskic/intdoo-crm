<?php
/* @var $this MaklerController */
/* @var $data Makler */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('idmakler')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idmakler), array('view', 'id'=>$data->idmakler)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ime')); ?>:</b>
	<?php echo CHtml::encode($data->ime); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('prezime')); ?>:</b>
	<?php echo CHtml::encode($data->prezime); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('adresa')); ?>:</b>
	<?php echo CHtml::encode($data->adresa); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lokacija')); ?>:</b>
	<?php echo CHtml::encode($data->lokacija); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idkorisnik')); ?>:</b>
	<?php echo CHtml::encode($data->idkorisnik); ?>
	<br />


</div>