<?php 
/*
 * @var Termin $termin
 */
?>
<h1><?php echo Yii::t("main","MOJI_TERMINI");?></h1>
<?php
$this->widget("application.components.TerminGridWidget",array(
		'termin'=>$termin,
		'dataProvider'=>$termin->search(),
                'isMaklerTermins'=>true,
));
?>