<?php
/* @var $this MaklerController */
/* @var $makler Makler */
/* @var $form CActiveForm */
?>

<div class="form">
<?php 
$msUrl=Yii::app()->assetManager->publish(Yii::getPathOfAlias("ext.multipleSelect"));
Yii::app()->clientScript->registerScriptFile($msUrl."/jquery.multiple.select.js");
Yii::app()->clientScript->registerCssFile($msUrl."/multiple-select.css");
?>
<?php Yii::setPathOfAlias("bootstrap",
 Yii::getPathOfAlias("ext.bootstrap"));?>
<script>
$(function(){
	$("#lokacije").multipleSelect({
     filter:true,
	placeholder:"...",
});
	$("#dodajButton").click(function(){
		var $select=$("#lokacije"),
			$input=$("#dodajInput"),
			value=$.trim($input.val()),
			$opt=$("<option />", {
				value:value,
				text:value,
			});
		if(!value)
		{
			$input.focus();
		return;
		}
		$opt.prop("selected",true);
		$input.val("");
		$select.append($opt).multipleSelect("refresh");
	});
});
</script>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'makler-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo CHtml::errorSummary(array($korisnik,$makler)); ?>
        <div class="row">
		<?php echo $form->labelEx($korisnik,'korisnickoIme'); ?>
		<?php echo $form->textField($korisnik,'korisnickoIme',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($korisnik,'korisnickoIme'); ?>
	</div>
	<?php if($makler->scenario=="create"):?>
		<div class="row">
		<?php echo $form->labelEx($korisnik,'lozinka'); ?>
		<?php echo $form->passwordField($korisnik,'lozinka'); ?>
		<?php echo $form->error($korisnik,'lozinka'); ?>
	</div>
		<div class="row">
		<?php echo $form->labelEx($korisnik,'lozinka2'); ?>
		<?php echo $form->passwordField($korisnik,'lozinka2'); ?>
		<?php echo $form->error($korisnik,'lozinka2'); ?>
	</div>
	<?php endif;?>
	<div class="row">
		<?php echo $form->labelEx($makler,'ime'); ?>
		<?php echo $form->textField($makler,'ime',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($makler,'ime'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($makler,'prezime'); ?>
		<?php echo $form->textField($makler,'prezime',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($makler,'prezime'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($makler,'adresa'); ?>
		<?php echo $form->textField($makler,'adresa',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($makler,'adresa'); ?>
	</div>
<div class="row">
		<?php echo $form->labelEx($makler,'telefon'); ?>
		<?php echo $form->textField($makler,'telefon',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($makler,'telefon'); ?>
	</div>
<div class="row">
		<?php echo $form->labelEx($makler,'email'); ?>
		<?php echo $form->textField($makler,'email',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($makler,'email'); ?>
	</div>

<div class="row">
		<?php echo $form->labelEx($makler,Yii::t('main',"FIRMA")); ?>
		<?php echo $form->dropDownList($makler, 'idfirma',array('---Odaberi---',CHtml::listData(Firma::model()->findAll(),'id','naziv'),)); ?>
		<?php echo $form->error($makler,'idfirma'); ?>
	</div>
    <h1><?php echo Yii::t("main","KOMENTAR");?></h1>
<div class="row">

    <?php echo $form->labelEx($makler,'komentar');?>
    <?php echo $form->textArea($makler,'komentar',array('rows'=>4,'style'=>'width:300px;'));?>
    <?php echo $form->error($makler,'komentar');?>
</div>

	  <h1><?php echo Yii::t("main","LOKACIJE");?></h1>
	<div class="row">
	<?php echo CHtml::label(Yii::t("main","LOKACIJE"),"Makler_lokacije");?>

	<?php echo $form->listBox($makler, "lokacije",CHtml::listData(
			Lokacija::model()->findAll(array(
								'select'=>'id,lokacija',
								'distinct'=>true,
										)), "lokacija","lokacija"),array(
                                'id'=>'lokacije',
								'multiple'=>'multiple',
									
				));?>
   
	                       </div>

	                       	<div class="addLocationRow">
	                 <?php echo CHtml::textField("dodaj","",array(
      		'id'=>'dodajInput',
      ));?>
      <?php $this->widget("bootstrap.widgets.TbButton",array(
      		'buttonType'=>'button',
      		'type'=>'success',
      		'size'=>'small',
      		'label'=>Yii::t("main","DODAJ_LOKACIJU"),
      		'htmlOptions'=>array(
      				'id'=>'dodajButton',
					'style'=>'margin-left:5px;'
      )
      ));?>
	         
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($makler->isNewRecord ? Yii::t("main","SACUVAJ") : Yii::t("main","SACUVAJ")); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->