<?php
/* @var $this MaklerController */
/* @var $model Makler */

$this->breadcrumbs=array(
	Yii::t("main","MAKLER")=>array('index'),
	Yii::t("main","MAKLER_DODAJ"),
);

?>

<h1><?php echo Yii::t("main","MAKLER_DODAJ");?></h1>

<?php $this->renderPartial('_form', array('korisnik'=>$korisnik,'makler'=>$makler)); ?>