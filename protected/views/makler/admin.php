<?php
/* @var $this MaklerController */
/* @var $model Makler */
Yii::setPathOfAlias("bootstrap",
Yii::getPathOfAlias('application.extensions.bootstrap'));
$this->breadcrumbs=array(
	Yii::t("main","MAKLERI")=>array('admin'),
);
$this->menu=array(
	array('label'=>Yii::t("main","MAKLER_DODAJ"), 'url'=>array('create')),
);
$this->menu=array(
	array('label'=>'List Makler', 'url'=>array('index')),
	array('label'=>'Create Makler', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#makler-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php echo Yii::t("main", "TITLE_MAKLERA") ?></h1>


<table style="border: 1px solid black;" align="right">
    <tr>
        <td>
<?php echo CHtml::link(Yii::t("main", "MAKLER_DODAJ"),'create'); ?>
        </td>
    </tr>
</table>
<br>
<hr>

<?php 

if(Yii::app()->user->tip!="acc"){

$this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'makler-grid',
	'dataProvider'=>$model->search(),
		'type'=>'bordered striped',
		"emptyText"=>Yii::t("main","NEMA_REZULTATA"),
	'filter'=>$model,
	'columns'=>array(
                'ime',
		'prezime',
		'adresa',
                'telefon',
                'email',
      array(
		'name'=>'korisnickoIme',
		'value'=>'$data->idkorisnik0->korisnickoIme',
		'filter'=>CHtml::activeTextField($model->idkorisnik0,'korisnickoIme'),
            'header'=>Yii::t("main","KORISNICKO_IME"),
),
		
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>"{view}{update}{delete}",
        'deleteConfirmation'=>Yii::t("main","MAKLER_POTVRDA_BRISANJA"),
         'updateButtonLabel'=>Yii::t("main","AZURIRAJ"),
       'deleteButtonLabel'=>Yii::t("main","BRISANJE"),
        'viewButtonLabel'=>Yii::t("main","PREGLED"),

                    'buttons'=>array(
                        'visible'=>Yii::app()->user->tip!="acc",
                    
                )
                            
                
                ),
	),
));}else{
    $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'makler-grid',
	'dataProvider'=>$model->search(),
		'type'=>'bordered striped',
		"emptyText"=>Yii::t("main","NEMA_REZULTATA"),
	'filter'=>$model,
	'columns'=>array(
            array(
        'name'  => 'ime',
        'value' => '$data->ime',
        'type'  => 'raw',
    ),
		
		'prezime',
		'adresa',
                'telefon',
                'email',
      array(
		'name'=>'korisnickoIme',
		'value'=>'$data->idkorisnik0->korisnickoIme',
		'filter'=>CHtml::activeTextField($model->idkorisnik0,'korisnickoIme'),
                'header'=>Yii::t("main","KORISNICKO_IME"),
),
		
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>"{view}{update}",
        'deleteConfirmation'=>Yii::t("main","MAKLER_POTVRDA_BRISANJA"),
         'updateButtonLabel'=>Yii::t("main","AZURIRAJ"),
       'deleteButtonLabel'=>Yii::t("main","BRISANJE"),
        'viewButtonLabel'=>Yii::t("main","PREGLED"),

                    'buttons'=>array(
                    
                )
                            
                
                ),
	),
));
} ?>
<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$model->search(),
	'itemView'=>'progressView',
)); ?>

