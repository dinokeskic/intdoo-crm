<?php
/* @var $this MaklerController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Maklers',
);

$this->menu=array(
	array('label'=>'Create Makler', 'url'=>array('create')),
	array('label'=>'Manage Makler', 'url'=>array('admin')),
);
?>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
