<?php
/* @var $this MaklerController */
/* @var $model Makler */
Yii::setPathOfAlias("bootstrap",
Yii::getPathOfAlias('application.extensions.bootstrap'));

$this->breadcrumbs=array(
	Yii::t("main","MAKLERI")=>array('admin'),
	$model->idkorisnik0->korisnickoIme,
        
);

?>

<?php if(($model->getPosljednjaNarudzba()==null || $model->getProcenatOstvarenihTermina()==100)
        && (Yii::app()->user->tip=="admin" || Yii::app()->user->tip=="posrednik")) { ?>
<div class='narudzbaDialogOpenerButton pull-right' style="margin-top: 10px;">
    <?php $this->widget("bootstrap.widgets.TbButton",array(
       'label'=>Yii::t("main","NOVA_NARUDZBA"),
        'type'=>'primary',
        'buttonType'=>'button',
        'htmlOptions'=>array(
            'id'=>'novaNarudzbaDialogOpenerButton',
        )
    )); ?>
</div>
<?php } ?>
    
<h1><?php echo $model->ime." ".$model->prezime."          ";?> 
   
<?php
$this->widget(
        'bootstrap.widgets.TbButtonGroup', 
        array(
            'size'=>'medium',
            
            'buttons'=> array(
                array(
                'label'=>'',
                'items'=>array(
                    array('label'=>Yii::t("main","MAKLER_DODAJ"), 'url'=>array('create')),
                    array('label'=>Yii::t("main","AZURIRANJE_MAKLERA"), 'url'=>array('update', 'id'=>$model->idmakler)),
                    array('label'=>Yii::t("main","PROMJENA_LOZINKE"),'url'=>array('korisnik/promjenaLozinke','korisnik'=>$model->idkorisnik0->korisnickoIme)),
                     '---',
                    array('label'=>Yii::t("main","BRISANJE"),'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->idmakler),'confirm'=>Yii::t("main","MAKLER_POTVRDA_BRISANJA"))),
                ),
            ),
           ),
        )
        );
?>

</h1>

<?php 
$lokacije="";
foreach($model->lokacije as $lokacija)
{
	$lokacije=$lokacije.$lokacija->lokacija.",";
}
?>


<?php $this->widget(
		'zii.widgets.jui.CJuiTabs',array(
			'tabs'=>array(
                            
                            'Info'=>array(
    'id'=>'info-tab',
    'content'=>$this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ime',
		'prezime',
		'adresa',
                'telefon',
                'email',
'lokacije'=>array(
			'label'=>Yii::t("main","LOKACIJE"),
			'value'=>$lokacije,
			),
            'komentar',
            'firma.naziv',
	),
), true)
),
Yii::t("main","KALENDAR")=>array(
		'id'=>'kalendar-tab',
'content'=>$this->widget('application.components.Calendar',array(
			'ajaxUrl'=>Yii::app()->createUrl("termin/getTerminsForMakler",array("idmakler"=>$model->idmakler)),
),true)
),

),
'options'=>array(
		'collapsible'=>true,
),
'htmlOptions'=>array(
		'style'=>'min-height:700px;margin-bottom:30px;',
)

)
		);?>
<?php $this->renderPartial("narudzbaTerminaDialog",array("makler"=>$model));
