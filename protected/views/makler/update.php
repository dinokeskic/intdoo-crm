<?php
/* @var $this MaklerController */
/* @var $model Makler */

$this->breadcrumbs=array(
	Yii::t("main","MAKLER")=>array('index'),
	$makler->idkorisnik0->korisnickoIme=>array('view','id'=>$makler->idmakler),
	Yii::t("main","AZURIRANJE_MAKLERA"),
);
?>

<h1><?php echo Yii::t("main","AZURIRANJE_MAKLERA")." ";?><?php echo $makler->idkorisnik0->korisnickoIme; ?></h1>

<?php $this->renderPartial('_form', array('makler'=>$makler,'korisnik'=>$makler->idkorisnik0)); ?>