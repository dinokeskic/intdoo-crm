<div class="comment" id="<?php echo $data->idkom_wv;?>">
<div class="author">
<?php echo $data->idwiedervorlage0->idkorisnik0->korisnickoIme;?>
</div>
<div class="time">
<?php echo DateTime::createFromFormat("Y-m-d H:i:s",$data->datum)->format('d.M.Y u H:i');
?>
</div>
<div class="comment-content">
<?php nl2br(CHtml::encode($data->text));?>
</div>
</div>