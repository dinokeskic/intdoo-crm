<?php
/* @var $this FirmaController */
/* @var $model Firma */
Yii::setPathOfAlias("bootstrap",
Yii::getPathOfAlias('application.extensions.bootstrap'));
$cs=Yii::app()->clientscript;
   $cs->registerScriptFile( Yii::app()->theme->baseUrl . '/js/bootstrap-dropdown.js', CClientScript::POS_END );
$this->breadcrumbs=array(
	'Firmas'=>array('index'),
	$model->id,
);
Yii::app()->bootstrap->registerDatepicker();
?>
<script>
  $(function() {
    $( "#datum1" ).datepicker({
    	dateFormat: 'yy-mm-dd'
    });

    $( "#datum2" ).datepicker({
    	dateFormat: 'yy-mm-dd'
    });
  });
  </script>
<h1><?php echo Yii::t("main", "FIRMA_PREGLED"); ?></h1>
<hr>

	
</form>
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'naziv',
		'adresa',
		'telefon',
	),
)); ?>
<h1><?php echo Yii::t("main","IZVJESTAJ"); ?></h1>
<form method="GET">
<table>
<tr>
	<td><?php echo Yii::t("main","OD"); ?></td>
	<td><input type="text" id="datum1" name="datum1" /></td>


	<td><?php echo Yii::t("main","DO"); ?></td>
	<td><input type="text" id="datum2" name="datum2"/></td>

	
	<td>
		<input type="submit" name="ispis" value="Print"/>
	</td>
</tr>
</table>
<?php
if(isset($_GET['ispis'])){
	$datum1=$_GET['datum1'];
	$datum2=$_GET['datum2'];
	$id=Yii::app()->request->getQuery('id');

	$this->renderPartial('print');
}
?>
<hr>
<h1><?php echo Yii::t("main","IZDANI_RACUNI"); ?></h1>
<?php
$id=Yii::app()->request->getQuery('id');
$izdati=Yii::app()->db->createCommand("select * from racuni r inner join firma f on r.idfirma=f.id where r.idfirma=$id order by r.datum desc")->queryAll();

foreach ($izdati as $key) {
	$d=date_format(date_create($key['datum']),'d.m.Y');
	?>

<a href="<?php echo Yii::app()->baseUrl; ?>/racun/<?php echo $key['lokacija'].'.pdf'; ?>" target="" >Download</a> <?php echo '  '.$key['naziv'].'  '; ?><?php echo '  '.$d.'<br> '; ?>
	<?php
}
?>
