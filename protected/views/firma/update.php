<?php
/* @var $this FirmaController */
/* @var $model Firma */

$this->breadcrumbs=array(
	'Firmas'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Firma', 'url'=>array('index')),
	array('label'=>'Create Firma', 'url'=>array('create')),
	array('label'=>'View Firma', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Firma', 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t("main", "FIRMA_AZURIRAJ"); ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>