<?php
/* @var $this FirmaController */
/* @var $model Firma */
Yii::setPathOfAlias("bootstrap",
Yii::getPathOfAlias('application.extensions.bootstrap'));

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#firma-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<?php $this->widget("bootstrap.widgets.TbButton",array(
		'buttonType'=>'link',
		'type'=>'primary',
		'size'=>'small',
		'label'=>Yii::t("main","FIRMA_DODAJ"),
		'url'=>array('firma/create'),
		'htmlOptions'=>array(
		'style'=>'margin-top:10px;'
)
));?>
<br>

<?php
 $this->widget('bootstrap.widgets.TbGridView',array(
     'id'=>'firma-grid',
     'dataProvider'=>$model->search(),
     'type'=>'bordered striped',
     'emptyText'=>  Yii::t("main", "NEMA_REZULTATA"),
     'filter'=>$model,
     'columns'=>array(
        'naziv',
         'adresa',
         'telefon',
         array(
             'class'=>'bootstrap.widgets.TbButtonColumn',
             'template'=>'{view}{update}{delete}',
             'deleteConfirmation'=>Yii::t("main","FIRMA_POTVRDA_BRISANJA"),
         'updateButtonLabel'=>Yii::t("main","AZURIRAJ"),
       'deleteButtonLabel'=>Yii::t("main","BRISANJE"),
        'viewButtonLabel'=>Yii::t("main","PREGLED"),
         )
     )
 ));
?>
