<?php $this->widget("ext.timeago.JTimeAgo",array(
    'selector'=>'.timeago',
));?>
<h1><?php echo Yii::t("main","NOTIFIKACIJE");?></h1>
<?php foreach($korisnik->notifikacije as $notifikacija):?>
<div class="<?php echo $notifikacija->getCssClass(); ?>">
    <span><?php echo $notifikacija->text;?></span>
    <span class="muted"><abbr class="timeago" title="<?php echo $notifikacija->vrijeme;?>"></abbr></span>
</div>
<?php if($notifikacija->pregledano==false)
{
    $notifikacija->pregledano=true;
    $notifikacija->save();
}
?>
<?php endforeach;?>
