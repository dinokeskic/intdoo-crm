<?php
/* @var $this KorisnikController */
Yii::setPathOfAlias("bootstrap",
Yii::getPathOfAlias('application.extensions.bootstrap'));
$this->breadcrumbs=array(
	'Korisnik',
);

?>
<h1> <?php echo Yii::t("main", "KALENDAR"); ?> </h1>
    <?php
$this->widget('application.components.Calendar',array(
			'ajaxUrl'=>Yii::app()->createUrl("termin/getTermins"),
			'template'=>'allTerminsTemplate',
                        'showDeleteButton'=>true,
                        'deleteImageLink'=>Yii::app()->basePath."/images/delete.png",
));
?>
