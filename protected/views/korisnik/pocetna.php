<?php
/* @var $this KorisnikController */
Yii::setPathOfAlias("bootstrap",
Yii::getPathOfAlias('application.extensions.bootstrap'));
$this->breadcrumbs=array(
	'Korisnik',
);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl."/js/wiedervorlageSave.js");
?>
<h1> <?php echo Yii::t("main", "MOJ_KALENDAR"); ?> </h1>
    <?php
$this->widget('application.components.Calendar',array(
			'ajaxUrl'=>Yii::app()->createUrl("wiedervorlage/getWiedervorlages",array("idkorisnik"=>  Yii::app()->user->getId())),
                          'template'=>'calendarTemplate',
                          'dayClickEvent'=>'showWiedervorlageDialog',
                          'showDeleteButton'=>true,
                          'deleteImageLink'=>Yii::app()->request->baseUrl."/images/button_delete_red.png",
                          'deleteConfirmMessage'=>Yii::t("main","OBRISI_WIEDERVORLAGE"),
));

?>
<?php $model=new Wiedervorlage();
$stranka=new Stranka();?>
<?php $this->renderPartial("addWiedervorlageDialog",array('model'=>$model,'stranka'=>$stranka)); ?>