<?php
Yii::app()->clientScript->registerScript("saveWiedervorlageUrl",
        "var saveWiedervorlageUrl='".Yii::app()->createUrl("wiedervorlage/create")."';",  CClientScript::POS_HEAD);
?>
<?php new JsTrans("main", "bs"); ?>
<?php
$this->beginWidget("zii.widgets.jui.CJuiDialog",array(
    'id'=>'addWiedervorlageDialog',
    'options'=>array(
        'title'=>Yii::t("main","DODAJ_WIEDERVORLAGE"),
        'autoOpen'=>false,
        'width'=>500,
        'height'=>600,
        
    )
));
?>
<div id="datum" class="muted"></div>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'wiedervorlage-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>true,
        'enableClientValidation'=>false,
)); ?>
    <h4><?php echo Yii::t("main","VRIJEME");?>
    </h4>
    <hr>
    
	<div class="row">
		
		<?php echo $form->labelEx($model,'vrijeme'); ?>
	  <?php
	  $now=new DateTime();
         $startDate=$now->format("Y-m-d H:i:s");
         Yii::log($startDate,"info","myCategory");
            $this->widget('bootstrap.widgets.TbTimePicker',array(
                'name'=>'vrijemeWiedervorlage',
          	'options'=>array(
                    'showMeridian'=>false,
				),
                'htmlOptions'=>array(
                    'id'=>'vrijemeWiedervorlage',
                )
               
            ));
          //echo $form->textField($model,'vrijeme',array('size'=>45,'maxlength'=>45));
            ?>

		<?php echo $form->error($model,'vrijeme'); ?>
        </div>đ   <?php if(Yii::app()->user->tip=="admin") { ?>
    <div class="row">
        <?php echo $form->labelEx($model,"cijena"); ?>
        <?php echo $form->textField($model,"cijena"); ?>
        <?php echo $form->error($model,"cijena"); ?>
    </div>
    <?php } ?>
    <?php echo CHtml::activeHiddenField($model, "vrijeme",array('id'=>'datumVrijemeWiedervorlage'));?>
    <h4><?php echo Yii::t("main","STRANKA");?></h4>
    <hr>
	<div class="row">
		<?php echo $form->labelEx($stranka,  Yii::t("main", "IME_STRANKE")); ?>
		<?php echo $form->textField($stranka,'imeStranke',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($stranka,'imeStranke'); ?>
	</div>
        <div class="row">
		<?php echo $form->labelEx($stranka,Yii::t("main", "PREZIME_STRANKE")); ?>
		<?php echo $form->textField($stranka,'prezimeStranke',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($stranka,'prezimeStranke'); ?>
	</div>
        <div class="row">
		<?php echo $form->labelEx($stranka,Yii::t("main", "TELEFON_STRANKE")); ?>
		<?php echo $form->textField($stranka,'telefonStranke',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($stranka,'telefonStranke'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($stranka,Yii::t("main", "ADRESA_STRANKE")); ?>
		<?php echo $form->textField($stranka,'adresaStranke',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($stranka,'adresaStranke'); ?>
	</div>
    <div class="row">
        <?php echo $form->labelEx($stranka,Yii::t("main", "LOKACIJA")); ?>
		<?php echo $form->textField($stranka,'lokacija'); ?>
		<?php echo $form->error($stranka,'lokacija'); ?>
    </div>
        <div class="row">
		<?php echo $form->labelEx($stranka,Yii::t("main", "GODISTE_STRANKE")); ?>
		<?php echo $form->textField($stranka,'godisteStranke',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($stranka,'godisteStranke'); ?>
	</div>
        <div class="row">
		<?php echo $form->labelEx($stranka,Yii::t("main", "ZANIMANJE_STRANKE")); ?>
		<?php echo $form->textField($stranka,'zanimanjeStranke',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($stranka,'zanimanjeStranke'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($stranka,'pkv_vk'); ?>
		<?php echo $form->dropDownList($stranka,'pkv_vk',array(''=>'PKV/VK...',
                    'PKV'=>'PKV',
                    'VK'=>'VK',
                    'BAV'=>'BAV',
                    'SV'=>'SV')); ?>
		<?php echo $form->error($stranka,'pkv_vk'); ?>
	</div>
	<?php $this->widget("bootstrap.widgets.TbButton",array(
           'type'=>'primary',
            'buttonType'=>'button',
            'label'=>Yii::t("main","SPREMI"),
            'htmlOptions'=>array(
                'id'=>'saveWiedervorlageButton',
                'class'=>'offset1'
            ),
        )); ?>

</div><!-- form -->

<?php $this->endWidget("bootstrap.widgets.TbActiveForm"); ?>
<?php
$this->endWidget("zii.widgets.jui.CJuiDialog");
?>