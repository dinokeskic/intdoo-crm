<?php

$this->pageTitle=Yii::app()->name . ' - Login';
Yii::app()->bootstrap->registerAssetJs('notify');
Yii::app()->bootstrap->registerAssetCss('notify');
$this->breadcrumbs=array(
	'Korisnik',
);
?>

<h1>Login</h1>
<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'promjenaLozinke-form',
)); ?>

<br>
<br>
<br>
<br>
<?php if(Yii::app()->request->getQuery("korisnik")==Yii::app()->user->name):?>
	<div class="row">
		<?php echo $form->labelEx($korisnik,'staraLozinka'); ?>
		<?php echo $form->passwordField($korisnik,'staraLozinka',
				array('autocomplete'=>'off')); ?>
		<?php echo $form->error($korisnik,'staraLozinka'); ?>
	</div>
	<?php endif;?>
	<div class="row">
		<?php echo $form->labelEx($korisnik,'lozinka'); ?>
		<?php echo $form->passwordField($korisnik,'lozinka',array('autocomplete'=>'off',
				'value'=>''
		)); ?>
		<?php echo $form->error($korisnik,'lozinka'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($korisnik,'lozinka2'); ?>
		<?php echo $form->passwordField($korisnik,'lozinka2',array(
'autocomplete'=>'off'
		)); ?>
		<?php echo $form->error($korisnik,'lozinka2'); ?>

	</div>


	<div class="row buttons">
		<?php echo CHtml::submitButton(Yii::t("main","PROMJENI")
				); ?>
	</div>

<?php $this->endWidget(); ?>
</div>