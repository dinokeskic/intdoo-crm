<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Login';
Yii::setPathOfAlias("bootstrap",
Yii::getPathOfAlias('application.extensions.bootstrap'));
?>
<?php
 /* $this->beginWidget('bootstrap.widgets.TbBox',array(
		'headerIcon'=>'icon-tag',
		'htmlOptions'=>array(
				'class'=>'span4',
)
)); */
 ?>
 <br/>
 <br/>
 <div class="parent" style="text-align:center;">
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'login-form',
	'type'=>'vertical',
		'htmlOptions'=>array(
				'class'=>'well span4 offset3',
				'style'=>"margin 0px auto;display:inline-block;",
)
)); ?>
<h3 class="muted offset1">INT d.o.o CRM 0.9 (beta)</h3>
<?php echo $form->textFieldRow($korisnik,'korisnickoIme',array('class'=>'span3','style'=>'width: 100%','placeholder'=>Yii::t("main","KORISNICKO_IME"),'labelOptions'=>array('label'=>false)));?>
<?php echo $form->passwordFieldRow($korisnik,'lozinka',array('class'=>'span3','style'=>'width: 100%','placeholder'=>Yii::t("main","LOZINKA"),'labelOptions'=>array('label'=>false)));?>
<?php echo $form->checkBoxRow($korisnik,'zapamtiMe');?>
<br>
<br>
<?php $this->widget('bootstrap.widgets.TbButton',array(
		'buttonType'=>'submit',
		'label'=>'Anmelden',
		'htmlOptions'=>array(
				'class'=>'span6 offset3',
				
)		
));?>
</div>
<?php $this->endWidget(); ?>
 <?php // $this->endWidget();?>