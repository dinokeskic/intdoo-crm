<?php
/* @var $this KorisnikController */
Yii::setPathOfAlias("bootstrap",
Yii::getPathOfAlias('application.extensions.bootstrap'));
$this->breadcrumbs=array(
	'Korisnik',
);
?>
<h1> <?php echo Yii::t("main", "MOJ_KALENDAR"); ?> </h1>
    <?php
    $criteria=new CDbCriteria();
    
    $criteria->condition='idkorisnik=:idkorisnik';
    $criteria->params=array('idkorisnik'=>Yii::app()->user->getId());
    
    $makler=  Makler::model()->find($criteria);
    
$this->widget('application.components.Calendar',array(
			'ajaxUrl'=>Yii::app()->createUrl("termin/getTerminsForMakler",array("idmakler"=>$makler->idmakler)),
));

?>