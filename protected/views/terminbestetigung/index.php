<?php
/* @var $this TerminController */

$this->breadcrumbs=array(
	'Terminbestetigung',
);
?>
<?php $this->widget('application.components.TerminbestetigungGridWidget',array(
		'terminbestetigung'=>$terminbestetigung,
		'terminstattgefunden'=>$terminstattgefunden,
		'dataProvider'=>$terminbestetigung->search(),
		'showMaklerColumn'=>true,
))?>