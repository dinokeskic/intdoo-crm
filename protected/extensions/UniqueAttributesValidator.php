<?php
class UniqueAttributesValidator extends CValidator {
	public $secondAttribute;
	public $message;
	public function validateAttribute($object,$attribute)
	{
		$criteria=new CDbCriteria();
		$arr=explode(".",$this->secondAttribute );
		if(isset($arr[1]))
		{
			$with=array();
		for($i=0;$i<count($arr)-1;$i++)
		{
			array_push($with,$arr[$i]);
		}
		$criteria->with=$with;
			$criteria->together=true;
		}
		$criteria->compare($this->secondAttribute, CHtml::value($object, $this->secondAttribute));
		$criteria->addCondition("t.".$attribute."='".$object->$attribute."'");
		$objects=$object->model()->findAll($criteria);
	
		if(!empty($objects))
		{
			Yii::log("Array is not empty","info","emptyArray");
			$this->addError($object,$attribute,$this->message);
		}
		else
		{
			Yii::log("Array is  empty","info","emptyArray");
		}
	}
}