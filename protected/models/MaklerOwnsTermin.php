<?php

/**
 * This is the model class for table "makler_owns_termin".
 *
 * The followings are the available columns in table 'makler_owns_termin':
 * @property string $idmakler
 * @property string $idtermin
 * @property string $vrijeme
 * @property Makler $makler
 * @property Termin $termin
 */
class MaklerOwnsTermin extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'makler_owns_termin';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		$now=new CDbExpression("NOW()");
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idmakler, idtermin', 'required'),
			array('idmakler','ext.UniqueAttributesValidator','secondAttribute'=>'termin.vrijeme',
					'message'=>Yii::t("main","MAKLER_IMA_TERMIN"),
		),
			array('vrijeme','default','value'=>$now),
			array('idmakler, idtermin', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idmakler, idtermin, vrijeme', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
				'makler'=>array(self::BELONGS_TO,"Makler","idmakler"),
				'termin'=>array(self::BELONGS_TO,"Termin","idtermin"),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idmakler' => Yii::t("main","MAKLER"),
			'idtermin' => 'termin',
			'vrijeme' => 'Vrijeme',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idmakler',$this->idmakler,true);
		$criteria->compare('idtermin',$this->idtermin,true);
		$criteria->compare('vrijeme',$this->vrijeme,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MaklerOwnsTermin the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
