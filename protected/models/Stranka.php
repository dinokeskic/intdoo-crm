<?php

/**
 * This is the model class for table "stranka".
 *
 * The followings are the available columns in table 'stranka':
 * @property string $idstranka
 * @property string $imeStranke
 * @property string $prezimeStranke
 * @property string $telefonStranke
 * @property string $adresaStranke
 * @property string $godisteStranke
 * @property string $zanimanjeStranke
 * @property string $pkv_vk
 * @property string $lokacija
 *
 * The followings are the available model relations:
 * @property Wiedervorlage[] $wiedervorlages
 */
class Stranka extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'stranka';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
        
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('imeStranke, prezimeStranke, telefonStranke, adresaStranke, godisteStranke, zanimanjeStranke, pkv_vk,lokacija', 'required'),
				array('idstranka','safe','on'=>'update'),
			array('imeStranke, prezimeStranke, telefonStranke, adresaStranke, godisteStranke, zanimanjeStranke, pkv_vk', 'length', 'max'=>45),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idstranka, imeStranke, prezimeStranke, telefonStranke, adresaStranke, godisteStranke, zanimanjeStranke,lokacija, pkv_vk', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'wiedervorlages' => array(self::HAS_MANY, 'Wiedervorlage', 'idstranke'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idstranka' => Yii::t("main","ID_STRANKA"),
			'imeStranke' => Yii::t("main","IME"),
			'prezimeStranke' => Yii::t("main","PREZIME"),
			'telefonStranke' => Yii::t("main","TELEFON"),
			'adresaStranke' => Yii::t("main","ADRESA"),
			'godisteStranke' => Yii::t("main","GODISTE"),
			'zanimanjeStranke' => Yii::t("main","ZANIMANJE"),
			'pkv_vk' =>Yii::t("main","VRSTA_TERMINA"),
                        'lokacija'=>Yii::t("main","LOKACIJA"),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idstranka',$this->idstranka,true);
		$criteria->compare('imeStranke',$this->imeStranke,true);
		$criteria->compare('prezimeStranke',$this->prezimeStranke,true);
		$criteria->compare('telefonStranke',$this->telefonStranke,true);
		$criteria->compare('adresaStranke',$this->adresaStranke,true);
		$criteria->compare('godisteStranke',$this->godisteStranke,true);
		$criteria->compare('zanimanjeStranke',$this->zanimanjeStranke,true);
		$criteria->compare('pkv_vk',$this->pkv_vk,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Stranka the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
