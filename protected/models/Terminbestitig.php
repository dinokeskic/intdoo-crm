<?php

/**
 * This is the model class for table "terminbestitig".
 *
 * The followings are the available columns in table 'terminbestitig':
 * @property string $idterminbestitig
 * @property string $idtermin
 * @property string $status
 * @property string $datum
 *
 * The followings are the available model relations:
 * @property KomTermbestitig[] $komTermbestitigs
 * @property Termin $idtermin0
 * @property Terminstatgefunden[] $terminstatgefundens
 */
class Terminbestitig extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'terminbestitig';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		//@todo Add rules based on different scenarios. That is, the create and cancel scenario
		return array(
				array('datum','default','value'=>new CDbExpression('NOW()')),
			array('idtermin', 'length', 'max'=>10),
			array('status', 'length', 'max'=>45),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idterminbestitig, idtermin, status, datum', 'safe', 'on'=>'search'),
		);
	}
public function beforeSave()
{
	$date=new DateTime();
	$this->datum=$date->format("Y-m-d H:i:s");
	return parent::beforeSave();
}
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'komTermbestitigs' => array(self::HAS_MANY, 'KomTermbestitig', 'idterminbestitig'),
			'idtermin0' => array(self::BELONGS_TO, 'Termin', 'idtermin'),
			'terminstatgefundens' => array(self::HAS_MANY, 'Terminstatgefunden', 'idterminbestig'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idterminbestitig' => 'Idterminbestitig',
			'idtermin' => 'Idtermin',
			'status' => 'Status',
			'datum' => 'Datum',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
      $criteria->with=array(
      		'idtermin0',
      		'idtermin0.idwiedervorlage0',
      		'idtermin0.idwiedervorlage0.idstranke0',
      		'idtermin0.idwiedervorlage0.idkorisnik0'=>array('alias'=>'zaposlenik_korisnik'),
      		'idtermin0.idwiedervorlage0.idkorisnik0.zaposlenik'
      		
      				);
      $criteria->together=true;
      if(isset($this->datum) && $this->datum!="")
      {
      	
      	$dateArray=DateHelper::splitDateRange($this->datum);
      	$criteria->addBetweenCondition('t.datum',$dateArray['beginTime'], $dateArray['endTime']);
      }
      if(isset($this->idtermin0->vrijeme) && $this->idtermin0->vrijeme!="")
      {
      	$dateArray=DateHelper::splitDateRange($this->idtermin0->vrijeme);
      	$criteria->addBetweenCondition("idtermin0.vrijeme", $dateArray['beginTime'],$dateArray['endTime']);
      }
      $criteria->compare('idtermin0.status',"bestätigt");
      $criteria->compare('zaposlenik_korisnik.korisnickoIme',$this->idtermin0->idwiedervorlage0->idkorisnik0->korisnickoIme,true);
		/*if(isset($this->status) && $this->status!="")
		{
			$criteria->compare("t.status",$this->status,true);
		}
		else
		{
		$criteria->addInCondition("t.status",array("offen","absage"));
		}*/
		$imePrezimeQuery=$this->idtermin0->idwiedervorlage0->idstranke0->imeStranke;
		$criteria->compare('idstranke0.telefonStranke',$this->idtermin0->idwiedervorlage0->idstranke0->telefonStranke,true);
		Yii::log("Vrijednost imena stranke je: ".$this->idtermin0->idwiedervorlage0->idstranke0->imeStranke);
$criteria->addCondition("idstranke0.imeStranke like
		 '$imePrezimeQuery%' or
		idstranke0.prezimeStranke like '$imePrezimeQuery%'"
		);
		$criteria->compare('idstranke0.adresaStranke',$this->idtermin0->idwiedervorlage0->idstranke0->adresaStranke,true);
                $criteria->compare("idstranke0.lokacija", $this->idtermin0->idwiedervorlage0->idstranke0->lokacija,true);
                $criteria->compare("idwiedervorlage0.cijena",$this->idtermin0->idwiedervorlage0->cijena,true);
		$criteria->compare('idstranke0.godisteStranke',$this->idtermin0->idwiedervorlage0->idstranke0->godisteStranke,true);
		$criteria->compare('idstranke0.pkv_vk',$this->idtermin0->idwiedervorlage0->idstranke0->pkv_vk,true);
		$criteria->compare('idstranke0.zanimanjeStranke',$this->idtermin0->idwiedervorlage0->idstranke0->zanimanjeStranke,true);
		$sort=new CSort();
		$sort->defaultOrder="t.datum";
		$sort->attributes=array(
				'ime'=>array(
						'asc'=>'idstranke0.imeStranke',
						'desc'=>'idstranke0.imeStranke desc'
				),
				'adresa'=>array(
						'asc'=>'idstranke0.adresaStranke',
						'desc'=>'idstranke0.adresaStranke desc',
				),
				'godište'=>array(
						'asc'=>'idstranke0.godisteStranke',
						'desc'=>'idstranke0.adresaStranke desc',
				),
				'zanimanje'=>array(
						'asc'=>'idstranke0.zanimanjeStranke',
						'desc'=>'idstranke0.zanimanjeStranke desc',
				),
				'telefon'=>array(
						'asc'=>'idstranke0.telefonStranke',
						'desc'=>'idstranke0.telefonStranke desc',
				),
				'vrijeme'=>array(
						'asc'=>'idtermin0.vrijeme',
						'desc'=>'idtermin0.vrijeme desc',
				),
				'vrijeme_potvrde'=>array(
						'asc'=>'t.datum',
						'desc'=>'t.datum desc',
				),
				'autor'=>array(
						'asc'=>'zaposlenik_korisnik.korisnickoIme',
						'desc'=>'zaposlenik_korisnik.korisnickoIme desc',
				),
                                'lokacija'=>array(
                                                'asc'=>'idstranke0.lokacija',
                                                'desc'=>'idstranke0.lokacija desc',
                                ),
                                   'cijena'=>array(
                                    'asc'=>'idwiedervorlage0.cijena',
                                    'desc'=>'idwiedervorlage0.cijena desc'
                                )
		);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
				'sort'=>$sort,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Terminbestitig the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
