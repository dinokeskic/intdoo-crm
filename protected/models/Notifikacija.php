<?php

/**
 * This is the model class for table "notifikacija".
 *
 * The followings are the available columns in table 'notifikacija':
 * @property integer $id
 * @property string $vrijeme
 * @property string $text
 * @property string $idkorisnik
 * @property integer $pregledano
 * @property string $tip
 * @property string $code
 *
 * The followings are the available model relations:
 * @property Korisnik $idkorisnik0
 */
class Notifikacija extends CActiveRecord
{
   const MAKLER_INTESTED_TERMIN="makler_interested_termin";
   const MAKLER_ASSIGNED_TERMIN="makler_assigned_termin";
   const WIEDERVORLAGE_APPROACHING="wiedervorlage_approaching";
   const TERMIN_APPROACHING="termin_approaching";
   const WIEDERVORLAGE_CREATED="wiedervorlage_created";
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'notifikacija';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
            $now=new CDbExpression("NOW()");
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array(' text, idkorisnik', 'required'),
			array('pregledano', 'boolean'),
                    array('vrijeme','default','value'=>$now),
			array('text', 'length', 'max'=>200),
			array('idkorisnik', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, vrijeme, text, idkorisnik, pregledano', 'safe', 'on'=>'search'),
		);
	}
public function getCssClass()
{
    if($this->pregledano==true)
        return "seen";
    else
        return "not-seen";
}
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idkorisnik0' => array(self::BELONGS_TO, 'Korisnik', 'idkorisnik'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'vrijeme' => 'Vrijeme',
			'text' => 'Text',
			'idkorisnik' => 'Idkorisnik',
			'pregledano' => 'Pregledano',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('vrijeme',$this->vrijeme,true);
		$criteria->compare('text',$this->text,true);
		$criteria->compare('idkorisnik',$this->idkorisnik,true);
		$criteria->compare('pregledano',$this->pregledano);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Notifikacija the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
