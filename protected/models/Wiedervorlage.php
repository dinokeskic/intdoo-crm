<?php

/**
 * This is the model class for table "wiedervorlage".
 *
 * The followings are the available columns in table 'wiedervorlage':
 * @property string $idwiedervorlage
 * @property string $idstranke
 * @property string $idmakler
 * @property string $idkorisnik
 * @property string $status
 * @property string $vrijeme
 * @property double $cijena
 * 
 *
 * The followings are the available model relations:
 * @property KomWv[] $komWvs
 * @property Termin[] $termins
 * @property Stranka $idstranke0
 * @property Makler $idmakler0
 * @property Korisnik $idkorisnik0
 * 
 */
class Wiedervorlage extends CActiveRecord
{
	public $pocetniDatum;
	public $krajnjiDatum;
	public $oldAttributes;
        public $minPrice;
        public $maxPrice;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		
		return 'wiedervorlage';
	}
public function afterFind()
{
	$this->oldAttributes=$this->attributes;
}
	/**
	 * @return array validation rules for model attributes.
	 */
   
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idstranke, idmakler, idkorisnik, status', 'safe','on'=>'adminCreate,adminUpdate,create,update'),
                        array('cijena', 'numerical' , 'on'=>'adminCreate,adminUpdate'),
                        array('vrijeme','required'),
                      // array('cijena',  'safe',   'on'=>'create','update'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('cijena,idwiedervorlage, idstranke, idmakler, idkorisnik, status, vrijeme', 'safe', 'on'=>'searchByKorisnik,search,searchByMakler,createTermin'),
		);
	}
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'komWvs' => array(self::HAS_MANY, 'KomWv', 'idwiedervorlage'),
			'termins' => array(self::HAS_MANY, 'Termin', 'idwiedervorlage'),
			'idstranke0' => array(self::BELONGS_TO, 'Stranka', 'idstranke'),
			'idkorisnik0' => array(self::BELONGS_TO, 'Korisnik', 'idkorisnik'),
		);
	}
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idwiedervorlage' => Yii::t("main","ID_WIEDERVORLAGE"),
			'idstranke' => Yii::t("main","ID_STRANKA"),
			'idmakler' => Yii::t("main","ID_MAKLER"),
			'idkorisnik' => Yii::t("main","ID_KORISNIK"),
			'status' => Yii::t("main","STATUS"),
			'vrijeme' => Yii::t("main","VRIJEME"),
                        'cijena'=>Yii::t("main","CIJENA"),
		);
	}
	public function beforeValidate()
	{
		Yii::log("beforeValidate pozvana","info","wiedervorlage_before_validate");
		if($this->scenario=="create" || $this->scenario=="update" || $this->scenario=="adminCreate"
                        || $this->scenario=="adminUpdate")
		{
			Yii::log("vrijeme uneseno od korisnika:".$this->vrijeme,"info","wiedervorlage_before_validate");
			Yii::log("Staro vrijeme:".$this->oldAttributes['vrijeme'],"info","wiedervorlage_before_validate");
			if($this->scenario=="update")
			{
			$staroVrijeme=new DateTime($this->vrijeme);
			$novoVrijeme=new DateTime($this->oldAttributes['vrijeme']);
			if($novoVrijeme==$staroVrijeme)
			{
				Yii::log("Vremena se podudaraju!","info","wiedervorlage_before_validate");
			$this->getValidatorList()->removeAt(1);
			}
			}
	   $vrijemeDateTime=new DateTime($this->vrijeme);
	   $this->vrijeme=$vrijemeDateTime->format("Y-m-d H:i:s");
		}
		return parent::beforeValidate();
	} 
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		Yii::log("search method called");
		$criteria=new CDbCriteria;

		$criteria->with=array('idstranke0');
		$criteria->together=true;
                if(Yii::app()->user->tip!="makler")
                {
			$criteria->compare("t.idkorisnik",$this->idkorisnik);
                }
		if(isset($this->vrijeme) && $this->vrijeme!="")
		{
		list($beginTime,$endTime)=explode("-", $this->vrijeme);
		 $date=DateTime::createFromFormat("d.m.Y H:i ", $beginTime);
		$beginTime=$date->format("Y-m-d H:i:s");		
		$date2=DateTime::createFromFormat(" d.m.Y H:i", $endTime);
		$endTime=$date2->format("Y-m-d H:i:s");
		$criteria->addBetweenCondition('t.vrijeme',$beginTime, $endTime);
		}
		if(isset($this->status) && $this->status!="")
		{
			$criteria->compare("t.status",$this->status,true);
		}
                elseif (Yii::app()->user->tip=="makler") {
                $criteria->compare("t.status","offen",true);
            }
		else
		{
                    
		$criteria->addInCondition("t.status",array("offen","absage"));
		}

		$criteria->compare('idstranke0.telefonStranke',$this->idstranke0->telefonStranke,true);
                $criteria->compare("cijena", $this->cijena,true);
                $criteria->compare("idstranke0.lokacija", $this->idstranke0->lokacija,true);
		$imePrezimeQuery=$this->idstranke0->imeStranke;
		$criteria->addCondition("idstranke0.imeStranke like
		 '$imePrezimeQuery%' or
		idstranke0.prezimeStranke like '$imePrezimeQuery%'"
		);
		$criteria->compare('idstranke0.adresaStranke',$this->idstranke0->adresaStranke,true);
		$criteria->compare('idstranke0.godisteStranke',$this->idstranke0->godisteStranke,true);
		$criteria->compare('idstranke0.pkv_vk',$this->idstranke0->pkv_vk,true);
		$criteria->compare('idstranke0.zanimanjeStranke',$this->idstranke0->zanimanjeStranke,true);
		$sort=new CSort();
	$sort->defaultOrder="vrijeme";
			$sort->attributes=array(
							'ime'=>array(
									'asc'=>'idstranke0.imeStranke',
									'desc'=>'idstranke0.prezimeStranke desc'
							),
							'prezime'=>array(
									'asc'=>'idstranke0.prezimeStranke',
									'desc'=>'idstranke0.prezimeStranke desc',
							),
							'adresa'=>array(
									'asc'=>'idstranke0.adresaStranke',
									'desc'=>'idstranke0.adresaStranke desc',
							),
							'godište'=>array(
									'asc'=>'idstranke0.godisteStranke',
									'desc'=>'idstranke0.adresaStranke desc',
							),
							'zanimanje'=>array(
									'asc'=>'idstranke0.zanimanjeStranke',
									'desc'=>'idstranke0.zanimanjeStranke desc',
							),
							'telefon'=>array(
									'asc'=>'idstranke0.telefonStranke',
									'desc'=>'idstranke0.telefonStranke desc',
							),
							'vrijeme'=>array(
									'asc'=>'vrijeme',
									'desc'=>'vrijeme desc',
							),
							'status'=>array(
									'asc'=>'t.status',
									'desc'=>'t.status desc',
							),
                                                          'lokacija'=>array(
                                                              'asc'=>'idstranke0.lokacija',
                                                              'desc'=>'idstranke0.lokacija desc',
                                                          ),
                                                          'cijena'=>array(
                                                              'asc'=>'t.cijena',
                                                              'desc'=>'t.cijena desc',
                                                          )
					);
		$dataProvider= new  CActiveDataProvider($this,array(
				'criteria'=>$criteria,
	
		)			
				);
$dataProvider->sort=$sort;
return $dataProvider;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Wiedervorlage the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
