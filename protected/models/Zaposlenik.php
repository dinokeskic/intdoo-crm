<?php

/**
 * This is the model class for table "zaposlenik".
 *
 * The followings are the available columns in table 'zaposlenik':
 * @property string $idzaposlenik
 * @property string $ime
 * @property string $prezime
 * @property string $idkorisnik
 *
 * The followings are the available model relations:
 * @property Korisnik $idkorisnik0
 */
class Zaposlenik extends CActiveRecord
{
	public $korisnicko_ime;
	public $tipKorisnika;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'zaposlenik';
		
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ime,prezime', 'required','on'=>"create,update",
					'message'=>Yii::t("main","REQUIRED_FIELD"),
		),
		    
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idzaposlenik, ime, prezime, idkorisnik', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idkorisnik0' => array(self::BELONGS_TO, 'Korisnik', 'idkorisnik'),
			
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idzaposlenik' => 'ID Zaposlenika',
			'ime' => Yii::t("main","IME"),
			'prezime' => Yii::t("main","PREZIME"),
			'idkorisnik' => "ID korisnika",
				'tipKorisnika'=>Yii::t("main","KORISNICKE_PRIVILEGIJE"),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->with=array('idkorisnik0');
		$criteria->together=true;
		$criteria->compare('ime',$this->ime,true);
		$criteria->compare('prezime',$this->prezime,true);
		$criteria->compare('idkorisnik0.korisnickoIme',$this->idkorisnik0->korisnickoIme,true);
       $criteria->compare('idkorisnik0.tip',$this->idkorisnik0->tip,true);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
				'sort'=>array(
						'attributes'=>array(
						'ime'=>array(
								'asc'=>'ime',
								'desc'=>'ime desc',
				),
						'prezime'=>array(
								'asc'=>'prezime',
								'desc'=>'prezime desc',
						),
						'idkorisnik0.korisnickoIme'=>array(
								'asc'=>'idkorisnik0.korisnickoIme',
								'desc'=>'idkorisnik0.korisnickoIme desc',
						),
						'idkorisnik0.tip'=>array(
								'asc'=>'idkorisnik0.tip',
								'desc'=>'idkorisnik0.tip desc',
						)
								)
		)
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Zaposlenik the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
