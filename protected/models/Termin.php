<?php

/**
 * This is the model class for table "termin".
 *
 * The followings are the available columns in table 'termin':
 * @property string $idtermin
 * @property string $idwiedervorlage
 * @property string $vrijeme
 * @property string $status
 * @property string $rok_za_potvrdu
 *
 * The followings are the available model relations:
 * @property KomTermin[] $komTermins
 * @property Wiedervorlage $idwiedervorlage0
 * @property Terminbestitig[] $terminbestitigs
 * @property MaklerWantsTermin[] $interestedMaklers
 * @property MaklerOwnsTermin  $assignedMakler
 */
class Termin extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'termin';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array(' vrijeme, rok_za_potvrdu', 'required'),
			array('idwiedervorlage', 'length', 'max'=>10),
			array('status', 'length', 'max'=>45),
				array('idwiedervorlage,status','safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idtermin, idwiedervorlage, vrijeme, status, rok_za_potvrdu', 'safe', 'on'=>'search'),
				
				
		);
	}
	public function isMaklerAssigned()
	{
		if($this->assignedMakler!=null)
		return true;
		else 
			return false;
	}
	public function isMaklerAssignedString()
	{
		if($this->isMaklerAssigned())
			return "true";
		else
			return "false";
	}
	public function assignedToCurrentUser()
	{
		if($this->assignedMakler!=null)
		{
		return Yii::app()->user->name==$this->assignedMakler->makler->idkorisnik0->korisnickoIme;
		}
		else
		{
			Yii::log("assignedMakler je null",'info',"assignedToCurrentUser");
			return false;
		}
	
	}
	public function currentUserInterested()
	{
		$criteria=new CDbCriteria();
		$criteria->with=array('makler','makler.idkorisnik0');
		$criteria->compare('idkorisnik0.korisnickoIme',Yii::app()->user->name);
		$criteria->compare('idtermin',$this->idtermin);
		$mwt=MaklerWantsTermin::model()->find($criteria);
		if($mwt==null)
			return false;
		else
			return true;
	}
	
 public function interestedMaklersProvider()
 {
 	$criteria=new CDbCriteria();
 	$criteria->compare("idtermin",$this->idtermin);
 	return new CActiveDataProvider('MaklerWantsTermin',array(
 			'criteria'=>$criteria,
 			'sort'=>array(
 					'defaultOrder'=>'t.vrijeme',
 	),
 			'pagination'=>array(
 					'pageSize'=>5,
 			)
 			
 	));
 	
 }
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'komTermins' => array(self::HAS_MANY, 'KomTermin', 'idtermin'),
			'idwiedervorlage0' => array(self::BELONGS_TO, 'Wiedervorlage', 'idwiedervorlage'),
			'terminbestitigs' => array(self::HAS_MANY, 'Terminbestitig', 'idtermin'),
			'interestedMaklers'=>array(self::HAS_MANY,"MaklerWantsTermin",'idtermin'),
			'assignedMakler'=>array(self::HAS_ONE,"MaklerOwnsTermin",'idtermin'),
		);
	}
	
	
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idtermin' => Yii::t("main","ID_TERMINA"),
			'idwiedervorlage' => Yii::t("main","ID_WIEDERVORLAGE"),
			'vrijeme' => Yii::t("main","VRIJEME"),
			'status' => Yii::t("main","Status"),
			'rok_za_potvrdu' => Yii::t("main","ROK_ZA_POTVRDU"),
		);
	}
	public function beforeValidate()
	{
		if($this->scenario=="create")
		{
			Yii::log("vrijeme in before validate:".$this->vrijeme,"info","beforeValidate");
	   $vrijemeDateTime=new DateTime($this->vrijeme);
	   $this->vrijeme=$vrijemeDateTime->format("Y-m-d H:i:s");
	   $rokDateTime=new DateTime($this->rok_za_potvrdu);
	   $this->rok_za_potvrdu=$rokDateTime->format("Y-m-d H:i:s");
		}
		return parent::beforeValidate();
	} 
	public function getCssClass()
	{
            if($this->isMaklerAssigned())
                return "success";
		switch($this->status)
		{
			case "offen" :
					{
						return "";
					}
			case "absage" :
				{
					return "error";
				}
			case "bestetigung":
					{
						return "success";
					}
			default :
				{
					return "";
				}		
		}
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
            $now=new CDbExpression("NOW()");
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
      $criteria->with=array(
      		'idwiedervorlage0',
      		'idwiedervorlage0.idstranke0',
      		'idwiedervorlage0.idkorisnik0'=>array('alias'=>'zaposlenik_korisnik'),
      		'idwiedervorlage0.idkorisnik0.zaposlenik',
      		'assignedMakler',
      		'assignedMakler.makler',
      		'assignedMakler.makler.idkorisnik0'=>array('alias'=>'makler_korisnik'),
      		
      				);
                        
      $criteria->together=true;
      if($this->scenario=='mojiTermini')
      {
      	$criteria->compare("makler_korisnik.korisnickoIme", Yii::app()->user->name);
    
      }
      if($this->scenario=="UserIsMakler")
      {
          $criteria->addCondition("assignedMakler.idtermin is   null");
      }
      if(isset($this->vrijeme) && $this->vrijeme!="")
      {
      	Yii::log("vrijeme je postavljeno","info","Termin::search");
      	$dateArray=DateHelper::splitDateRange($this->vrijeme);
        $nowDate=new DateTime();
        $nowString=$nowDate->format("Y-m-d H:i:s");
        $beginTime=  DateTime::createFromFormat("Y-m-d H:i:s",$dateArray['beginTime']);
        if(Yii::app()->user->isMakler() && $beginTime<$nowDate  )
        {
            $criteria->addCondition("t.vrijeme > ".$now);
        }
        else {
      	$criteria->addBetweenCondition('t.vrijeme',$dateArray['beginTime'], $dateArray['endTime']);
        }
      }
      if(isset($this->rok_za_potvrdu) && $this->rok_za_potvrdu!="")
      {
      	$dateArray=DateHelper::splitDateRange($this->rok_za_potvrdu);
      	$criteria->addBetweenCondition("t.rok_za_potvrdu", $dateArray['beginTime'],$dateArray['endTime']);
      }
      
      
      $criteria->compare('zaposlenik_korisnik.korisnickoIme',$this->idwiedervorlage0->idkorisnik0->korisnickoIme,true);
		if(isset($this->status) && $this->status!="")
		{
			$criteria->compare("t.status",$this->status,true);
		}
		else
		{
                    if(Yii::app()->user->tip=="makler")
                        $criteria->compare ("t.status", "offen");
                    else
		$criteria->addInCondition("t.status",array("offen","absage"));
		}
		$imePrezimeQuery=$this->idwiedervorlage0->idstranke0->imeStranke;
		$criteria->compare('idstranke0.telefonStranke',$this->idwiedervorlage0->idstranke0->telefonStranke,true);
                $criteria->compare("idstranke0.lokacija", $this->idwiedervorlage0->idstranke0->lokacija,true);
                $criteria->compare("idwiedervorlage0.cijena",$this->idwiedervorlage0->cijena,true);
		Yii::log("Vrijednost imena stranke je: ".$this->idwiedervorlage0->idstranke0->imeStranke);
$criteria->addCondition("idstranke0.imeStranke like
		 '$imePrezimeQuery%' or
		idstranke0.prezimeStranke like '$imePrezimeQuery%'"
		);
		$criteria->compare('idstranke0.adresaStranke',$this->idwiedervorlage0->idstranke0->adresaStranke,true);
		$criteria->compare('idstranke0.godisteStranke',$this->idwiedervorlage0->idstranke0->godisteStranke,true);
		$criteria->compare('idstranke0.pkv_vk',$this->idwiedervorlage0->idstranke0->pkv_vk,true);
		$criteria->compare('idstranke0.zanimanjeStranke',$this->idwiedervorlage0->idstranke0->zanimanjeStranke,true);
		$sort=new CSort();
		$sort->defaultOrder="t.vrijeme";
		$sort->attributes=array(
				'ime'=>array(
						'asc'=>'idstranke0.imeStranke',
						'desc'=>'idstranke0.imeStranke desc'
				),
				'adresa'=>array(
						'asc'=>'idstranke0.adresaStranke',
						'desc'=>'idstranke0.adresaStranke desc',
				),
				'godište'=>array(
						'asc'=>'idstranke0.godisteStranke',
						'desc'=>'idstranke0.adresaStranke desc',
				),
				'zanimanje'=>array(
						'asc'=>'idstranke0.zanimanjeStranke',
						'desc'=>'idstranke0.zanimanjeStranke desc',
				),
				'telefon'=>array(
						'asc'=>'idstranke0.telefonStranke',
						'desc'=>'idstranke0.telefonStranke desc',
				),
				'vrijeme'=>array(
						'asc'=>'t.vrijeme',
						'desc'=>'t.vrijeme desc',
				),
				'rok_za_potvrdu'=>array(
						'asc'=>'t.rok_za_potvrdu',
						'desc'=>'t.rok_za_potvrdu desc',
				),
				'status'=>array(
						'asc'=>'t.status',
						'desc'=>'t.status desc',
				),
				'autor'=>array(
						'asc'=>'zaposlenik_korisnik.korisnickoIme',
						'desc'=>'zaposlenik_korisnik.korisnickoIme desc',
				),
                                'lokacija'=>array(
                                    'asc'=>'idstranke0.lokacija',
                                    'desc'=>'idstranke0.lokacija desc',
                                ),
                                'cijena'=>array(
                                    'asc'=>'idwiedervorlage0.cijena',
                                    'desc'=>'idwiedervorlage0.cijena desc'
                                )
		);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
				'sort'=>$sort,
                    'pagination'=>array(
                        'pageSize'=>5,
                    )
		));
	}
        
        
	/*
	 * As the name of the method implies,
	 * it simply checks if the user in current session is the author 
	 * of this termin instance
	 */
	public function currentUserIsAuthor()
	{
		$authorUsername=$this->idwiedervorlage0->idkorisnik0->korisnickoIme;
		return $authorUsername==Yii::app()->user->name;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Termin the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
