<?php

/**
 * This is the model class for table "kom_wv".
 *
 * The followings are the available columns in table 'kom_wv':
 * @property string $idkom_wv
 * @property string $idwiedervorlage
 * @property string $text
 * @property string $datum
 *
 * The followings are the available model relations:
 * @property Wiedervorlage $idwiedervorlage0
 */
class KomWv extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'kom_wv';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('text,idkom_wv,idwiedervorlage,datum', 'safe'),
				array('datum','default','value'=>new CDbExpression('NOW()')),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idkom_wv, idwiedervorlage, text, datum', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idwiedervorlage0' => array(self::BELONGS_TO, 'Wiedervorlage', 'idwiedervorlage'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idkom_wv' =>Yii::t("main","ID_KOMENTAR"),
			'idwiedervorlage' => Yii::t("main","ID_WIEDERVORLAGE"),
			'text' => Yii::t("main","TEKST"),
			'datum' => Yii::t("main","DATUM"),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idkom_wv',$this->idkom_wv,true);
		$criteria->compare('idwiedervorlage',$this->idwiedervorlage,true);
		$criteria->compare('text',$this->text,true);
		$criteria->compare('datum',$this->datum,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return KomWv the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
