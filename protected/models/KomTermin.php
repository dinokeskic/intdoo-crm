<?php

/**
 * This is the model class for table "kom_termin".
 *
 * The followings are the available columns in table 'kom_termin':
 * @property string $idkom_termin
 * @property string $idtermin
 * @property string $datum
 * @property string $text
 *
 * The followings are the available model relations:
 * @property Termin $idtermin0
 */
class KomTermin extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'kom_termin';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idtermin, text', 'required'),
				array('datum','default','value'=>new CDbExpression('NOW()')),
			array('idtermin', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idkom_termin, idtermin, datum, text', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idtermin0' => array(self::BELONGS_TO, 'Termin', 'idtermin'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idkom_termin' => 'Idkom Termin',
			'idtermin' => 'Idtermin',
			'datum' => 'Datum',
			'text' => 'Text',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idkom_termin',$this->idkom_termin,true);
		$criteria->compare('idtermin',$this->idtermin,true);
		$criteria->compare('datum',$this->datum,true);
		$criteria->compare('text',$this->text,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return KomTermin the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
