<?php

/**
 * This is the model class for table "kom_termbestitig".
 *
 * The followings are the available columns in table 'kom_termbestitig':
 * @property string $idkom_termbestitig
 * @property string $idterminbestitig
 * @property string $datum
 * @property string $text
 *
 * The followings are the available model relations:
 * @property Terminbestitig $idterminbestitig0
 */
class KomTermbestitig extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'kom_termbestitig';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idterminbestitig, datum, text', 'required'),
			array('idterminbestitig', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idkom_termbestitig, idterminbestitig, datum, text', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idterminbestitig0' => array(self::BELONGS_TO, 'Terminbestitig', 'idterminbestitig'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idkom_termbestitig' => 'Idkom Termbestitig',
			'idterminbestitig' => 'Idterminbestitig',
			'datum' => 'Datum',
			'text' => 'Text',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idkom_termbestitig',$this->idkom_termbestitig,true);
		$criteria->compare('idterminbestitig',$this->idterminbestitig,true);
		$criteria->compare('datum',$this->datum,true);
		$criteria->compare('text',$this->text,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return KomTermbestitig the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
