<?php

/**
 * This is the model class for table "narudzba_termina".
 *
 * The followings are the available columns in table 'narudzba_termina':
 * @property string $idnarudzba
 * @property integer $godina
 * @property integer $mjesec
 * @property string $idmakler
 * @property integer $br_termina
 * @property string $vrijeme_narudzbe
 *
 * The followings are the available model relations:
 * @property Lokacija $idmakler0
 */
class NarudzbaTermina extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'narudzba_termina';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
            $now=new CDbExpression("NOW()");
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idmakler, br_termina', 'required'),
                    array('vrijeme_narudzbe','default','value'=>$now),
			array('idmakler', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idmakler, godina, mjesec','safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idmakler0' => array(self::BELONGS_TO, 'Lokacija', 'idmakler'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idnarudzba' => 'Idnarudzba',
			'godina' => 'Godina',
			'mjesec' => 'Mjesec',
			'idmakler' => 'Idmakler',
			'br_termina' => Yii::t("main","BROJ_TERMINA"),
			'vrijeme_narudzbe' => 'Vrijeme Narudzbe',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idnarudzba',$this->idnarudzba,true);
		$criteria->compare('godina',$this->godina);
		$criteria->compare('mjesec',$this->mjesec);
		$criteria->compare('idmakler',$this->idmakler,true);
		$criteria->compare('br_termina',$this->br_termina);
		$criteria->compare('vrijeme_narudzbe',$this->vrijeme_narudzbe,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return NarudzbaTermina the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
