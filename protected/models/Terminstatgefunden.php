<?php

/**
 * This is the model class for table "terminstatgefunden".
 *
 * The followings are the available columns in table 'terminstatgefunden':
 * @property string $idterminstatgefunden
 * @property string $idterminbestig
 * @property double $cijena
 * @property string $datum
 *
 * The followings are the available model relations:
 * @property KomTsg[] $komTsgs
 * @property Terminbestitig $idterminbestig0
 */
class Terminstatgefunden extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'terminstatgefunden';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
				array('idterminbestig,cijena','safe'),
				array('datum','default','value'=>new CDbExpression('NOW()')),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idterminstatgefunden, idterminbestig, cijena, datum', 'safe', 'on'=>'search'),
		);
	}
        public function getFirmName()
        {
            if($this->idterminbestig0->idtermin0->isMaklerAssigned())
            {
                if($this->idterminbestig0->idtermin0->assignedMakler->makler->firma!=null)
                return $this->idterminbestig0->idtermin0->assignedMakler->makler->firma->naziv;
                else
                    return "";
            }
            else
            {
                return "";
            }
        }
        public function getMaklerUsername()
        {
            if($this->idterminbestig0->idtermin0->isMaklerAssigned())
            {
                return $this->idterminbestig0->idtermin0->assignedMakler->makler->idkorisnik0->korisnickoIme;
            }
 else {
     return "";
 }
        }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'komTsgs' => array(self::HAS_MANY, 'KomTsg', 'idtsg'),
			'idterminbestig0' => array(self::BELONGS_TO, 'Terminbestitig', 'idterminbestig'),
		);
	}
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idterminstatgefunden' => 'Idterminstatgefunden',
			'idterminbestig' => 'Idterminbestig',
			'cijena' => Yii::t("main","CIJENA"),
			'datum' => 'Datum',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
      $criteria->with=array(
      		'idterminbestig0',
      		'idterminbestig0.idtermin0',
      		'idterminbestig0.idtermin0.idwiedervorlage0.idstranke0',
      		'idterminbestig0.idtermin0.idwiedervorlage0.idkorisnik0'=>array('alias'=>'zaposlenik_korisnik'),
      		'idterminbestig0.idtermin0.idwiedervorlage0.idkorisnik0.zaposlenik',
                'idterminbestig0.idtermin0.assignedMakler.makler.idkorisnik0'=>array('alias'=>'makler_korisnik'),
                'idterminbestig0.idtermin0.assignedMakler.makler.firma',
      		
      				);
      
      $criteria->together=true;
      if(isset($this->idterminbestig0->idtermin0->vrijeme) && $this->idterminbestig0->idtermin0->vrijeme!="")
      {
      	$dateArray=DateHelper::splitDateRange($this->idterminbestig0->idtermin0->vrijeme);
      	$criteria->addBetweenCondition("idtermin0.vrijeme", $dateArray['beginTime'],$dateArray['endTime']);
      }
      $criteria->compare("firma.naziv", $this->idterminbestig0->idtermin0->assignedMakler->makler->firma->naziv,true);
      $criteria->compare("makler_korisnik.korisnickoIme", $this->idterminbestig0->idtermin0->assignedMakler->makler->idkorisnik0->korisnickoIme,true);
      $criteria->compare('zaposlenik_korisnik.korisnickoIme',$this->idterminbestig0->idtermin0->idwiedervorlage0->idkorisnik0->korisnickoIme,true);
		/*if(isset($this->status) && $this->status!="")
		{
			$criteria->compare("t.status",$this->status,true);
		}
		else
		{
		$criteria->addInCondition("t.status",array("offen","absage"));
		}*/
		$imePrezimeQuery=$this->idterminbestig0->idtermin0->idwiedervorlage0->idstranke0->imeStranke;
		$criteria->compare('idstranke0.telefonStranke',$this->idterminbestig0->idtermin0->idwiedervorlage0->idstranke0->telefonStranke,true);
		Yii::log("Vrijednost imena stranke je: ".$this->idterminbestig0->idtermin0->idwiedervorlage0->idstranke0->imeStranke);
$criteria->addCondition("idstranke0.imeStranke like
		 '$imePrezimeQuery%' or
		idstranke0.prezimeStranke like '$imePrezimeQuery%'"
		);
		$criteria->compare('idstranke0.adresaStranke',$this->idterminbestig0->idtermin0->idwiedervorlage0->idstranke0->adresaStranke,true);
                $criteria->compare("idstranke0.lokacija", $this->idterminbestig0->idtermin0->idwiedervorlage0->idstranke0->lokacija);
		$criteria->compare('idstranke0.godisteStranke',$this->idterminbestig0->idtermin0->idwiedervorlage0->idstranke0->godisteStranke,true);
		$criteria->compare('idstranke0.pkv_vk',$this->idterminbestig0->idtermin0->idwiedervorlage0->idstranke0->pkv_vk,true);
		$criteria->compare('idstranke0.zanimanjeStranke',$this->idterminbestig0->idtermin0->idwiedervorlage0->idstranke0->zanimanjeStranke,true);
		$sort=new CSort();
		$sort->defaultOrder="idtermin0.vrijeme";
		$sort->attributes=array(
				'ime'=>array(
						'asc'=>'idstranke0.imeStranke',
						'desc'=>'idstranke0.imeStranke desc'
				),
				'adresa'=>array(
						'asc'=>'idstranke0.adresaStranke',
						'desc'=>'idstranke0.adresaStranke desc',
				),
				'godište'=>array(
						'asc'=>'idstranke0.godisteStranke',
						'desc'=>'idstranke0.adresaStranke desc',
				),
				'zanimanje'=>array(
						'asc'=>'idstranke0.zanimanjeStranke',
						'desc'=>'idstranke0.zanimanjeStranke desc',
				),
				'telefon'=>array(
						'asc'=>'idstranke0.telefonStranke',
						'desc'=>'idstranke0.telefonStranke desc',
				),
				'vrijeme'=>array(
						'asc'=>'idtermin0.vrijeme',
						'desc'=>'idtermin0.vrijeme desc',
				),
				'autor'=>array(
						'asc'=>'zaposlenik_korisnik.korisnickoIme',
						'desc'=>'zaposlenik_korisnik.korisnickoIme desc',
				),
				'cijena'=>array(
						'asc'=>'t.cijena',
						'desc'=>'t.cijena desc',
				),
                                'makler'=>array(
                                    'asc'=>'makler_korisnik.korisnickoIme',
                                    'desc'=>'makler_korisnik.korisnickoIme desc',
                                ),
                                'firma'=>array(
                                        'asc'=>'firma.naziv',
                                        'desc'=>'firma.naziv desc',
                                ),
                                   'lokacija'=>array(
                                                'asc'=>'idstranke0.lokacija',
                                                'desc'=>'idstranke0.lokacija desc',
                                )
                    
		);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
				'sort'=>$sort,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Terminstatgefunden the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
