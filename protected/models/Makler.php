<?php

/**
 * This is the model class for table "makler".
 *
 * The followings are the available columns in table 'makler':
 * @property string $idmakler
 * @property string $ime
 * @property string $prezime
 * @property string $adresa
 * @property string $lokacija
 * @property string $idkorisnik
 * @property string $telefon
 * @property string $komentar
 * @property integer $idfirma
 *
 * The followings are the available model relations:
 * @property Korisnik $idkorisnik0
 * @property Lokacija[] $lokacije
 * @property MaklerOwnsTermin[] $terminsInterestedIn
 * @property MaklerWantsTermin[] $assignedTermins
 * @property Firma $firma 
 */
class Makler extends CActiveRecord
{
	public $lokacijeString="";
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'makler';
	}
        /*
         * @return NarudzbaTermina  order of appointments with the latest date
         */
        public function getPosljednjaNarudzba()
        {
            $criteria=new CDbCriteria();
            $criteria->compare("idmakler", $this->idmakler);
           $criteria->order="vrijeme_narudzbe desc";
          $narudzbe=  NarudzbaTermina::model()->findAll($criteria);
          if(!empty($narudzbe))
          {
              return $narudzbe[0];
          }
          else
          {
              return null;
          }
          
        }
        /*
         * @return integer number of assigned appointments since last order
         */
        public function getBrDodijeljenihTerminaOdNarudzbe()
        {
           $narudzba=$this->getPosljednjaNarudzba();
           if($narudzba!=null)
           {
            $criteria=new CDbCriteria();
            $now=new CDbExpression("NOW()");
            $criteria->with=array("termin");
            $criteria->together=true;
            $criteria->addCondition("termin.vrijeme > ".$now);
            $criteria->compare("idmakler", $this->idmakler);
            $mots=  MaklerOwnsTermin::model()->findAll($criteria);
            return    count($mots);
           }
           else {
               return 0;
           }
        }
        public function getProcenatOstvarenihTermina()
        {
            $brTermina=$this->getPosljednjaNarudzba()->br_termina;
           $brDodTermina=$this->getBrDodijeljenihTerminaOdNarudzbe();
           return NumUtil::percentage($brTermina, $brDodTermina);
        }
        public function getProgressColor()
        {
            $procenat=$this->getProcenatOstvarenihTermina();
            if($procenat<=30)
            {
                return "danger";
            }
            elseif ($procenat>30 && $procenat<=70) {
            return "warning";
        }
 else {
     return "success";
 }
        }
	public static function allMaklersArray()
	{
		$maklers=Makler::model()->findAll();
		$jsonArray=array();
		foreach($maklers as $makler)
		{
			$json['korisnickoIme']=$makler->idkorisnik0->korisnickoIme;
			$json['ime']=$makler->ime;
			$json['prezime']=$makler->prezime;
			$json['adresa']=$makler->adresa;
			$json['telefon']=$makler->telefon;
			$json['id']=$makler->idmakler;
			$json['text']=$makler->ime." ".$makler->prezime;
			array_push($jsonArray,$json);
		}
    return $jsonArray;
	}
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ime, prezime, adresa', 'required'),
                        array('komentar','safe'),
				array('tip','safe','on'=>'create,update'),
			array('ime, prezime, adresa', 'length', 'max'=>45),
			array('lokacija', 'length', 'max'=>1000),
			array('idfirma', 'length', 'max'=>10),
                        array('email', 'length', 'max'=>45),
                        array('telefon', 'length', 'max'=>100),
                    
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idmakler, ime, prezime, adresa, lokacija', 'safe', 'on'=>'search'),
		);
	}
public function afterFind()
{
	foreach($this->lokacije as $lokacija)
	{
		
	}
}
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idkorisnik0' => array(self::BELONGS_TO, 'Korisnik', 'idkorisnik'),
			'wiedervorlages' => array(self::HAS_MANY, 'Wiedervorlage', 'idmakler'),
			'terminsInterestedIn'=>array(self::HAS_MANY,"MaklerWantsTermin",'idmakler'),
			'assignedTermins'=>array(self::HAS_MANY,"MaklerOwnsTermin","idmakler"),
                         'narudzbe'=>array(self::HAS_ONE,'NarudzbaTermina',"idmakler",),
			'lokacije'=>array(self::HAS_MANY,"Lokacija",'idmakler'),
                        'firma'=>array(self::BELONGS_TO,"Firma",'idfirma'),
		);
	}
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idmakler' => Yii::t("main","ID_MAKLER"),
			'ime' => Yii::t("main","IME"),
			'prezime' => Yii::t("main","PREZIME"),
			'adresa' => Yii::t("main","ADRESA"),
                     	'lokacija' => Yii::t("main","LOKACIJA"),
			'idkorisnik' => Yii::t("main","ID_KORISNIK"),
                        'komentar'=>Yii::t("main","KOMENTAR")
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		$criteria=new CDbCriteria;
                $criteria->compare("idkorisnik0.lozinka", $this->ime);
     $criteria->with=array('idkorisnik0');
     $criteria->together=true;
     	$criteria->compare('idkorisnik0.korisnickoIme',$this->idkorisnik0->korisnickoIme,true);
		$criteria->compare('idmakler',$this->idmakler,true);
		$criteria->compare('ime',$this->ime,true);
		$criteria->compare('prezime',$this->prezime,true);
		$criteria->compare('adresa',$this->adresa,true);
		$criteria->compare('lokacija',$this->lokacija,true);
		$criteria->compare('idkorisnik',$this->idkorisnik,true);
                $criteria->compare("telefon", $this->telefon,true);
                $criteria->compare("email", $this->email,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
				'sort'=>array(
						'attributes'=>array(
								'ime'=>array(
										'asc'=>'ime asc',
										'desc'=>'ime desc',
						),
								'prezime'=>array(
										'asc'=>'prezime asc',
										'desc'=>'prezime desc',
								),
								'adresa'=>array(
										'asc'=>'adresa asc',
										'desc'=>'adresa desc',
								),
								'lokacija'=>array(
										'asc'=>'lokacija asc',
										'desc'=>'lokacija desc',
								),
                                                                'telefon'=>array(
                                                                    'asc'=>'telefon',
                                                                       'desc'=>'telefon desc',
                                                                ),
                                                                'email'=>array(
                                                                    'asc'=>'email',
                                                                    'desc'=>'email desc',
                                                                ),
                                                                'korisnickoIme'=>array(
                                                                    'asc'=>'idkorisnik0.korisnickoIme',
                                                                    'desc'=>'idkorisnik0.korisnickoIme desc',
                                                                )
				)
		),
                    'pagination'=>array(
                        'pageSize'=>5,
                    )
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Makler the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
