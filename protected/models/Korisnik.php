<?php

/**
 * This is the model class for table "korisnik".
 *
 * The followings are the available columns in table 'korisnik':
 * @property string $id
 * @property string $korisnickoIme
 * @property string $lozinka
 * @property string $status
 * @property string $tip
 *
 * The followings are the available model relations:
 * @property Makler[] $maklers
 * @property Wiedervorlage[] $wiedervorlages
 * @property Zaposlenik $zaposlenik
 * @property Notifikacija[] $notifikacije
 * @property integer $notifikacijeCount
 */
class Korisnik extends CActiveRecord
{
	public $identitet;
	public $zapamtiMe;
	public $lozinka2;
	public $staraLozinka;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'korisnik';
		
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
				array('staraLozinka,zapamtiMe','safe','on'=>'adminMijenjaLozinku'),
				array('korisnickoIme','unique','on'=>"create,update,makler_create,makler_update",
						"message"=>Yii::t("main","KORISNIK_UNIQUE_MESSAGE"),
		),
			array('tip','required','on'=>"create,update",
					'message'=>Yii::t("main","TIP_REQUIRED"),
		),
				array('tip','safe','on'=>'makler_create,makler_update'),
				array('staraLozinka','required','on'=>'promjenaLozinke',
						'message'=>Yii::t("main","STARA_LOZINKA_REQUIRED"),
		),
			array('status','default','value'=>'blabla'),
			array('korisnickoIme', 'required','on'=>'login,create,update,makler_create,makler_update',
					'message'=>Yii::t("main","KORISNICKO_IME_REQUIRED"),
		),


				array('lozinka', 'required','on'=>'login,create,promjenaLozinke,adminMijenjaLozinku,makler_create',
						'message'=>Yii::t("main","LOZINKA_REQUIRED_MESSAGE"),
				),

		    array('lozinka2','required','on'=>'create,promjenaLozinke,adminMijenjaLozinku,makler_create',
		    		'message'=>Yii::t("main","LOZINKA2_REQUIRED"),
		),
		    array('lozinka','compare','compareAttribute'=>'lozinka2','on'=>"create,promjenaLozinke,adminMijenjaLozinku,makler_create",
		    		"message"=>Yii::t("main","LOZINKE_NO_MATCH"),
		   
		),
	  array('korisnickoIme,tip,lozinka,lozinka2,status,zapamtiMe',"safe",'on'=>'search'),
		
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
		
		);
	}
public function checkForNotifications()
{
    if(Yii::app()->user->tip=="zaposlenik" || Yii::app()->user->tip=="admin")
    {
        Yii::log("korisnik je zaposlenik","info");
    $dateTime=new DateTime();
    $dateTime->add(new DateInterval("PT1H30M"));
    $now=new DateTime();
    $criteria=new CDbCriteria();
    $criteria->compare("idkorisnik", $this->id);
    $criteria->addBetweenCondition("vrijeme",$now->format("Y-m-d H:i:s"),
           $dateTime->format("Y-m-d H:i:s"));
    $wiedervorlages=  Wiedervorlage::model()->findAll($criteria);
    Yii::log("broj wiedervorlage-a:".count($wiedervorlages),"info");
    foreach ($wiedervorlages as $wiedervorlage)
    {
        $dateEnd=new DateTime($wiedervorlage->vrijeme);
        $dateStart=new DateTime($wiedervorlage->vrijeme);
        $dateStart->sub(new DateInterval("PT1H30M"));
       
        $nowTime=new DateTime();
        Yii::log("sistemsko vrijeme je:".$nowTime->format("H:i"),"info","sistemskoVrijeme");
            $criteria=new CDbCriteria();
            Yii::log("pocetno vrijeme:".$dateStart->format("d.m.Y H:i"),"info","pocetnoVrijeme");
            Yii::log("krajnje vrijeme:".$dateEnd->format("d.m.Y H:i"),"info","krajnjeVrijeme");
            $criteria->compare("idkorisnik", $this->id);
         $criteria->addBetweenCondition("vrijeme", $dateStart->format("Y-m-d H:i:s"),$dateEnd->format("Y-m-d H:i:s"));
         $criteria->compare("code",$wiedervorlage->idwiedervorlage);
         $notifikacije=  Notifikacija::model()->findAll($criteria);
         if(empty($notifikacije))
         {
             $not=new Notifikacija();
             $not->idkorisnik=  $this->id;
             $wiedervorlageLink=  CHtml::link("wiedervorlage",array(
                 'wiedervorlage/view',
                 'id'=>$wiedervorlage->idwiedervorlage,
             ));
             $dt=new DateTime($wiedervorlage->vrijeme);
             $vrijeme= $dt->format("H:i");
             $not->text=Yii::t("main","PODSJETNIK_WIEDERVORLAGE",array(
                 '{wiedervorlageLink}'=>$wiedervorlageLink,
                 '{vrijeme}'=>$vrijeme,
             ));
             $not->pregledano=0;
             $not->tip=  Notifikacija::WIEDERVORLAGE_APPROACHING;
             $not->code=$wiedervorlage->idwiedervorlage;
             $not->save();
         }
    }
  
    }
    else
    {
        Yii::log("korisnik je makler","info","tipKorisnika");
    $dateTime=new DateTime();
    $dateTime->add(new DateInterval("PT1H30M"));
    $now=new DateTime();
    $criteria=new CDbCriteria();
    $criteria->with=array('assignedMakler','assignedMakler.makler');
    $criteria->together=true;
    $criteria->compare("makler.idkorisnik", $this->id);
    $criteria->addBetweenCondition("t.vrijeme",$now->format("Y-m-d H:i:s"),
           $dateTime->format("Y-m-d H:i:s"));
    $termins=  Termin::model()->findAll($criteria);
    Yii::log("broj termin-a:".count($termins),"info");
    foreach ($termins as $termin)
    {
        $dateEnd=new DateTime($termin->vrijeme);
        $dateStart=new DateTime($termin->vrijeme);
        $dateStart->sub(new DateInterval("PT1H30M"));
            $criteria=new CDbCriteria();
            $criteria->compare("idkorisnik", $this->id);
         $criteria->addBetweenCondition("vrijeme", $dateStart->format("Y-m-d H:i:s"),$dateEnd->format("Y-m-d H:i:s"));
         $criteria->compare("code",$termin->idtermin);
         $notifikacije=  Notifikacija::model()->findAll($criteria);
         if(empty($notifikacije))
         {
             $not=new Notifikacija();
             $not->idkorisnik=  $this->id;
             $terminLink=  CHtml::link("termin",array(
                 'termin/view',
                 'id'=>$termin->idtermin,
             ));
             $dt=new DateTime($termin->vrijeme);
             $vrijeme= $dt->format("H:i");
             $not->text=Yii::t("main","PODSJETNIK_TERMIN",array(
                 '{terminLink}'=>$terminLink,
                 '{vrijeme}'=>$vrijeme,
             ));
             $not->pregledano=0;
             $not->tip=  Notifikacija::TERMIN_APPROACHING;
             $not->code=$termin->idtermin;
             $not->save();
         }
    }
    }
}
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'maklers' => array(self::HAS_MANY, 'Makler', 'idkorisnik'),
			'wiedervorlages' => array(self::HAS_MANY, 'Wiedervorlage', 'idkorisnik'),
			'zaposlenik' => array(self::HAS_ONE, 'Zaposlenik', array('idkorisnik'=>"id")),
                    'notifikacije'=>array(self::HAS_MANY,'Notifikacija','idkorisnik',
                        'order'=>'notifikacije.vrijeme desc', 'limit'=>15),
                    'notifikacijeCount'=>array(self::STAT,'Notifikacija','idkorisnik',
                        'condition'=>'pregledano=0'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		switch($this->scenario)
		{
			case 'promjenaLozinke':
				{
					return array(
							'id' => 'ID',
							'korisnickoIme' => Yii::t("main","KORISNICKO_IME"),
							'lozinka' => Yii::t("main","LOZINKA"),
							'lozinka2'=>Yii::t("main","PONOVITE_LOZINKU"),
							'status' => 'Status',
							'tip' => Yii::t("main","TIP"),
							'staraLozinka'=>Yii::t("main","STARA_LOZINKA"),
					);
				}
			default:
				{
					return array(
							'id' => 'ID',
							'korisnickoIme' => Yii::t("main","KORISNICKO_IME"),
							'lozinka' => Yii::t("main","LOZINKA"),
							'status' => 'Status',
						'tip' => Yii::t("main","TIP"),
							'lozinka2'=>Yii::t("main","PONOVITE_LOZINKU"),
							'zapamtiMe'=>Yii::t("main","ZAPAMTI_ME"),
					);
				}
		}
	
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('korisnickoIme',$this->korisnickoIme,true);
		$criteria->compare('lozinka',$this->lozinka,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('tip',$this->tip,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Korisnik the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	public function login()
	{
	
			if($this->identitet===null)
			{
				$this->identitet=new UserIdentity ($this->korisnickoIme, $this->lozinka);
				$this->identitet->authenticate();
			}
			if($this->identitet->errorCode===UserIdentity::ERROR_NONE)
			{
	
				$duration=$this->zapamtiMe ? 3600*24*2 : 0; // 2 days
				Yii::app()->user->login($this->identitet,$duration);
	
				return true;
			}
			else {
	
				return false;
			}
	
	}
	public function beforeSave() {
		if($this->scenario=="create" || $this->scenario=="promjenaLozinke"
				 || $this->scenario=="adminMijenjaLozinku" || $this->scenario=="makler_create")
		{
			$hash=md5($this->lozinka);
			$this->lozinka=$hash;
		}

		return parent::beforeSave();
	}
}
